package com.nabcommunity.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.nabcommunity.IntegrationTest;
import com.nabcommunity.domain.StatusValidationsHistorique;
import com.nabcommunity.domain.enumeration.StatusValidations;
import com.nabcommunity.repository.StatusValidationsHistoriqueRepository;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link StatusValidationsHistoriqueResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class StatusValidationsHistoriqueResourceIT {

    private static final LocalDate DEFAULT_DATE_MODIFIACATION = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_MODIFIACATION = LocalDate.now(ZoneId.systemDefault());

    private static final StatusValidations DEFAULT_STATUS = StatusValidations.VALIDE;
    private static final StatusValidations UPDATED_STATUS = StatusValidations.REFUSE;

    private static final String ENTITY_API_URL = "/api/status-validations-historiques";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private StatusValidationsHistoriqueRepository statusValidationsHistoriqueRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restStatusValidationsHistoriqueMockMvc;

    private StatusValidationsHistorique statusValidationsHistorique;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static StatusValidationsHistorique createEntity(EntityManager em) {
        StatusValidationsHistorique statusValidationsHistorique = new StatusValidationsHistorique()
            .dateModifiacation(DEFAULT_DATE_MODIFIACATION)
            .status(DEFAULT_STATUS);
        return statusValidationsHistorique;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static StatusValidationsHistorique createUpdatedEntity(EntityManager em) {
        StatusValidationsHistorique statusValidationsHistorique = new StatusValidationsHistorique()
            .dateModifiacation(UPDATED_DATE_MODIFIACATION)
            .status(UPDATED_STATUS);
        return statusValidationsHistorique;
    }

    @BeforeEach
    public void initTest() {
        statusValidationsHistorique = createEntity(em);
    }

    @Test
    @Transactional
    void createStatusValidationsHistorique() throws Exception {
        int databaseSizeBeforeCreate = statusValidationsHistoriqueRepository.findAll().size();
        // Create the StatusValidationsHistorique
        restStatusValidationsHistoriqueMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(statusValidationsHistorique))
            )
            .andExpect(status().isCreated());

        // Validate the StatusValidationsHistorique in the database
        List<StatusValidationsHistorique> statusValidationsHistoriqueList = statusValidationsHistoriqueRepository.findAll();
        assertThat(statusValidationsHistoriqueList).hasSize(databaseSizeBeforeCreate + 1);
        StatusValidationsHistorique testStatusValidationsHistorique = statusValidationsHistoriqueList.get(
            statusValidationsHistoriqueList.size() - 1
        );
        assertThat(testStatusValidationsHistorique.getDateModifiacation()).isEqualTo(DEFAULT_DATE_MODIFIACATION);
        assertThat(testStatusValidationsHistorique.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    void createStatusValidationsHistoriqueWithExistingId() throws Exception {
        // Create the StatusValidationsHistorique with an existing ID
        statusValidationsHistorique.setId(1L);

        int databaseSizeBeforeCreate = statusValidationsHistoriqueRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restStatusValidationsHistoriqueMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(statusValidationsHistorique))
            )
            .andExpect(status().isBadRequest());

        // Validate the StatusValidationsHistorique in the database
        List<StatusValidationsHistorique> statusValidationsHistoriqueList = statusValidationsHistoriqueRepository.findAll();
        assertThat(statusValidationsHistoriqueList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllStatusValidationsHistoriques() throws Exception {
        // Initialize the database
        statusValidationsHistoriqueRepository.saveAndFlush(statusValidationsHistorique);

        // Get all the statusValidationsHistoriqueList
        restStatusValidationsHistoriqueMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(statusValidationsHistorique.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateModifiacation").value(hasItem(DEFAULT_DATE_MODIFIACATION.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    void getStatusValidationsHistorique() throws Exception {
        // Initialize the database
        statusValidationsHistoriqueRepository.saveAndFlush(statusValidationsHistorique);

        // Get the statusValidationsHistorique
        restStatusValidationsHistoriqueMockMvc
            .perform(get(ENTITY_API_URL_ID, statusValidationsHistorique.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(statusValidationsHistorique.getId().intValue()))
            .andExpect(jsonPath("$.dateModifiacation").value(DEFAULT_DATE_MODIFIACATION.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    void getNonExistingStatusValidationsHistorique() throws Exception {
        // Get the statusValidationsHistorique
        restStatusValidationsHistoriqueMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewStatusValidationsHistorique() throws Exception {
        // Initialize the database
        statusValidationsHistoriqueRepository.saveAndFlush(statusValidationsHistorique);

        int databaseSizeBeforeUpdate = statusValidationsHistoriqueRepository.findAll().size();

        // Update the statusValidationsHistorique
        StatusValidationsHistorique updatedStatusValidationsHistorique = statusValidationsHistoriqueRepository
            .findById(statusValidationsHistorique.getId())
            .get();
        // Disconnect from session so that the updates on updatedStatusValidationsHistorique are not directly saved in db
        em.detach(updatedStatusValidationsHistorique);
        updatedStatusValidationsHistorique.dateModifiacation(UPDATED_DATE_MODIFIACATION).status(UPDATED_STATUS);

        restStatusValidationsHistoriqueMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedStatusValidationsHistorique.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedStatusValidationsHistorique))
            )
            .andExpect(status().isOk());

        // Validate the StatusValidationsHistorique in the database
        List<StatusValidationsHistorique> statusValidationsHistoriqueList = statusValidationsHistoriqueRepository.findAll();
        assertThat(statusValidationsHistoriqueList).hasSize(databaseSizeBeforeUpdate);
        StatusValidationsHistorique testStatusValidationsHistorique = statusValidationsHistoriqueList.get(
            statusValidationsHistoriqueList.size() - 1
        );
        assertThat(testStatusValidationsHistorique.getDateModifiacation()).isEqualTo(UPDATED_DATE_MODIFIACATION);
        assertThat(testStatusValidationsHistorique.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void putNonExistingStatusValidationsHistorique() throws Exception {
        int databaseSizeBeforeUpdate = statusValidationsHistoriqueRepository.findAll().size();
        statusValidationsHistorique.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restStatusValidationsHistoriqueMockMvc
            .perform(
                put(ENTITY_API_URL_ID, statusValidationsHistorique.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(statusValidationsHistorique))
            )
            .andExpect(status().isBadRequest());

        // Validate the StatusValidationsHistorique in the database
        List<StatusValidationsHistorique> statusValidationsHistoriqueList = statusValidationsHistoriqueRepository.findAll();
        assertThat(statusValidationsHistoriqueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchStatusValidationsHistorique() throws Exception {
        int databaseSizeBeforeUpdate = statusValidationsHistoriqueRepository.findAll().size();
        statusValidationsHistorique.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restStatusValidationsHistoriqueMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(statusValidationsHistorique))
            )
            .andExpect(status().isBadRequest());

        // Validate the StatusValidationsHistorique in the database
        List<StatusValidationsHistorique> statusValidationsHistoriqueList = statusValidationsHistoriqueRepository.findAll();
        assertThat(statusValidationsHistoriqueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamStatusValidationsHistorique() throws Exception {
        int databaseSizeBeforeUpdate = statusValidationsHistoriqueRepository.findAll().size();
        statusValidationsHistorique.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restStatusValidationsHistoriqueMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(statusValidationsHistorique))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the StatusValidationsHistorique in the database
        List<StatusValidationsHistorique> statusValidationsHistoriqueList = statusValidationsHistoriqueRepository.findAll();
        assertThat(statusValidationsHistoriqueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateStatusValidationsHistoriqueWithPatch() throws Exception {
        // Initialize the database
        statusValidationsHistoriqueRepository.saveAndFlush(statusValidationsHistorique);

        int databaseSizeBeforeUpdate = statusValidationsHistoriqueRepository.findAll().size();

        // Update the statusValidationsHistorique using partial update
        StatusValidationsHistorique partialUpdatedStatusValidationsHistorique = new StatusValidationsHistorique();
        partialUpdatedStatusValidationsHistorique.setId(statusValidationsHistorique.getId());

        partialUpdatedStatusValidationsHistorique.status(UPDATED_STATUS);

        restStatusValidationsHistoriqueMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedStatusValidationsHistorique.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedStatusValidationsHistorique))
            )
            .andExpect(status().isOk());

        // Validate the StatusValidationsHistorique in the database
        List<StatusValidationsHistorique> statusValidationsHistoriqueList = statusValidationsHistoriqueRepository.findAll();
        assertThat(statusValidationsHistoriqueList).hasSize(databaseSizeBeforeUpdate);
        StatusValidationsHistorique testStatusValidationsHistorique = statusValidationsHistoriqueList.get(
            statusValidationsHistoriqueList.size() - 1
        );
        assertThat(testStatusValidationsHistorique.getDateModifiacation()).isEqualTo(DEFAULT_DATE_MODIFIACATION);
        assertThat(testStatusValidationsHistorique.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void fullUpdateStatusValidationsHistoriqueWithPatch() throws Exception {
        // Initialize the database
        statusValidationsHistoriqueRepository.saveAndFlush(statusValidationsHistorique);

        int databaseSizeBeforeUpdate = statusValidationsHistoriqueRepository.findAll().size();

        // Update the statusValidationsHistorique using partial update
        StatusValidationsHistorique partialUpdatedStatusValidationsHistorique = new StatusValidationsHistorique();
        partialUpdatedStatusValidationsHistorique.setId(statusValidationsHistorique.getId());

        partialUpdatedStatusValidationsHistorique.dateModifiacation(UPDATED_DATE_MODIFIACATION).status(UPDATED_STATUS);

        restStatusValidationsHistoriqueMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedStatusValidationsHistorique.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedStatusValidationsHistorique))
            )
            .andExpect(status().isOk());

        // Validate the StatusValidationsHistorique in the database
        List<StatusValidationsHistorique> statusValidationsHistoriqueList = statusValidationsHistoriqueRepository.findAll();
        assertThat(statusValidationsHistoriqueList).hasSize(databaseSizeBeforeUpdate);
        StatusValidationsHistorique testStatusValidationsHistorique = statusValidationsHistoriqueList.get(
            statusValidationsHistoriqueList.size() - 1
        );
        assertThat(testStatusValidationsHistorique.getDateModifiacation()).isEqualTo(UPDATED_DATE_MODIFIACATION);
        assertThat(testStatusValidationsHistorique.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void patchNonExistingStatusValidationsHistorique() throws Exception {
        int databaseSizeBeforeUpdate = statusValidationsHistoriqueRepository.findAll().size();
        statusValidationsHistorique.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restStatusValidationsHistoriqueMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, statusValidationsHistorique.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(statusValidationsHistorique))
            )
            .andExpect(status().isBadRequest());

        // Validate the StatusValidationsHistorique in the database
        List<StatusValidationsHistorique> statusValidationsHistoriqueList = statusValidationsHistoriqueRepository.findAll();
        assertThat(statusValidationsHistoriqueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchStatusValidationsHistorique() throws Exception {
        int databaseSizeBeforeUpdate = statusValidationsHistoriqueRepository.findAll().size();
        statusValidationsHistorique.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restStatusValidationsHistoriqueMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(statusValidationsHistorique))
            )
            .andExpect(status().isBadRequest());

        // Validate the StatusValidationsHistorique in the database
        List<StatusValidationsHistorique> statusValidationsHistoriqueList = statusValidationsHistoriqueRepository.findAll();
        assertThat(statusValidationsHistoriqueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamStatusValidationsHistorique() throws Exception {
        int databaseSizeBeforeUpdate = statusValidationsHistoriqueRepository.findAll().size();
        statusValidationsHistorique.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restStatusValidationsHistoriqueMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(statusValidationsHistorique))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the StatusValidationsHistorique in the database
        List<StatusValidationsHistorique> statusValidationsHistoriqueList = statusValidationsHistoriqueRepository.findAll();
        assertThat(statusValidationsHistoriqueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteStatusValidationsHistorique() throws Exception {
        // Initialize the database
        statusValidationsHistoriqueRepository.saveAndFlush(statusValidationsHistorique);

        int databaseSizeBeforeDelete = statusValidationsHistoriqueRepository.findAll().size();

        // Delete the statusValidationsHistorique
        restStatusValidationsHistoriqueMockMvc
            .perform(delete(ENTITY_API_URL_ID, statusValidationsHistorique.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<StatusValidationsHistorique> statusValidationsHistoriqueList = statusValidationsHistoriqueRepository.findAll();
        assertThat(statusValidationsHistoriqueList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
