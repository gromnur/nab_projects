package com.nabcommunity.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.nabcommunity.IntegrationTest;
import com.nabcommunity.domain.Missions;
import com.nabcommunity.repository.MissionsRepository;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link MissionsResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MissionsResourceIT {

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_LIEU = "AAAAAAAAAA";
    private static final String UPDATED_LIEU = "BBBBBBBBBB";

    private static final Float DEFAULT_PRIX = 1F;
    private static final Float UPDATED_PRIX = 2F;

    private static final LocalDate DEFAULT_DATE_DEBUT_MISSION = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_DEBUT_MISSION = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DATE_FIN_MISSION = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_FIN_MISSION = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DATE_CREATION_MISSION = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_CREATION_MISSION = LocalDate.now(ZoneId.systemDefault());

    private static final String ENTITY_API_URL = "/api/missions";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MissionsRepository missionsRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMissionsMockMvc;

    private Missions missions;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Missions createEntity(EntityManager em) {
        Missions missions = new Missions()
            .nom(DEFAULT_NOM)
            .lieu(DEFAULT_LIEU)
            .prix(DEFAULT_PRIX)
            .dateDebutMission(DEFAULT_DATE_DEBUT_MISSION)
            .dateFinMission(DEFAULT_DATE_FIN_MISSION)
            .dateCreationMission(DEFAULT_DATE_CREATION_MISSION);
        return missions;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Missions createUpdatedEntity(EntityManager em) {
        Missions missions = new Missions()
            .nom(UPDATED_NOM)
            .lieu(UPDATED_LIEU)
            .prix(UPDATED_PRIX)
            .dateDebutMission(UPDATED_DATE_DEBUT_MISSION)
            .dateFinMission(UPDATED_DATE_FIN_MISSION)
            .dateCreationMission(UPDATED_DATE_CREATION_MISSION);
        return missions;
    }

    @BeforeEach
    public void initTest() {
        missions = createEntity(em);
    }

    @Test
    @Transactional
    void createMissions() throws Exception {
        int databaseSizeBeforeCreate = missionsRepository.findAll().size();
        // Create the Missions
        restMissionsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(missions)))
            .andExpect(status().isCreated());

        // Validate the Missions in the database
        List<Missions> missionsList = missionsRepository.findAll();
        assertThat(missionsList).hasSize(databaseSizeBeforeCreate + 1);
        Missions testMissions = missionsList.get(missionsList.size() - 1);
        assertThat(testMissions.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testMissions.getLieu()).isEqualTo(DEFAULT_LIEU);
        assertThat(testMissions.getPrix()).isEqualTo(DEFAULT_PRIX);
        assertThat(testMissions.getDateDebutMission()).isEqualTo(DEFAULT_DATE_DEBUT_MISSION);
        assertThat(testMissions.getDateFinMission()).isEqualTo(DEFAULT_DATE_FIN_MISSION);
        assertThat(testMissions.getDateCreationMission()).isEqualTo(DEFAULT_DATE_CREATION_MISSION);
    }

    @Test
    @Transactional
    void createMissionsWithExistingId() throws Exception {
        // Create the Missions with an existing ID
        missions.setId(1L);

        int databaseSizeBeforeCreate = missionsRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMissionsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(missions)))
            .andExpect(status().isBadRequest());

        // Validate the Missions in the database
        List<Missions> missionsList = missionsRepository.findAll();
        assertThat(missionsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllMissions() throws Exception {
        // Initialize the database
        missionsRepository.saveAndFlush(missions);

        // Get all the missionsList
        restMissionsMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(missions.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].lieu").value(hasItem(DEFAULT_LIEU)))
            .andExpect(jsonPath("$.[*].prix").value(hasItem(DEFAULT_PRIX.doubleValue())))
            .andExpect(jsonPath("$.[*].dateDebutMission").value(hasItem(DEFAULT_DATE_DEBUT_MISSION.toString())))
            .andExpect(jsonPath("$.[*].dateFinMission").value(hasItem(DEFAULT_DATE_FIN_MISSION.toString())))
            .andExpect(jsonPath("$.[*].dateCreationMission").value(hasItem(DEFAULT_DATE_CREATION_MISSION.toString())));
    }

    @Test
    @Transactional
    void getMissions() throws Exception {
        // Initialize the database
        missionsRepository.saveAndFlush(missions);

        // Get the missions
        restMissionsMockMvc
            .perform(get(ENTITY_API_URL_ID, missions.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(missions.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.lieu").value(DEFAULT_LIEU))
            .andExpect(jsonPath("$.prix").value(DEFAULT_PRIX.doubleValue()))
            .andExpect(jsonPath("$.dateDebutMission").value(DEFAULT_DATE_DEBUT_MISSION.toString()))
            .andExpect(jsonPath("$.dateFinMission").value(DEFAULT_DATE_FIN_MISSION.toString()))
            .andExpect(jsonPath("$.dateCreationMission").value(DEFAULT_DATE_CREATION_MISSION.toString()));
    }

    @Test
    @Transactional
    void getNonExistingMissions() throws Exception {
        // Get the missions
        restMissionsMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewMissions() throws Exception {
        // Initialize the database
        missionsRepository.saveAndFlush(missions);

        int databaseSizeBeforeUpdate = missionsRepository.findAll().size();

        // Update the missions
        Missions updatedMissions = missionsRepository.findById(missions.getId()).get();
        // Disconnect from session so that the updates on updatedMissions are not directly saved in db
        em.detach(updatedMissions);
        updatedMissions
            .nom(UPDATED_NOM)
            .lieu(UPDATED_LIEU)
            .prix(UPDATED_PRIX)
            .dateDebutMission(UPDATED_DATE_DEBUT_MISSION)
            .dateFinMission(UPDATED_DATE_FIN_MISSION)
            .dateCreationMission(UPDATED_DATE_CREATION_MISSION);

        restMissionsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedMissions.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedMissions))
            )
            .andExpect(status().isOk());

        // Validate the Missions in the database
        List<Missions> missionsList = missionsRepository.findAll();
        assertThat(missionsList).hasSize(databaseSizeBeforeUpdate);
        Missions testMissions = missionsList.get(missionsList.size() - 1);
        assertThat(testMissions.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testMissions.getLieu()).isEqualTo(UPDATED_LIEU);
        assertThat(testMissions.getPrix()).isEqualTo(UPDATED_PRIX);
        assertThat(testMissions.getDateDebutMission()).isEqualTo(UPDATED_DATE_DEBUT_MISSION);
        assertThat(testMissions.getDateFinMission()).isEqualTo(UPDATED_DATE_FIN_MISSION);
        assertThat(testMissions.getDateCreationMission()).isEqualTo(UPDATED_DATE_CREATION_MISSION);
    }

    @Test
    @Transactional
    void putNonExistingMissions() throws Exception {
        int databaseSizeBeforeUpdate = missionsRepository.findAll().size();
        missions.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMissionsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, missions.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(missions))
            )
            .andExpect(status().isBadRequest());

        // Validate the Missions in the database
        List<Missions> missionsList = missionsRepository.findAll();
        assertThat(missionsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMissions() throws Exception {
        int databaseSizeBeforeUpdate = missionsRepository.findAll().size();
        missions.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMissionsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(missions))
            )
            .andExpect(status().isBadRequest());

        // Validate the Missions in the database
        List<Missions> missionsList = missionsRepository.findAll();
        assertThat(missionsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMissions() throws Exception {
        int databaseSizeBeforeUpdate = missionsRepository.findAll().size();
        missions.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMissionsMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(missions)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Missions in the database
        List<Missions> missionsList = missionsRepository.findAll();
        assertThat(missionsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMissionsWithPatch() throws Exception {
        // Initialize the database
        missionsRepository.saveAndFlush(missions);

        int databaseSizeBeforeUpdate = missionsRepository.findAll().size();

        // Update the missions using partial update
        Missions partialUpdatedMissions = new Missions();
        partialUpdatedMissions.setId(missions.getId());

        partialUpdatedMissions.dateDebutMission(UPDATED_DATE_DEBUT_MISSION);

        restMissionsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMissions.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMissions))
            )
            .andExpect(status().isOk());

        // Validate the Missions in the database
        List<Missions> missionsList = missionsRepository.findAll();
        assertThat(missionsList).hasSize(databaseSizeBeforeUpdate);
        Missions testMissions = missionsList.get(missionsList.size() - 1);
        assertThat(testMissions.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testMissions.getLieu()).isEqualTo(DEFAULT_LIEU);
        assertThat(testMissions.getPrix()).isEqualTo(DEFAULT_PRIX);
        assertThat(testMissions.getDateDebutMission()).isEqualTo(UPDATED_DATE_DEBUT_MISSION);
        assertThat(testMissions.getDateFinMission()).isEqualTo(DEFAULT_DATE_FIN_MISSION);
        assertThat(testMissions.getDateCreationMission()).isEqualTo(DEFAULT_DATE_CREATION_MISSION);
    }

    @Test
    @Transactional
    void fullUpdateMissionsWithPatch() throws Exception {
        // Initialize the database
        missionsRepository.saveAndFlush(missions);

        int databaseSizeBeforeUpdate = missionsRepository.findAll().size();

        // Update the missions using partial update
        Missions partialUpdatedMissions = new Missions();
        partialUpdatedMissions.setId(missions.getId());

        partialUpdatedMissions
            .nom(UPDATED_NOM)
            .lieu(UPDATED_LIEU)
            .prix(UPDATED_PRIX)
            .dateDebutMission(UPDATED_DATE_DEBUT_MISSION)
            .dateFinMission(UPDATED_DATE_FIN_MISSION)
            .dateCreationMission(UPDATED_DATE_CREATION_MISSION);

        restMissionsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMissions.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMissions))
            )
            .andExpect(status().isOk());

        // Validate the Missions in the database
        List<Missions> missionsList = missionsRepository.findAll();
        assertThat(missionsList).hasSize(databaseSizeBeforeUpdate);
        Missions testMissions = missionsList.get(missionsList.size() - 1);
        assertThat(testMissions.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testMissions.getLieu()).isEqualTo(UPDATED_LIEU);
        assertThat(testMissions.getPrix()).isEqualTo(UPDATED_PRIX);
        assertThat(testMissions.getDateDebutMission()).isEqualTo(UPDATED_DATE_DEBUT_MISSION);
        assertThat(testMissions.getDateFinMission()).isEqualTo(UPDATED_DATE_FIN_MISSION);
        assertThat(testMissions.getDateCreationMission()).isEqualTo(UPDATED_DATE_CREATION_MISSION);
    }

    @Test
    @Transactional
    void patchNonExistingMissions() throws Exception {
        int databaseSizeBeforeUpdate = missionsRepository.findAll().size();
        missions.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMissionsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, missions.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(missions))
            )
            .andExpect(status().isBadRequest());

        // Validate the Missions in the database
        List<Missions> missionsList = missionsRepository.findAll();
        assertThat(missionsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMissions() throws Exception {
        int databaseSizeBeforeUpdate = missionsRepository.findAll().size();
        missions.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMissionsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(missions))
            )
            .andExpect(status().isBadRequest());

        // Validate the Missions in the database
        List<Missions> missionsList = missionsRepository.findAll();
        assertThat(missionsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMissions() throws Exception {
        int databaseSizeBeforeUpdate = missionsRepository.findAll().size();
        missions.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMissionsMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(missions)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Missions in the database
        List<Missions> missionsList = missionsRepository.findAll();
        assertThat(missionsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMissions() throws Exception {
        // Initialize the database
        missionsRepository.saveAndFlush(missions);

        int databaseSizeBeforeDelete = missionsRepository.findAll().size();

        // Delete the missions
        restMissionsMockMvc
            .perform(delete(ENTITY_API_URL_ID, missions.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Missions> missionsList = missionsRepository.findAll();
        assertThat(missionsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
