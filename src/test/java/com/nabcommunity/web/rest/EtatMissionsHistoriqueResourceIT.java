package com.nabcommunity.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.nabcommunity.IntegrationTest;
import com.nabcommunity.domain.EtatMissionsHistorique;
import com.nabcommunity.domain.enumeration.EtatMissions;
import com.nabcommunity.repository.EtatMissionsHistoriqueRepository;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link EtatMissionsHistoriqueResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class EtatMissionsHistoriqueResourceIT {

    private static final LocalDate DEFAULT_DATE_MODIFICATION = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_MODIFICATION = LocalDate.now(ZoneId.systemDefault());

    private static final EtatMissions DEFAULT_ETAT = EtatMissions.CHOIX_CREATIF;
    private static final EtatMissions UPDATED_ETAT = EtatMissions.NON_DETAILLEE;

    private static final String ENTITY_API_URL = "/api/etat-missions-historiques";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private EtatMissionsHistoriqueRepository etatMissionsHistoriqueRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restEtatMissionsHistoriqueMockMvc;

    private EtatMissionsHistorique etatMissionsHistorique;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EtatMissionsHistorique createEntity(EntityManager em) {
        EtatMissionsHistorique etatMissionsHistorique = new EtatMissionsHistorique()
            .dateModification(DEFAULT_DATE_MODIFICATION)
            .etat(DEFAULT_ETAT);
        return etatMissionsHistorique;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EtatMissionsHistorique createUpdatedEntity(EntityManager em) {
        EtatMissionsHistorique etatMissionsHistorique = new EtatMissionsHistorique()
            .dateModification(UPDATED_DATE_MODIFICATION)
            .etat(UPDATED_ETAT);
        return etatMissionsHistorique;
    }

    @BeforeEach
    public void initTest() {
        etatMissionsHistorique = createEntity(em);
    }

    @Test
    @Transactional
    void createEtatMissionsHistorique() throws Exception {
        int databaseSizeBeforeCreate = etatMissionsHistoriqueRepository.findAll().size();
        // Create the EtatMissionsHistorique
        restEtatMissionsHistoriqueMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(etatMissionsHistorique))
            )
            .andExpect(status().isCreated());

        // Validate the EtatMissionsHistorique in the database
        List<EtatMissionsHistorique> etatMissionsHistoriqueList = etatMissionsHistoriqueRepository.findAll();
        assertThat(etatMissionsHistoriqueList).hasSize(databaseSizeBeforeCreate + 1);
        EtatMissionsHistorique testEtatMissionsHistorique = etatMissionsHistoriqueList.get(etatMissionsHistoriqueList.size() - 1);
        assertThat(testEtatMissionsHistorique.getDateModification()).isEqualTo(DEFAULT_DATE_MODIFICATION);
        assertThat(testEtatMissionsHistorique.getEtat()).isEqualTo(DEFAULT_ETAT);
    }

    @Test
    @Transactional
    void createEtatMissionsHistoriqueWithExistingId() throws Exception {
        // Create the EtatMissionsHistorique with an existing ID
        etatMissionsHistorique.setId(1L);

        int databaseSizeBeforeCreate = etatMissionsHistoriqueRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restEtatMissionsHistoriqueMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(etatMissionsHistorique))
            )
            .andExpect(status().isBadRequest());

        // Validate the EtatMissionsHistorique in the database
        List<EtatMissionsHistorique> etatMissionsHistoriqueList = etatMissionsHistoriqueRepository.findAll();
        assertThat(etatMissionsHistoriqueList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllEtatMissionsHistoriques() throws Exception {
        // Initialize the database
        etatMissionsHistoriqueRepository.saveAndFlush(etatMissionsHistorique);

        // Get all the etatMissionsHistoriqueList
        restEtatMissionsHistoriqueMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(etatMissionsHistorique.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateModification").value(hasItem(DEFAULT_DATE_MODIFICATION.toString())))
            .andExpect(jsonPath("$.[*].etat").value(hasItem(DEFAULT_ETAT.toString())));
    }

    @Test
    @Transactional
    void getEtatMissionsHistorique() throws Exception {
        // Initialize the database
        etatMissionsHistoriqueRepository.saveAndFlush(etatMissionsHistorique);

        // Get the etatMissionsHistorique
        restEtatMissionsHistoriqueMockMvc
            .perform(get(ENTITY_API_URL_ID, etatMissionsHistorique.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(etatMissionsHistorique.getId().intValue()))
            .andExpect(jsonPath("$.dateModification").value(DEFAULT_DATE_MODIFICATION.toString()))
            .andExpect(jsonPath("$.etat").value(DEFAULT_ETAT.toString()));
    }

    @Test
    @Transactional
    void getNonExistingEtatMissionsHistorique() throws Exception {
        // Get the etatMissionsHistorique
        restEtatMissionsHistoriqueMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewEtatMissionsHistorique() throws Exception {
        // Initialize the database
        etatMissionsHistoriqueRepository.saveAndFlush(etatMissionsHistorique);

        int databaseSizeBeforeUpdate = etatMissionsHistoriqueRepository.findAll().size();

        // Update the etatMissionsHistorique
        EtatMissionsHistorique updatedEtatMissionsHistorique = etatMissionsHistoriqueRepository
            .findById(etatMissionsHistorique.getId())
            .get();
        // Disconnect from session so that the updates on updatedEtatMissionsHistorique are not directly saved in db
        em.detach(updatedEtatMissionsHistorique);
        updatedEtatMissionsHistorique.dateModification(UPDATED_DATE_MODIFICATION).etat(UPDATED_ETAT);

        restEtatMissionsHistoriqueMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedEtatMissionsHistorique.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedEtatMissionsHistorique))
            )
            .andExpect(status().isOk());

        // Validate the EtatMissionsHistorique in the database
        List<EtatMissionsHistorique> etatMissionsHistoriqueList = etatMissionsHistoriqueRepository.findAll();
        assertThat(etatMissionsHistoriqueList).hasSize(databaseSizeBeforeUpdate);
        EtatMissionsHistorique testEtatMissionsHistorique = etatMissionsHistoriqueList.get(etatMissionsHistoriqueList.size() - 1);
        assertThat(testEtatMissionsHistorique.getDateModification()).isEqualTo(UPDATED_DATE_MODIFICATION);
        assertThat(testEtatMissionsHistorique.getEtat()).isEqualTo(UPDATED_ETAT);
    }

    @Test
    @Transactional
    void putNonExistingEtatMissionsHistorique() throws Exception {
        int databaseSizeBeforeUpdate = etatMissionsHistoriqueRepository.findAll().size();
        etatMissionsHistorique.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEtatMissionsHistoriqueMockMvc
            .perform(
                put(ENTITY_API_URL_ID, etatMissionsHistorique.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(etatMissionsHistorique))
            )
            .andExpect(status().isBadRequest());

        // Validate the EtatMissionsHistorique in the database
        List<EtatMissionsHistorique> etatMissionsHistoriqueList = etatMissionsHistoriqueRepository.findAll();
        assertThat(etatMissionsHistoriqueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchEtatMissionsHistorique() throws Exception {
        int databaseSizeBeforeUpdate = etatMissionsHistoriqueRepository.findAll().size();
        etatMissionsHistorique.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEtatMissionsHistoriqueMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(etatMissionsHistorique))
            )
            .andExpect(status().isBadRequest());

        // Validate the EtatMissionsHistorique in the database
        List<EtatMissionsHistorique> etatMissionsHistoriqueList = etatMissionsHistoriqueRepository.findAll();
        assertThat(etatMissionsHistoriqueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamEtatMissionsHistorique() throws Exception {
        int databaseSizeBeforeUpdate = etatMissionsHistoriqueRepository.findAll().size();
        etatMissionsHistorique.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEtatMissionsHistoriqueMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(etatMissionsHistorique))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the EtatMissionsHistorique in the database
        List<EtatMissionsHistorique> etatMissionsHistoriqueList = etatMissionsHistoriqueRepository.findAll();
        assertThat(etatMissionsHistoriqueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateEtatMissionsHistoriqueWithPatch() throws Exception {
        // Initialize the database
        etatMissionsHistoriqueRepository.saveAndFlush(etatMissionsHistorique);

        int databaseSizeBeforeUpdate = etatMissionsHistoriqueRepository.findAll().size();

        // Update the etatMissionsHistorique using partial update
        EtatMissionsHistorique partialUpdatedEtatMissionsHistorique = new EtatMissionsHistorique();
        partialUpdatedEtatMissionsHistorique.setId(etatMissionsHistorique.getId());

        partialUpdatedEtatMissionsHistorique.dateModification(UPDATED_DATE_MODIFICATION);

        restEtatMissionsHistoriqueMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEtatMissionsHistorique.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEtatMissionsHistorique))
            )
            .andExpect(status().isOk());

        // Validate the EtatMissionsHistorique in the database
        List<EtatMissionsHistorique> etatMissionsHistoriqueList = etatMissionsHistoriqueRepository.findAll();
        assertThat(etatMissionsHistoriqueList).hasSize(databaseSizeBeforeUpdate);
        EtatMissionsHistorique testEtatMissionsHistorique = etatMissionsHistoriqueList.get(etatMissionsHistoriqueList.size() - 1);
        assertThat(testEtatMissionsHistorique.getDateModification()).isEqualTo(UPDATED_DATE_MODIFICATION);
        assertThat(testEtatMissionsHistorique.getEtat()).isEqualTo(DEFAULT_ETAT);
    }

    @Test
    @Transactional
    void fullUpdateEtatMissionsHistoriqueWithPatch() throws Exception {
        // Initialize the database
        etatMissionsHistoriqueRepository.saveAndFlush(etatMissionsHistorique);

        int databaseSizeBeforeUpdate = etatMissionsHistoriqueRepository.findAll().size();

        // Update the etatMissionsHistorique using partial update
        EtatMissionsHistorique partialUpdatedEtatMissionsHistorique = new EtatMissionsHistorique();
        partialUpdatedEtatMissionsHistorique.setId(etatMissionsHistorique.getId());

        partialUpdatedEtatMissionsHistorique.dateModification(UPDATED_DATE_MODIFICATION).etat(UPDATED_ETAT);

        restEtatMissionsHistoriqueMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEtatMissionsHistorique.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEtatMissionsHistorique))
            )
            .andExpect(status().isOk());

        // Validate the EtatMissionsHistorique in the database
        List<EtatMissionsHistorique> etatMissionsHistoriqueList = etatMissionsHistoriqueRepository.findAll();
        assertThat(etatMissionsHistoriqueList).hasSize(databaseSizeBeforeUpdate);
        EtatMissionsHistorique testEtatMissionsHistorique = etatMissionsHistoriqueList.get(etatMissionsHistoriqueList.size() - 1);
        assertThat(testEtatMissionsHistorique.getDateModification()).isEqualTo(UPDATED_DATE_MODIFICATION);
        assertThat(testEtatMissionsHistorique.getEtat()).isEqualTo(UPDATED_ETAT);
    }

    @Test
    @Transactional
    void patchNonExistingEtatMissionsHistorique() throws Exception {
        int databaseSizeBeforeUpdate = etatMissionsHistoriqueRepository.findAll().size();
        etatMissionsHistorique.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEtatMissionsHistoriqueMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, etatMissionsHistorique.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(etatMissionsHistorique))
            )
            .andExpect(status().isBadRequest());

        // Validate the EtatMissionsHistorique in the database
        List<EtatMissionsHistorique> etatMissionsHistoriqueList = etatMissionsHistoriqueRepository.findAll();
        assertThat(etatMissionsHistoriqueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchEtatMissionsHistorique() throws Exception {
        int databaseSizeBeforeUpdate = etatMissionsHistoriqueRepository.findAll().size();
        etatMissionsHistorique.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEtatMissionsHistoriqueMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(etatMissionsHistorique))
            )
            .andExpect(status().isBadRequest());

        // Validate the EtatMissionsHistorique in the database
        List<EtatMissionsHistorique> etatMissionsHistoriqueList = etatMissionsHistoriqueRepository.findAll();
        assertThat(etatMissionsHistoriqueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamEtatMissionsHistorique() throws Exception {
        int databaseSizeBeforeUpdate = etatMissionsHistoriqueRepository.findAll().size();
        etatMissionsHistorique.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEtatMissionsHistoriqueMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(etatMissionsHistorique))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the EtatMissionsHistorique in the database
        List<EtatMissionsHistorique> etatMissionsHistoriqueList = etatMissionsHistoriqueRepository.findAll();
        assertThat(etatMissionsHistoriqueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteEtatMissionsHistorique() throws Exception {
        // Initialize the database
        etatMissionsHistoriqueRepository.saveAndFlush(etatMissionsHistorique);

        int databaseSizeBeforeDelete = etatMissionsHistoriqueRepository.findAll().size();

        // Delete the etatMissionsHistorique
        restEtatMissionsHistoriqueMockMvc
            .perform(delete(ENTITY_API_URL_ID, etatMissionsHistorique.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<EtatMissionsHistorique> etatMissionsHistoriqueList = etatMissionsHistoriqueRepository.findAll();
        assertThat(etatMissionsHistoriqueList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
