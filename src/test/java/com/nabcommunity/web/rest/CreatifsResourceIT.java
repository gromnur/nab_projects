package com.nabcommunity.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.nabcommunity.IntegrationTest;
import com.nabcommunity.domain.Creatifs;
import com.nabcommunity.repository.CreatifsRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CreatifsResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CreatifsResourceIT {

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_MAIL = "AAAAAAAAAA";
    private static final String UPDATED_MAIL = "BBBBBBBBBB";

    private static final String DEFAULT_ADRESSE_SIEGE_SOCIAL = "AAAAAAAAAA";
    private static final String UPDATED_ADRESSE_SIEGE_SOCIAL = "BBBBBBBBBB";

    private static final String DEFAULT_TELEPHONNE = "AAAAAAAAAA";
    private static final String UPDATED_TELEPHONNE = "BBBBBBBBBB";

    private static final String DEFAULT_INSTAGRAM = "AAAAAAAAAA";
    private static final String UPDATED_INSTAGRAM = "BBBBBBBBBB";

    private static final String DEFAULT_PRENOM = "AAAAAAAAAA";
    private static final String UPDATED_PRENOM = "BBBBBBBBBB";

    private static final Boolean DEFAULT_MICRO_ENTREPRISE = false;
    private static final Boolean UPDATED_MICRO_ENTREPRISE = true;

    private static final String DEFAULT_DATE_RECRUTEMENT = "AAAAAAAAAA";
    private static final String UPDATED_DATE_RECRUTEMENT = "BBBBBBBBBB";

    private static final String DEFAULT_SIRET = "AAAAAAAAAA";
    private static final String UPDATED_SIRET = "BBBBBBBBBB";

    private static final String DEFAULT_SIREN = "AAAAAAAAAA";
    private static final String UPDATED_SIREN = "BBBBBBBBBB";

    private static final String DEFAULT_VILLE = "AAAAAAAAAA";
    private static final String UPDATED_VILLE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/creatifs";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CreatifsRepository creatifsRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCreatifsMockMvc;

    private Creatifs creatifs;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Creatifs createEntity(EntityManager em) {
        Creatifs creatifs = new Creatifs()
            .nom(DEFAULT_NOM)
            .mail(DEFAULT_MAIL)
            .adresseSiegeSocial(DEFAULT_ADRESSE_SIEGE_SOCIAL)
            .telephonne(DEFAULT_TELEPHONNE)
            .instagram(DEFAULT_INSTAGRAM)
            .prenom(DEFAULT_PRENOM)
            .microEntreprise(DEFAULT_MICRO_ENTREPRISE)
            .dateRecrutement(DEFAULT_DATE_RECRUTEMENT)
            .siret(DEFAULT_SIRET)
            .siren(DEFAULT_SIREN)
            .ville(DEFAULT_VILLE);
        return creatifs;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Creatifs createUpdatedEntity(EntityManager em) {
        Creatifs creatifs = new Creatifs()
            .nom(UPDATED_NOM)
            .mail(UPDATED_MAIL)
            .adresseSiegeSocial(UPDATED_ADRESSE_SIEGE_SOCIAL)
            .telephonne(UPDATED_TELEPHONNE)
            .instagram(UPDATED_INSTAGRAM)
            .prenom(UPDATED_PRENOM)
            .microEntreprise(UPDATED_MICRO_ENTREPRISE)
            .dateRecrutement(UPDATED_DATE_RECRUTEMENT)
            .siret(UPDATED_SIRET)
            .siren(UPDATED_SIREN)
            .ville(UPDATED_VILLE);
        return creatifs;
    }

    @BeforeEach
    public void initTest() {
        creatifs = createEntity(em);
    }

    @Test
    @Transactional
    void createCreatifs() throws Exception {
        int databaseSizeBeforeCreate = creatifsRepository.findAll().size();
        // Create the Creatifs
        restCreatifsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(creatifs)))
            .andExpect(status().isCreated());

        // Validate the Creatifs in the database
        List<Creatifs> creatifsList = creatifsRepository.findAll();
        assertThat(creatifsList).hasSize(databaseSizeBeforeCreate + 1);
        Creatifs testCreatifs = creatifsList.get(creatifsList.size() - 1);
        assertThat(testCreatifs.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testCreatifs.getMail()).isEqualTo(DEFAULT_MAIL);
        assertThat(testCreatifs.getAdresseSiegeSocial()).isEqualTo(DEFAULT_ADRESSE_SIEGE_SOCIAL);
        assertThat(testCreatifs.getTelephonne()).isEqualTo(DEFAULT_TELEPHONNE);
        assertThat(testCreatifs.getInstagram()).isEqualTo(DEFAULT_INSTAGRAM);
        assertThat(testCreatifs.getPrenom()).isEqualTo(DEFAULT_PRENOM);
        assertThat(testCreatifs.getMicroEntreprise()).isEqualTo(DEFAULT_MICRO_ENTREPRISE);
        assertThat(testCreatifs.getDateRecrutement()).isEqualTo(DEFAULT_DATE_RECRUTEMENT);
        assertThat(testCreatifs.getSiret()).isEqualTo(DEFAULT_SIRET);
        assertThat(testCreatifs.getSiren()).isEqualTo(DEFAULT_SIREN);
        assertThat(testCreatifs.getVille()).isEqualTo(DEFAULT_VILLE);
    }

    @Test
    @Transactional
    void createCreatifsWithExistingId() throws Exception {
        // Create the Creatifs with an existing ID
        creatifs.setId(1L);

        int databaseSizeBeforeCreate = creatifsRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCreatifsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(creatifs)))
            .andExpect(status().isBadRequest());

        // Validate the Creatifs in the database
        List<Creatifs> creatifsList = creatifsRepository.findAll();
        assertThat(creatifsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllCreatifs() throws Exception {
        // Initialize the database
        creatifsRepository.saveAndFlush(creatifs);

        // Get all the creatifsList
        restCreatifsMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(creatifs.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].mail").value(hasItem(DEFAULT_MAIL)))
            .andExpect(jsonPath("$.[*].adresseSiegeSocial").value(hasItem(DEFAULT_ADRESSE_SIEGE_SOCIAL)))
            .andExpect(jsonPath("$.[*].telephonne").value(hasItem(DEFAULT_TELEPHONNE)))
            .andExpect(jsonPath("$.[*].instagram").value(hasItem(DEFAULT_INSTAGRAM)))
            .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM)))
            .andExpect(jsonPath("$.[*].microEntreprise").value(hasItem(DEFAULT_MICRO_ENTREPRISE.booleanValue())))
            .andExpect(jsonPath("$.[*].dateRecrutement").value(hasItem(DEFAULT_DATE_RECRUTEMENT)))
            .andExpect(jsonPath("$.[*].siret").value(hasItem(DEFAULT_SIRET)))
            .andExpect(jsonPath("$.[*].siren").value(hasItem(DEFAULT_SIREN)))
            .andExpect(jsonPath("$.[*].ville").value(hasItem(DEFAULT_VILLE)));
    }

    @Test
    @Transactional
    void getCreatifs() throws Exception {
        // Initialize the database
        creatifsRepository.saveAndFlush(creatifs);

        // Get the creatifs
        restCreatifsMockMvc
            .perform(get(ENTITY_API_URL_ID, creatifs.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(creatifs.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.mail").value(DEFAULT_MAIL))
            .andExpect(jsonPath("$.adresseSiegeSocial").value(DEFAULT_ADRESSE_SIEGE_SOCIAL))
            .andExpect(jsonPath("$.telephonne").value(DEFAULT_TELEPHONNE))
            .andExpect(jsonPath("$.instagram").value(DEFAULT_INSTAGRAM))
            .andExpect(jsonPath("$.prenom").value(DEFAULT_PRENOM))
            .andExpect(jsonPath("$.microEntreprise").value(DEFAULT_MICRO_ENTREPRISE.booleanValue()))
            .andExpect(jsonPath("$.dateRecrutement").value(DEFAULT_DATE_RECRUTEMENT))
            .andExpect(jsonPath("$.siret").value(DEFAULT_SIRET))
            .andExpect(jsonPath("$.siren").value(DEFAULT_SIREN))
            .andExpect(jsonPath("$.ville").value(DEFAULT_VILLE));
    }

    @Test
    @Transactional
    void getNonExistingCreatifs() throws Exception {
        // Get the creatifs
        restCreatifsMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCreatifs() throws Exception {
        // Initialize the database
        creatifsRepository.saveAndFlush(creatifs);

        int databaseSizeBeforeUpdate = creatifsRepository.findAll().size();

        // Update the creatifs
        Creatifs updatedCreatifs = creatifsRepository.findById(creatifs.getId()).get();
        // Disconnect from session so that the updates on updatedCreatifs are not directly saved in db
        em.detach(updatedCreatifs);
        updatedCreatifs
            .nom(UPDATED_NOM)
            .mail(UPDATED_MAIL)
            .adresseSiegeSocial(UPDATED_ADRESSE_SIEGE_SOCIAL)
            .telephonne(UPDATED_TELEPHONNE)
            .instagram(UPDATED_INSTAGRAM)
            .prenom(UPDATED_PRENOM)
            .microEntreprise(UPDATED_MICRO_ENTREPRISE)
            .dateRecrutement(UPDATED_DATE_RECRUTEMENT)
            .siret(UPDATED_SIRET)
            .siren(UPDATED_SIREN)
            .ville(UPDATED_VILLE);

        restCreatifsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedCreatifs.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedCreatifs))
            )
            .andExpect(status().isOk());

        // Validate the Creatifs in the database
        List<Creatifs> creatifsList = creatifsRepository.findAll();
        assertThat(creatifsList).hasSize(databaseSizeBeforeUpdate);
        Creatifs testCreatifs = creatifsList.get(creatifsList.size() - 1);
        assertThat(testCreatifs.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testCreatifs.getMail()).isEqualTo(UPDATED_MAIL);
        assertThat(testCreatifs.getAdresseSiegeSocial()).isEqualTo(UPDATED_ADRESSE_SIEGE_SOCIAL);
        assertThat(testCreatifs.getTelephonne()).isEqualTo(UPDATED_TELEPHONNE);
        assertThat(testCreatifs.getInstagram()).isEqualTo(UPDATED_INSTAGRAM);
        assertThat(testCreatifs.getPrenom()).isEqualTo(UPDATED_PRENOM);
        assertThat(testCreatifs.getMicroEntreprise()).isEqualTo(UPDATED_MICRO_ENTREPRISE);
        assertThat(testCreatifs.getDateRecrutement()).isEqualTo(UPDATED_DATE_RECRUTEMENT);
        assertThat(testCreatifs.getSiret()).isEqualTo(UPDATED_SIRET);
        assertThat(testCreatifs.getSiren()).isEqualTo(UPDATED_SIREN);
        assertThat(testCreatifs.getVille()).isEqualTo(UPDATED_VILLE);
    }

    @Test
    @Transactional
    void putNonExistingCreatifs() throws Exception {
        int databaseSizeBeforeUpdate = creatifsRepository.findAll().size();
        creatifs.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCreatifsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, creatifs.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(creatifs))
            )
            .andExpect(status().isBadRequest());

        // Validate the Creatifs in the database
        List<Creatifs> creatifsList = creatifsRepository.findAll();
        assertThat(creatifsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCreatifs() throws Exception {
        int databaseSizeBeforeUpdate = creatifsRepository.findAll().size();
        creatifs.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCreatifsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(creatifs))
            )
            .andExpect(status().isBadRequest());

        // Validate the Creatifs in the database
        List<Creatifs> creatifsList = creatifsRepository.findAll();
        assertThat(creatifsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCreatifs() throws Exception {
        int databaseSizeBeforeUpdate = creatifsRepository.findAll().size();
        creatifs.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCreatifsMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(creatifs)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Creatifs in the database
        List<Creatifs> creatifsList = creatifsRepository.findAll();
        assertThat(creatifsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCreatifsWithPatch() throws Exception {
        // Initialize the database
        creatifsRepository.saveAndFlush(creatifs);

        int databaseSizeBeforeUpdate = creatifsRepository.findAll().size();

        // Update the creatifs using partial update
        Creatifs partialUpdatedCreatifs = new Creatifs();
        partialUpdatedCreatifs.setId(creatifs.getId());

        partialUpdatedCreatifs
            .nom(UPDATED_NOM)
            .adresseSiegeSocial(UPDATED_ADRESSE_SIEGE_SOCIAL)
            .instagram(UPDATED_INSTAGRAM)
            .siret(UPDATED_SIRET)
            .siren(UPDATED_SIREN)
            .ville(UPDATED_VILLE);

        restCreatifsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCreatifs.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCreatifs))
            )
            .andExpect(status().isOk());

        // Validate the Creatifs in the database
        List<Creatifs> creatifsList = creatifsRepository.findAll();
        assertThat(creatifsList).hasSize(databaseSizeBeforeUpdate);
        Creatifs testCreatifs = creatifsList.get(creatifsList.size() - 1);
        assertThat(testCreatifs.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testCreatifs.getMail()).isEqualTo(DEFAULT_MAIL);
        assertThat(testCreatifs.getAdresseSiegeSocial()).isEqualTo(UPDATED_ADRESSE_SIEGE_SOCIAL);
        assertThat(testCreatifs.getTelephonne()).isEqualTo(DEFAULT_TELEPHONNE);
        assertThat(testCreatifs.getInstagram()).isEqualTo(UPDATED_INSTAGRAM);
        assertThat(testCreatifs.getPrenom()).isEqualTo(DEFAULT_PRENOM);
        assertThat(testCreatifs.getMicroEntreprise()).isEqualTo(DEFAULT_MICRO_ENTREPRISE);
        assertThat(testCreatifs.getDateRecrutement()).isEqualTo(DEFAULT_DATE_RECRUTEMENT);
        assertThat(testCreatifs.getSiret()).isEqualTo(UPDATED_SIRET);
        assertThat(testCreatifs.getSiren()).isEqualTo(UPDATED_SIREN);
        assertThat(testCreatifs.getVille()).isEqualTo(UPDATED_VILLE);
    }

    @Test
    @Transactional
    void fullUpdateCreatifsWithPatch() throws Exception {
        // Initialize the database
        creatifsRepository.saveAndFlush(creatifs);

        int databaseSizeBeforeUpdate = creatifsRepository.findAll().size();

        // Update the creatifs using partial update
        Creatifs partialUpdatedCreatifs = new Creatifs();
        partialUpdatedCreatifs.setId(creatifs.getId());

        partialUpdatedCreatifs
            .nom(UPDATED_NOM)
            .mail(UPDATED_MAIL)
            .adresseSiegeSocial(UPDATED_ADRESSE_SIEGE_SOCIAL)
            .telephonne(UPDATED_TELEPHONNE)
            .instagram(UPDATED_INSTAGRAM)
            .prenom(UPDATED_PRENOM)
            .microEntreprise(UPDATED_MICRO_ENTREPRISE)
            .dateRecrutement(UPDATED_DATE_RECRUTEMENT)
            .siret(UPDATED_SIRET)
            .siren(UPDATED_SIREN)
            .ville(UPDATED_VILLE);

        restCreatifsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCreatifs.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCreatifs))
            )
            .andExpect(status().isOk());

        // Validate the Creatifs in the database
        List<Creatifs> creatifsList = creatifsRepository.findAll();
        assertThat(creatifsList).hasSize(databaseSizeBeforeUpdate);
        Creatifs testCreatifs = creatifsList.get(creatifsList.size() - 1);
        assertThat(testCreatifs.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testCreatifs.getMail()).isEqualTo(UPDATED_MAIL);
        assertThat(testCreatifs.getAdresseSiegeSocial()).isEqualTo(UPDATED_ADRESSE_SIEGE_SOCIAL);
        assertThat(testCreatifs.getTelephonne()).isEqualTo(UPDATED_TELEPHONNE);
        assertThat(testCreatifs.getInstagram()).isEqualTo(UPDATED_INSTAGRAM);
        assertThat(testCreatifs.getPrenom()).isEqualTo(UPDATED_PRENOM);
        assertThat(testCreatifs.getMicroEntreprise()).isEqualTo(UPDATED_MICRO_ENTREPRISE);
        assertThat(testCreatifs.getDateRecrutement()).isEqualTo(UPDATED_DATE_RECRUTEMENT);
        assertThat(testCreatifs.getSiret()).isEqualTo(UPDATED_SIRET);
        assertThat(testCreatifs.getSiren()).isEqualTo(UPDATED_SIREN);
        assertThat(testCreatifs.getVille()).isEqualTo(UPDATED_VILLE);
    }

    @Test
    @Transactional
    void patchNonExistingCreatifs() throws Exception {
        int databaseSizeBeforeUpdate = creatifsRepository.findAll().size();
        creatifs.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCreatifsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, creatifs.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(creatifs))
            )
            .andExpect(status().isBadRequest());

        // Validate the Creatifs in the database
        List<Creatifs> creatifsList = creatifsRepository.findAll();
        assertThat(creatifsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCreatifs() throws Exception {
        int databaseSizeBeforeUpdate = creatifsRepository.findAll().size();
        creatifs.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCreatifsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(creatifs))
            )
            .andExpect(status().isBadRequest());

        // Validate the Creatifs in the database
        List<Creatifs> creatifsList = creatifsRepository.findAll();
        assertThat(creatifsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCreatifs() throws Exception {
        int databaseSizeBeforeUpdate = creatifsRepository.findAll().size();
        creatifs.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCreatifsMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(creatifs)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Creatifs in the database
        List<Creatifs> creatifsList = creatifsRepository.findAll();
        assertThat(creatifsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCreatifs() throws Exception {
        // Initialize the database
        creatifsRepository.saveAndFlush(creatifs);

        int databaseSizeBeforeDelete = creatifsRepository.findAll().size();

        // Delete the creatifs
        restCreatifsMockMvc
            .perform(delete(ENTITY_API_URL_ID, creatifs.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Creatifs> creatifsList = creatifsRepository.findAll();
        assertThat(creatifsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
