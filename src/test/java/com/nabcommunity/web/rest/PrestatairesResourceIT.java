package com.nabcommunity.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.nabcommunity.IntegrationTest;
import com.nabcommunity.domain.Prestataires;
import com.nabcommunity.repository.PrestatairesRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PrestatairesResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PrestatairesResourceIT {

    private static final String DEFAULT_NOM_SOCIETE = "AAAAAAAAAA";
    private static final String UPDATED_NOM_SOCIETE = "BBBBBBBBBB";

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_PRENOM = "AAAAAAAAAA";
    private static final String UPDATED_PRENOM = "BBBBBBBBBB";

    private static final String DEFAULT_MAIL = "AAAAAAAAAA";
    private static final String UPDATED_MAIL = "BBBBBBBBBB";

    private static final String DEFAULT_ADRESSE = "AAAAAAAAAA";
    private static final String UPDATED_ADRESSE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_MICRO_ENTREPRISE = false;
    private static final Boolean UPDATED_MICRO_ENTREPRISE = true;

    private static final String DEFAULT_INSTAGRAM = "AAAAAAAAAA";
    private static final String UPDATED_INSTAGRAM = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/prestataires";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PrestatairesRepository prestatairesRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPrestatairesMockMvc;

    private Prestataires prestataires;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Prestataires createEntity(EntityManager em) {
        Prestataires prestataires = new Prestataires()
            .nomSociete(DEFAULT_NOM_SOCIETE)
            .nom(DEFAULT_NOM)
            .prenom(DEFAULT_PRENOM)
            .mail(DEFAULT_MAIL)
            .adresse(DEFAULT_ADRESSE)
            .microEntreprise(DEFAULT_MICRO_ENTREPRISE)
            .instagram(DEFAULT_INSTAGRAM);
        return prestataires;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Prestataires createUpdatedEntity(EntityManager em) {
        Prestataires prestataires = new Prestataires()
            .nomSociete(UPDATED_NOM_SOCIETE)
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM)
            .mail(UPDATED_MAIL)
            .adresse(UPDATED_ADRESSE)
            .microEntreprise(UPDATED_MICRO_ENTREPRISE)
            .instagram(UPDATED_INSTAGRAM);
        return prestataires;
    }

    @BeforeEach
    public void initTest() {
        prestataires = createEntity(em);
    }

    @Test
    @Transactional
    void createPrestataires() throws Exception {
        int databaseSizeBeforeCreate = prestatairesRepository.findAll().size();
        // Create the Prestataires
        restPrestatairesMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(prestataires)))
            .andExpect(status().isCreated());

        // Validate the Prestataires in the database
        List<Prestataires> prestatairesList = prestatairesRepository.findAll();
        assertThat(prestatairesList).hasSize(databaseSizeBeforeCreate + 1);
        Prestataires testPrestataires = prestatairesList.get(prestatairesList.size() - 1);
        assertThat(testPrestataires.getNomSociete()).isEqualTo(DEFAULT_NOM_SOCIETE);
        assertThat(testPrestataires.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testPrestataires.getPrenom()).isEqualTo(DEFAULT_PRENOM);
        assertThat(testPrestataires.getMail()).isEqualTo(DEFAULT_MAIL);
        assertThat(testPrestataires.getAdresse()).isEqualTo(DEFAULT_ADRESSE);
        assertThat(testPrestataires.getMicroEntreprise()).isEqualTo(DEFAULT_MICRO_ENTREPRISE);
        assertThat(testPrestataires.getInstagram()).isEqualTo(DEFAULT_INSTAGRAM);
    }

    @Test
    @Transactional
    void createPrestatairesWithExistingId() throws Exception {
        // Create the Prestataires with an existing ID
        prestataires.setId(1L);

        int databaseSizeBeforeCreate = prestatairesRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPrestatairesMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(prestataires)))
            .andExpect(status().isBadRequest());

        // Validate the Prestataires in the database
        List<Prestataires> prestatairesList = prestatairesRepository.findAll();
        assertThat(prestatairesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllPrestataires() throws Exception {
        // Initialize the database
        prestatairesRepository.saveAndFlush(prestataires);

        // Get all the prestatairesList
        restPrestatairesMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(prestataires.getId().intValue())))
            .andExpect(jsonPath("$.[*].nomSociete").value(hasItem(DEFAULT_NOM_SOCIETE)))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM)))
            .andExpect(jsonPath("$.[*].mail").value(hasItem(DEFAULT_MAIL)))
            .andExpect(jsonPath("$.[*].adresse").value(hasItem(DEFAULT_ADRESSE)))
            .andExpect(jsonPath("$.[*].microEntreprise").value(hasItem(DEFAULT_MICRO_ENTREPRISE.booleanValue())))
            .andExpect(jsonPath("$.[*].instagram").value(hasItem(DEFAULT_INSTAGRAM)));
    }

    @Test
    @Transactional
    void getPrestataires() throws Exception {
        // Initialize the database
        prestatairesRepository.saveAndFlush(prestataires);

        // Get the prestataires
        restPrestatairesMockMvc
            .perform(get(ENTITY_API_URL_ID, prestataires.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(prestataires.getId().intValue()))
            .andExpect(jsonPath("$.nomSociete").value(DEFAULT_NOM_SOCIETE))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.prenom").value(DEFAULT_PRENOM))
            .andExpect(jsonPath("$.mail").value(DEFAULT_MAIL))
            .andExpect(jsonPath("$.adresse").value(DEFAULT_ADRESSE))
            .andExpect(jsonPath("$.microEntreprise").value(DEFAULT_MICRO_ENTREPRISE.booleanValue()))
            .andExpect(jsonPath("$.instagram").value(DEFAULT_INSTAGRAM));
    }

    @Test
    @Transactional
    void getNonExistingPrestataires() throws Exception {
        // Get the prestataires
        restPrestatairesMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewPrestataires() throws Exception {
        // Initialize the database
        prestatairesRepository.saveAndFlush(prestataires);

        int databaseSizeBeforeUpdate = prestatairesRepository.findAll().size();

        // Update the prestataires
        Prestataires updatedPrestataires = prestatairesRepository.findById(prestataires.getId()).get();
        // Disconnect from session so that the updates on updatedPrestataires are not directly saved in db
        em.detach(updatedPrestataires);
        updatedPrestataires
            .nomSociete(UPDATED_NOM_SOCIETE)
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM)
            .mail(UPDATED_MAIL)
            .adresse(UPDATED_ADRESSE)
            .microEntreprise(UPDATED_MICRO_ENTREPRISE)
            .instagram(UPDATED_INSTAGRAM);

        restPrestatairesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedPrestataires.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedPrestataires))
            )
            .andExpect(status().isOk());

        // Validate the Prestataires in the database
        List<Prestataires> prestatairesList = prestatairesRepository.findAll();
        assertThat(prestatairesList).hasSize(databaseSizeBeforeUpdate);
        Prestataires testPrestataires = prestatairesList.get(prestatairesList.size() - 1);
        assertThat(testPrestataires.getNomSociete()).isEqualTo(UPDATED_NOM_SOCIETE);
        assertThat(testPrestataires.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testPrestataires.getPrenom()).isEqualTo(UPDATED_PRENOM);
        assertThat(testPrestataires.getMail()).isEqualTo(UPDATED_MAIL);
        assertThat(testPrestataires.getAdresse()).isEqualTo(UPDATED_ADRESSE);
        assertThat(testPrestataires.getMicroEntreprise()).isEqualTo(UPDATED_MICRO_ENTREPRISE);
        assertThat(testPrestataires.getInstagram()).isEqualTo(UPDATED_INSTAGRAM);
    }

    @Test
    @Transactional
    void putNonExistingPrestataires() throws Exception {
        int databaseSizeBeforeUpdate = prestatairesRepository.findAll().size();
        prestataires.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPrestatairesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, prestataires.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(prestataires))
            )
            .andExpect(status().isBadRequest());

        // Validate the Prestataires in the database
        List<Prestataires> prestatairesList = prestatairesRepository.findAll();
        assertThat(prestatairesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPrestataires() throws Exception {
        int databaseSizeBeforeUpdate = prestatairesRepository.findAll().size();
        prestataires.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPrestatairesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(prestataires))
            )
            .andExpect(status().isBadRequest());

        // Validate the Prestataires in the database
        List<Prestataires> prestatairesList = prestatairesRepository.findAll();
        assertThat(prestatairesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPrestataires() throws Exception {
        int databaseSizeBeforeUpdate = prestatairesRepository.findAll().size();
        prestataires.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPrestatairesMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(prestataires)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Prestataires in the database
        List<Prestataires> prestatairesList = prestatairesRepository.findAll();
        assertThat(prestatairesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePrestatairesWithPatch() throws Exception {
        // Initialize the database
        prestatairesRepository.saveAndFlush(prestataires);

        int databaseSizeBeforeUpdate = prestatairesRepository.findAll().size();

        // Update the prestataires using partial update
        Prestataires partialUpdatedPrestataires = new Prestataires();
        partialUpdatedPrestataires.setId(prestataires.getId());

        partialUpdatedPrestataires.nom(UPDATED_NOM).prenom(UPDATED_PRENOM).instagram(UPDATED_INSTAGRAM);

        restPrestatairesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPrestataires.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPrestataires))
            )
            .andExpect(status().isOk());

        // Validate the Prestataires in the database
        List<Prestataires> prestatairesList = prestatairesRepository.findAll();
        assertThat(prestatairesList).hasSize(databaseSizeBeforeUpdate);
        Prestataires testPrestataires = prestatairesList.get(prestatairesList.size() - 1);
        assertThat(testPrestataires.getNomSociete()).isEqualTo(DEFAULT_NOM_SOCIETE);
        assertThat(testPrestataires.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testPrestataires.getPrenom()).isEqualTo(UPDATED_PRENOM);
        assertThat(testPrestataires.getMail()).isEqualTo(DEFAULT_MAIL);
        assertThat(testPrestataires.getAdresse()).isEqualTo(DEFAULT_ADRESSE);
        assertThat(testPrestataires.getMicroEntreprise()).isEqualTo(DEFAULT_MICRO_ENTREPRISE);
        assertThat(testPrestataires.getInstagram()).isEqualTo(UPDATED_INSTAGRAM);
    }

    @Test
    @Transactional
    void fullUpdatePrestatairesWithPatch() throws Exception {
        // Initialize the database
        prestatairesRepository.saveAndFlush(prestataires);

        int databaseSizeBeforeUpdate = prestatairesRepository.findAll().size();

        // Update the prestataires using partial update
        Prestataires partialUpdatedPrestataires = new Prestataires();
        partialUpdatedPrestataires.setId(prestataires.getId());

        partialUpdatedPrestataires
            .nomSociete(UPDATED_NOM_SOCIETE)
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM)
            .mail(UPDATED_MAIL)
            .adresse(UPDATED_ADRESSE)
            .microEntreprise(UPDATED_MICRO_ENTREPRISE)
            .instagram(UPDATED_INSTAGRAM);

        restPrestatairesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPrestataires.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPrestataires))
            )
            .andExpect(status().isOk());

        // Validate the Prestataires in the database
        List<Prestataires> prestatairesList = prestatairesRepository.findAll();
        assertThat(prestatairesList).hasSize(databaseSizeBeforeUpdate);
        Prestataires testPrestataires = prestatairesList.get(prestatairesList.size() - 1);
        assertThat(testPrestataires.getNomSociete()).isEqualTo(UPDATED_NOM_SOCIETE);
        assertThat(testPrestataires.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testPrestataires.getPrenom()).isEqualTo(UPDATED_PRENOM);
        assertThat(testPrestataires.getMail()).isEqualTo(UPDATED_MAIL);
        assertThat(testPrestataires.getAdresse()).isEqualTo(UPDATED_ADRESSE);
        assertThat(testPrestataires.getMicroEntreprise()).isEqualTo(UPDATED_MICRO_ENTREPRISE);
        assertThat(testPrestataires.getInstagram()).isEqualTo(UPDATED_INSTAGRAM);
    }

    @Test
    @Transactional
    void patchNonExistingPrestataires() throws Exception {
        int databaseSizeBeforeUpdate = prestatairesRepository.findAll().size();
        prestataires.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPrestatairesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, prestataires.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(prestataires))
            )
            .andExpect(status().isBadRequest());

        // Validate the Prestataires in the database
        List<Prestataires> prestatairesList = prestatairesRepository.findAll();
        assertThat(prestatairesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPrestataires() throws Exception {
        int databaseSizeBeforeUpdate = prestatairesRepository.findAll().size();
        prestataires.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPrestatairesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(prestataires))
            )
            .andExpect(status().isBadRequest());

        // Validate the Prestataires in the database
        List<Prestataires> prestatairesList = prestatairesRepository.findAll();
        assertThat(prestatairesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPrestataires() throws Exception {
        int databaseSizeBeforeUpdate = prestatairesRepository.findAll().size();
        prestataires.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPrestatairesMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(prestataires))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Prestataires in the database
        List<Prestataires> prestatairesList = prestatairesRepository.findAll();
        assertThat(prestatairesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePrestataires() throws Exception {
        // Initialize the database
        prestatairesRepository.saveAndFlush(prestataires);

        int databaseSizeBeforeDelete = prestatairesRepository.findAll().size();

        // Delete the prestataires
        restPrestatairesMockMvc
            .perform(delete(ENTITY_API_URL_ID, prestataires.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Prestataires> prestatairesList = prestatairesRepository.findAll();
        assertThat(prestatairesList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
