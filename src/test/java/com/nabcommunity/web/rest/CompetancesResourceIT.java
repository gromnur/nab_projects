package com.nabcommunity.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.nabcommunity.IntegrationTest;
import com.nabcommunity.domain.Competances;
import com.nabcommunity.repository.CompetancesRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CompetancesResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class CompetancesResourceIT {

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/competances";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CompetancesRepository competancesRepository;

    @Mock
    private CompetancesRepository competancesRepositoryMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCompetancesMockMvc;

    private Competances competances;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Competances createEntity(EntityManager em) {
        Competances competances = new Competances().nom(DEFAULT_NOM).description(DEFAULT_DESCRIPTION);
        return competances;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Competances createUpdatedEntity(EntityManager em) {
        Competances competances = new Competances().nom(UPDATED_NOM).description(UPDATED_DESCRIPTION);
        return competances;
    }

    @BeforeEach
    public void initTest() {
        competances = createEntity(em);
    }

    @Test
    @Transactional
    void createCompetances() throws Exception {
        int databaseSizeBeforeCreate = competancesRepository.findAll().size();
        // Create the Competances
        restCompetancesMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(competances)))
            .andExpect(status().isCreated());

        // Validate the Competances in the database
        List<Competances> competancesList = competancesRepository.findAll();
        assertThat(competancesList).hasSize(databaseSizeBeforeCreate + 1);
        Competances testCompetances = competancesList.get(competancesList.size() - 1);
        assertThat(testCompetances.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testCompetances.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    void createCompetancesWithExistingId() throws Exception {
        // Create the Competances with an existing ID
        competances.setId(1L);

        int databaseSizeBeforeCreate = competancesRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCompetancesMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(competances)))
            .andExpect(status().isBadRequest());

        // Validate the Competances in the database
        List<Competances> competancesList = competancesRepository.findAll();
        assertThat(competancesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllCompetances() throws Exception {
        // Initialize the database
        competancesRepository.saveAndFlush(competances);

        // Get all the competancesList
        restCompetancesMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(competances.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllCompetancesWithEagerRelationshipsIsEnabled() throws Exception {
        when(competancesRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restCompetancesMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(competancesRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllCompetancesWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(competancesRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restCompetancesMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(competancesRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    void getCompetances() throws Exception {
        // Initialize the database
        competancesRepository.saveAndFlush(competances);

        // Get the competances
        restCompetancesMockMvc
            .perform(get(ENTITY_API_URL_ID, competances.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(competances.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION));
    }

    @Test
    @Transactional
    void getNonExistingCompetances() throws Exception {
        // Get the competances
        restCompetancesMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCompetances() throws Exception {
        // Initialize the database
        competancesRepository.saveAndFlush(competances);

        int databaseSizeBeforeUpdate = competancesRepository.findAll().size();

        // Update the competances
        Competances updatedCompetances = competancesRepository.findById(competances.getId()).get();
        // Disconnect from session so that the updates on updatedCompetances are not directly saved in db
        em.detach(updatedCompetances);
        updatedCompetances.nom(UPDATED_NOM).description(UPDATED_DESCRIPTION);

        restCompetancesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedCompetances.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedCompetances))
            )
            .andExpect(status().isOk());

        // Validate the Competances in the database
        List<Competances> competancesList = competancesRepository.findAll();
        assertThat(competancesList).hasSize(databaseSizeBeforeUpdate);
        Competances testCompetances = competancesList.get(competancesList.size() - 1);
        assertThat(testCompetances.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testCompetances.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void putNonExistingCompetances() throws Exception {
        int databaseSizeBeforeUpdate = competancesRepository.findAll().size();
        competances.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCompetancesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, competances.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(competances))
            )
            .andExpect(status().isBadRequest());

        // Validate the Competances in the database
        List<Competances> competancesList = competancesRepository.findAll();
        assertThat(competancesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCompetances() throws Exception {
        int databaseSizeBeforeUpdate = competancesRepository.findAll().size();
        competances.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCompetancesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(competances))
            )
            .andExpect(status().isBadRequest());

        // Validate the Competances in the database
        List<Competances> competancesList = competancesRepository.findAll();
        assertThat(competancesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCompetances() throws Exception {
        int databaseSizeBeforeUpdate = competancesRepository.findAll().size();
        competances.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCompetancesMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(competances)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Competances in the database
        List<Competances> competancesList = competancesRepository.findAll();
        assertThat(competancesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCompetancesWithPatch() throws Exception {
        // Initialize the database
        competancesRepository.saveAndFlush(competances);

        int databaseSizeBeforeUpdate = competancesRepository.findAll().size();

        // Update the competances using partial update
        Competances partialUpdatedCompetances = new Competances();
        partialUpdatedCompetances.setId(competances.getId());

        partialUpdatedCompetances.nom(UPDATED_NOM);

        restCompetancesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCompetances.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCompetances))
            )
            .andExpect(status().isOk());

        // Validate the Competances in the database
        List<Competances> competancesList = competancesRepository.findAll();
        assertThat(competancesList).hasSize(databaseSizeBeforeUpdate);
        Competances testCompetances = competancesList.get(competancesList.size() - 1);
        assertThat(testCompetances.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testCompetances.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    void fullUpdateCompetancesWithPatch() throws Exception {
        // Initialize the database
        competancesRepository.saveAndFlush(competances);

        int databaseSizeBeforeUpdate = competancesRepository.findAll().size();

        // Update the competances using partial update
        Competances partialUpdatedCompetances = new Competances();
        partialUpdatedCompetances.setId(competances.getId());

        partialUpdatedCompetances.nom(UPDATED_NOM).description(UPDATED_DESCRIPTION);

        restCompetancesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCompetances.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCompetances))
            )
            .andExpect(status().isOk());

        // Validate the Competances in the database
        List<Competances> competancesList = competancesRepository.findAll();
        assertThat(competancesList).hasSize(databaseSizeBeforeUpdate);
        Competances testCompetances = competancesList.get(competancesList.size() - 1);
        assertThat(testCompetances.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testCompetances.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void patchNonExistingCompetances() throws Exception {
        int databaseSizeBeforeUpdate = competancesRepository.findAll().size();
        competances.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCompetancesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, competances.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(competances))
            )
            .andExpect(status().isBadRequest());

        // Validate the Competances in the database
        List<Competances> competancesList = competancesRepository.findAll();
        assertThat(competancesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCompetances() throws Exception {
        int databaseSizeBeforeUpdate = competancesRepository.findAll().size();
        competances.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCompetancesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(competances))
            )
            .andExpect(status().isBadRequest());

        // Validate the Competances in the database
        List<Competances> competancesList = competancesRepository.findAll();
        assertThat(competancesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCompetances() throws Exception {
        int databaseSizeBeforeUpdate = competancesRepository.findAll().size();
        competances.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCompetancesMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(competances))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Competances in the database
        List<Competances> competancesList = competancesRepository.findAll();
        assertThat(competancesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCompetances() throws Exception {
        // Initialize the database
        competancesRepository.saveAndFlush(competances);

        int databaseSizeBeforeDelete = competancesRepository.findAll().size();

        // Delete the competances
        restCompetancesMockMvc
            .perform(delete(ENTITY_API_URL_ID, competances.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Competances> competancesList = competancesRepository.findAll();
        assertThat(competancesList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
