package com.nabcommunity.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.nabcommunity.IntegrationTest;
import com.nabcommunity.domain.StatusCreatifsHistorique;
import com.nabcommunity.domain.enumeration.StatusCreatifs;
import com.nabcommunity.repository.StatusCreatifsHistoriqueRepository;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link StatusCreatifsHistoriqueResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class StatusCreatifsHistoriqueResourceIT {

    private static final LocalDate DEFAULT_DATE_MODIFICATION = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_MODIFICATION = LocalDate.now(ZoneId.systemDefault());

    private static final StatusCreatifs DEFAULT_STATUS = StatusCreatifs.ACTIF;
    private static final StatusCreatifs UPDATED_STATUS = StatusCreatifs.INACTIF;

    private static final String ENTITY_API_URL = "/api/status-creatifs-historiques";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private StatusCreatifsHistoriqueRepository statusCreatifsHistoriqueRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restStatusCreatifsHistoriqueMockMvc;

    private StatusCreatifsHistorique statusCreatifsHistorique;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static StatusCreatifsHistorique createEntity(EntityManager em) {
        StatusCreatifsHistorique statusCreatifsHistorique = new StatusCreatifsHistorique()
            .dateModification(DEFAULT_DATE_MODIFICATION)
            .status(DEFAULT_STATUS);
        return statusCreatifsHistorique;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static StatusCreatifsHistorique createUpdatedEntity(EntityManager em) {
        StatusCreatifsHistorique statusCreatifsHistorique = new StatusCreatifsHistorique()
            .dateModification(UPDATED_DATE_MODIFICATION)
            .status(UPDATED_STATUS);
        return statusCreatifsHistorique;
    }

    @BeforeEach
    public void initTest() {
        statusCreatifsHistorique = createEntity(em);
    }

    @Test
    @Transactional
    void createStatusCreatifsHistorique() throws Exception {
        int databaseSizeBeforeCreate = statusCreatifsHistoriqueRepository.findAll().size();
        // Create the StatusCreatifsHistorique
        restStatusCreatifsHistoriqueMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(statusCreatifsHistorique))
            )
            .andExpect(status().isCreated());

        // Validate the StatusCreatifsHistorique in the database
        List<StatusCreatifsHistorique> statusCreatifsHistoriqueList = statusCreatifsHistoriqueRepository.findAll();
        assertThat(statusCreatifsHistoriqueList).hasSize(databaseSizeBeforeCreate + 1);
        StatusCreatifsHistorique testStatusCreatifsHistorique = statusCreatifsHistoriqueList.get(statusCreatifsHistoriqueList.size() - 1);
        assertThat(testStatusCreatifsHistorique.getDateModification()).isEqualTo(DEFAULT_DATE_MODIFICATION);
        assertThat(testStatusCreatifsHistorique.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    void createStatusCreatifsHistoriqueWithExistingId() throws Exception {
        // Create the StatusCreatifsHistorique with an existing ID
        statusCreatifsHistorique.setId(1L);

        int databaseSizeBeforeCreate = statusCreatifsHistoriqueRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restStatusCreatifsHistoriqueMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(statusCreatifsHistorique))
            )
            .andExpect(status().isBadRequest());

        // Validate the StatusCreatifsHistorique in the database
        List<StatusCreatifsHistorique> statusCreatifsHistoriqueList = statusCreatifsHistoriqueRepository.findAll();
        assertThat(statusCreatifsHistoriqueList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllStatusCreatifsHistoriques() throws Exception {
        // Initialize the database
        statusCreatifsHistoriqueRepository.saveAndFlush(statusCreatifsHistorique);

        // Get all the statusCreatifsHistoriqueList
        restStatusCreatifsHistoriqueMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(statusCreatifsHistorique.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateModification").value(hasItem(DEFAULT_DATE_MODIFICATION.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    void getStatusCreatifsHistorique() throws Exception {
        // Initialize the database
        statusCreatifsHistoriqueRepository.saveAndFlush(statusCreatifsHistorique);

        // Get the statusCreatifsHistorique
        restStatusCreatifsHistoriqueMockMvc
            .perform(get(ENTITY_API_URL_ID, statusCreatifsHistorique.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(statusCreatifsHistorique.getId().intValue()))
            .andExpect(jsonPath("$.dateModification").value(DEFAULT_DATE_MODIFICATION.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    void getNonExistingStatusCreatifsHistorique() throws Exception {
        // Get the statusCreatifsHistorique
        restStatusCreatifsHistoriqueMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewStatusCreatifsHistorique() throws Exception {
        // Initialize the database
        statusCreatifsHistoriqueRepository.saveAndFlush(statusCreatifsHistorique);

        int databaseSizeBeforeUpdate = statusCreatifsHistoriqueRepository.findAll().size();

        // Update the statusCreatifsHistorique
        StatusCreatifsHistorique updatedStatusCreatifsHistorique = statusCreatifsHistoriqueRepository
            .findById(statusCreatifsHistorique.getId())
            .get();
        // Disconnect from session so that the updates on updatedStatusCreatifsHistorique are not directly saved in db
        em.detach(updatedStatusCreatifsHistorique);
        updatedStatusCreatifsHistorique.dateModification(UPDATED_DATE_MODIFICATION).status(UPDATED_STATUS);

        restStatusCreatifsHistoriqueMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedStatusCreatifsHistorique.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedStatusCreatifsHistorique))
            )
            .andExpect(status().isOk());

        // Validate the StatusCreatifsHistorique in the database
        List<StatusCreatifsHistorique> statusCreatifsHistoriqueList = statusCreatifsHistoriqueRepository.findAll();
        assertThat(statusCreatifsHistoriqueList).hasSize(databaseSizeBeforeUpdate);
        StatusCreatifsHistorique testStatusCreatifsHistorique = statusCreatifsHistoriqueList.get(statusCreatifsHistoriqueList.size() - 1);
        assertThat(testStatusCreatifsHistorique.getDateModification()).isEqualTo(UPDATED_DATE_MODIFICATION);
        assertThat(testStatusCreatifsHistorique.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void putNonExistingStatusCreatifsHistorique() throws Exception {
        int databaseSizeBeforeUpdate = statusCreatifsHistoriqueRepository.findAll().size();
        statusCreatifsHistorique.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restStatusCreatifsHistoriqueMockMvc
            .perform(
                put(ENTITY_API_URL_ID, statusCreatifsHistorique.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(statusCreatifsHistorique))
            )
            .andExpect(status().isBadRequest());

        // Validate the StatusCreatifsHistorique in the database
        List<StatusCreatifsHistorique> statusCreatifsHistoriqueList = statusCreatifsHistoriqueRepository.findAll();
        assertThat(statusCreatifsHistoriqueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchStatusCreatifsHistorique() throws Exception {
        int databaseSizeBeforeUpdate = statusCreatifsHistoriqueRepository.findAll().size();
        statusCreatifsHistorique.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restStatusCreatifsHistoriqueMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(statusCreatifsHistorique))
            )
            .andExpect(status().isBadRequest());

        // Validate the StatusCreatifsHistorique in the database
        List<StatusCreatifsHistorique> statusCreatifsHistoriqueList = statusCreatifsHistoriqueRepository.findAll();
        assertThat(statusCreatifsHistoriqueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamStatusCreatifsHistorique() throws Exception {
        int databaseSizeBeforeUpdate = statusCreatifsHistoriqueRepository.findAll().size();
        statusCreatifsHistorique.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restStatusCreatifsHistoriqueMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(statusCreatifsHistorique))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the StatusCreatifsHistorique in the database
        List<StatusCreatifsHistorique> statusCreatifsHistoriqueList = statusCreatifsHistoriqueRepository.findAll();
        assertThat(statusCreatifsHistoriqueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateStatusCreatifsHistoriqueWithPatch() throws Exception {
        // Initialize the database
        statusCreatifsHistoriqueRepository.saveAndFlush(statusCreatifsHistorique);

        int databaseSizeBeforeUpdate = statusCreatifsHistoriqueRepository.findAll().size();

        // Update the statusCreatifsHistorique using partial update
        StatusCreatifsHistorique partialUpdatedStatusCreatifsHistorique = new StatusCreatifsHistorique();
        partialUpdatedStatusCreatifsHistorique.setId(statusCreatifsHistorique.getId());

        partialUpdatedStatusCreatifsHistorique.dateModification(UPDATED_DATE_MODIFICATION).status(UPDATED_STATUS);

        restStatusCreatifsHistoriqueMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedStatusCreatifsHistorique.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedStatusCreatifsHistorique))
            )
            .andExpect(status().isOk());

        // Validate the StatusCreatifsHistorique in the database
        List<StatusCreatifsHistorique> statusCreatifsHistoriqueList = statusCreatifsHistoriqueRepository.findAll();
        assertThat(statusCreatifsHistoriqueList).hasSize(databaseSizeBeforeUpdate);
        StatusCreatifsHistorique testStatusCreatifsHistorique = statusCreatifsHistoriqueList.get(statusCreatifsHistoriqueList.size() - 1);
        assertThat(testStatusCreatifsHistorique.getDateModification()).isEqualTo(UPDATED_DATE_MODIFICATION);
        assertThat(testStatusCreatifsHistorique.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void fullUpdateStatusCreatifsHistoriqueWithPatch() throws Exception {
        // Initialize the database
        statusCreatifsHistoriqueRepository.saveAndFlush(statusCreatifsHistorique);

        int databaseSizeBeforeUpdate = statusCreatifsHistoriqueRepository.findAll().size();

        // Update the statusCreatifsHistorique using partial update
        StatusCreatifsHistorique partialUpdatedStatusCreatifsHistorique = new StatusCreatifsHistorique();
        partialUpdatedStatusCreatifsHistorique.setId(statusCreatifsHistorique.getId());

        partialUpdatedStatusCreatifsHistorique.dateModification(UPDATED_DATE_MODIFICATION).status(UPDATED_STATUS);

        restStatusCreatifsHistoriqueMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedStatusCreatifsHistorique.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedStatusCreatifsHistorique))
            )
            .andExpect(status().isOk());

        // Validate the StatusCreatifsHistorique in the database
        List<StatusCreatifsHistorique> statusCreatifsHistoriqueList = statusCreatifsHistoriqueRepository.findAll();
        assertThat(statusCreatifsHistoriqueList).hasSize(databaseSizeBeforeUpdate);
        StatusCreatifsHistorique testStatusCreatifsHistorique = statusCreatifsHistoriqueList.get(statusCreatifsHistoriqueList.size() - 1);
        assertThat(testStatusCreatifsHistorique.getDateModification()).isEqualTo(UPDATED_DATE_MODIFICATION);
        assertThat(testStatusCreatifsHistorique.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void patchNonExistingStatusCreatifsHistorique() throws Exception {
        int databaseSizeBeforeUpdate = statusCreatifsHistoriqueRepository.findAll().size();
        statusCreatifsHistorique.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restStatusCreatifsHistoriqueMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, statusCreatifsHistorique.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(statusCreatifsHistorique))
            )
            .andExpect(status().isBadRequest());

        // Validate the StatusCreatifsHistorique in the database
        List<StatusCreatifsHistorique> statusCreatifsHistoriqueList = statusCreatifsHistoriqueRepository.findAll();
        assertThat(statusCreatifsHistoriqueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchStatusCreatifsHistorique() throws Exception {
        int databaseSizeBeforeUpdate = statusCreatifsHistoriqueRepository.findAll().size();
        statusCreatifsHistorique.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restStatusCreatifsHistoriqueMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(statusCreatifsHistorique))
            )
            .andExpect(status().isBadRequest());

        // Validate the StatusCreatifsHistorique in the database
        List<StatusCreatifsHistorique> statusCreatifsHistoriqueList = statusCreatifsHistoriqueRepository.findAll();
        assertThat(statusCreatifsHistoriqueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamStatusCreatifsHistorique() throws Exception {
        int databaseSizeBeforeUpdate = statusCreatifsHistoriqueRepository.findAll().size();
        statusCreatifsHistorique.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restStatusCreatifsHistoriqueMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(statusCreatifsHistorique))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the StatusCreatifsHistorique in the database
        List<StatusCreatifsHistorique> statusCreatifsHistoriqueList = statusCreatifsHistoriqueRepository.findAll();
        assertThat(statusCreatifsHistoriqueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteStatusCreatifsHistorique() throws Exception {
        // Initialize the database
        statusCreatifsHistoriqueRepository.saveAndFlush(statusCreatifsHistorique);

        int databaseSizeBeforeDelete = statusCreatifsHistoriqueRepository.findAll().size();

        // Delete the statusCreatifsHistorique
        restStatusCreatifsHistoriqueMockMvc
            .perform(delete(ENTITY_API_URL_ID, statusCreatifsHistorique.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<StatusCreatifsHistorique> statusCreatifsHistoriqueList = statusCreatifsHistoriqueRepository.findAll();
        assertThat(statusCreatifsHistoriqueList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
