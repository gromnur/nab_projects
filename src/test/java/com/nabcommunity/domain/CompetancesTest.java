package com.nabcommunity.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.nabcommunity.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CompetancesTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Competances.class);
        Competances competances1 = new Competances();
        competances1.setId(1L);
        Competances competances2 = new Competances();
        competances2.setId(competances1.getId());
        assertThat(competances1).isEqualTo(competances2);
        competances2.setId(2L);
        assertThat(competances1).isNotEqualTo(competances2);
        competances1.setId(null);
        assertThat(competances1).isNotEqualTo(competances2);
    }
}
