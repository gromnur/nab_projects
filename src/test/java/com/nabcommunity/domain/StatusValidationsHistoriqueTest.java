package com.nabcommunity.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.nabcommunity.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class StatusValidationsHistoriqueTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(StatusValidationsHistorique.class);
        StatusValidationsHistorique statusValidationsHistorique1 = new StatusValidationsHistorique();
        statusValidationsHistorique1.setId(1L);
        StatusValidationsHistorique statusValidationsHistorique2 = new StatusValidationsHistorique();
        statusValidationsHistorique2.setId(statusValidationsHistorique1.getId());
        assertThat(statusValidationsHistorique1).isEqualTo(statusValidationsHistorique2);
        statusValidationsHistorique2.setId(2L);
        assertThat(statusValidationsHistorique1).isNotEqualTo(statusValidationsHistorique2);
        statusValidationsHistorique1.setId(null);
        assertThat(statusValidationsHistorique1).isNotEqualTo(statusValidationsHistorique2);
    }
}
