package com.nabcommunity.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.nabcommunity.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PrestatairesTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Prestataires.class);
        Prestataires prestataires1 = new Prestataires();
        prestataires1.setId(1L);
        Prestataires prestataires2 = new Prestataires();
        prestataires2.setId(prestataires1.getId());
        assertThat(prestataires1).isEqualTo(prestataires2);
        prestataires2.setId(2L);
        assertThat(prestataires1).isNotEqualTo(prestataires2);
        prestataires1.setId(null);
        assertThat(prestataires1).isNotEqualTo(prestataires2);
    }
}
