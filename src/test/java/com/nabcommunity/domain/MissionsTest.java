package com.nabcommunity.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.nabcommunity.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class MissionsTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Missions.class);
        Missions missions1 = new Missions();
        missions1.setId(1L);
        Missions missions2 = new Missions();
        missions2.setId(missions1.getId());
        assertThat(missions1).isEqualTo(missions2);
        missions2.setId(2L);
        assertThat(missions1).isNotEqualTo(missions2);
        missions1.setId(null);
        assertThat(missions1).isNotEqualTo(missions2);
    }
}
