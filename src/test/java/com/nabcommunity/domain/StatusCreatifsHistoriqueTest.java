package com.nabcommunity.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.nabcommunity.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class StatusCreatifsHistoriqueTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(StatusCreatifsHistorique.class);
        StatusCreatifsHistorique statusCreatifsHistorique1 = new StatusCreatifsHistorique();
        statusCreatifsHistorique1.setId(1L);
        StatusCreatifsHistorique statusCreatifsHistorique2 = new StatusCreatifsHistorique();
        statusCreatifsHistorique2.setId(statusCreatifsHistorique1.getId());
        assertThat(statusCreatifsHistorique1).isEqualTo(statusCreatifsHistorique2);
        statusCreatifsHistorique2.setId(2L);
        assertThat(statusCreatifsHistorique1).isNotEqualTo(statusCreatifsHistorique2);
        statusCreatifsHistorique1.setId(null);
        assertThat(statusCreatifsHistorique1).isNotEqualTo(statusCreatifsHistorique2);
    }
}
