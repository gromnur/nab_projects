package com.nabcommunity.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.nabcommunity.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class EtatMissionsHistoriqueTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EtatMissionsHistorique.class);
        EtatMissionsHistorique etatMissionsHistorique1 = new EtatMissionsHistorique();
        etatMissionsHistorique1.setId(1L);
        EtatMissionsHistorique etatMissionsHistorique2 = new EtatMissionsHistorique();
        etatMissionsHistorique2.setId(etatMissionsHistorique1.getId());
        assertThat(etatMissionsHistorique1).isEqualTo(etatMissionsHistorique2);
        etatMissionsHistorique2.setId(2L);
        assertThat(etatMissionsHistorique1).isNotEqualTo(etatMissionsHistorique2);
        etatMissionsHistorique1.setId(null);
        assertThat(etatMissionsHistorique1).isNotEqualTo(etatMissionsHistorique2);
    }
}
