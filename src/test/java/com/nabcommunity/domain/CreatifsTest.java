package com.nabcommunity.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.nabcommunity.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CreatifsTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Creatifs.class);
        Creatifs creatifs1 = new Creatifs();
        creatifs1.setId(1L);
        Creatifs creatifs2 = new Creatifs();
        creatifs2.setId(creatifs1.getId());
        assertThat(creatifs1).isEqualTo(creatifs2);
        creatifs2.setId(2L);
        assertThat(creatifs1).isNotEqualTo(creatifs2);
        creatifs1.setId(null);
        assertThat(creatifs1).isNotEqualTo(creatifs2);
    }
}
