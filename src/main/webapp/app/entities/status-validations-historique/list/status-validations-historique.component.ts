import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IStatusValidationsHistorique } from '../status-validations-historique.model';
import { StatusValidationsHistoriqueService } from '../service/status-validations-historique.service';
import { StatusValidationsHistoriqueDeleteDialogComponent } from '../delete/status-validations-historique-delete-dialog.component';

@Component({
  selector: 'jhi-status-validations-historique',
  templateUrl: './status-validations-historique.component.html',
})
export class StatusValidationsHistoriqueComponent implements OnInit {
  statusValidationsHistoriques?: IStatusValidationsHistorique[];
  isLoading = false;

  constructor(protected statusValidationsHistoriqueService: StatusValidationsHistoriqueService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.statusValidationsHistoriqueService.query().subscribe(
      (res: HttpResponse<IStatusValidationsHistorique[]>) => {
        this.isLoading = false;
        this.statusValidationsHistoriques = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IStatusValidationsHistorique): number {
    return item.id!;
  }

  delete(statusValidationsHistorique: IStatusValidationsHistorique): void {
    const modalRef = this.modalService.open(StatusValidationsHistoriqueDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.statusValidationsHistorique = statusValidationsHistorique;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
