import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { StatusValidationsHistoriqueService } from '../service/status-validations-historique.service';

import { StatusValidationsHistoriqueComponent } from './status-validations-historique.component';

describe('StatusValidationsHistorique Management Component', () => {
  let comp: StatusValidationsHistoriqueComponent;
  let fixture: ComponentFixture<StatusValidationsHistoriqueComponent>;
  let service: StatusValidationsHistoriqueService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [StatusValidationsHistoriqueComponent],
    })
      .overrideTemplate(StatusValidationsHistoriqueComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(StatusValidationsHistoriqueComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(StatusValidationsHistoriqueService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.statusValidationsHistoriques?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
