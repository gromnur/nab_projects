import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IStatusValidationsHistorique, StatusValidationsHistorique } from '../status-validations-historique.model';
import { StatusValidationsHistoriqueService } from '../service/status-validations-historique.service';

@Injectable({ providedIn: 'root' })
export class StatusValidationsHistoriqueRoutingResolveService implements Resolve<IStatusValidationsHistorique> {
  constructor(protected service: StatusValidationsHistoriqueService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IStatusValidationsHistorique> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((statusValidationsHistorique: HttpResponse<StatusValidationsHistorique>) => {
          if (statusValidationsHistorique.body) {
            return of(statusValidationsHistorique.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new StatusValidationsHistorique());
  }
}
