import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { StatusValidationsHistoriqueComponent } from '../list/status-validations-historique.component';
import { StatusValidationsHistoriqueDetailComponent } from '../detail/status-validations-historique-detail.component';
import { StatusValidationsHistoriqueUpdateComponent } from '../update/status-validations-historique-update.component';
import { StatusValidationsHistoriqueRoutingResolveService } from './status-validations-historique-routing-resolve.service';

const statusValidationsHistoriqueRoute: Routes = [
  {
    path: '',
    component: StatusValidationsHistoriqueComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: StatusValidationsHistoriqueDetailComponent,
    resolve: {
      statusValidationsHistorique: StatusValidationsHistoriqueRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: StatusValidationsHistoriqueUpdateComponent,
    resolve: {
      statusValidationsHistorique: StatusValidationsHistoriqueRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: StatusValidationsHistoriqueUpdateComponent,
    resolve: {
      statusValidationsHistorique: StatusValidationsHistoriqueRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(statusValidationsHistoriqueRoute)],
  exports: [RouterModule],
})
export class StatusValidationsHistoriqueRoutingModule {}
