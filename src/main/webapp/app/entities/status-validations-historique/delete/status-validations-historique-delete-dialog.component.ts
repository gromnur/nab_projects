import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IStatusValidationsHistorique } from '../status-validations-historique.model';
import { StatusValidationsHistoriqueService } from '../service/status-validations-historique.service';

@Component({
  templateUrl: './status-validations-historique-delete-dialog.component.html',
})
export class StatusValidationsHistoriqueDeleteDialogComponent {
  statusValidationsHistorique?: IStatusValidationsHistorique;

  constructor(protected statusValidationsHistoriqueService: StatusValidationsHistoriqueService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.statusValidationsHistoriqueService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
