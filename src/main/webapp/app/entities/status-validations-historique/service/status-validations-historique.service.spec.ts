import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_FORMAT } from 'app/config/input.constants';
import { StatusValidations } from 'app/entities/enumerations/status-validations.model';
import { IStatusValidationsHistorique, StatusValidationsHistorique } from '../status-validations-historique.model';

import { StatusValidationsHistoriqueService } from './status-validations-historique.service';

describe('StatusValidationsHistorique Service', () => {
  let service: StatusValidationsHistoriqueService;
  let httpMock: HttpTestingController;
  let elemDefault: IStatusValidationsHistorique;
  let expectedResult: IStatusValidationsHistorique | IStatusValidationsHistorique[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(StatusValidationsHistoriqueService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      dateModifiacation: currentDate,
      status: StatusValidations.VALIDE,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          dateModifiacation: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a StatusValidationsHistorique', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          dateModifiacation: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateModifiacation: currentDate,
        },
        returnedFromService
      );

      service.create(new StatusValidationsHistorique()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a StatusValidationsHistorique', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          dateModifiacation: currentDate.format(DATE_FORMAT),
          status: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateModifiacation: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a StatusValidationsHistorique', () => {
      const patchObject = Object.assign({}, new StatusValidationsHistorique());

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          dateModifiacation: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of StatusValidationsHistorique', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          dateModifiacation: currentDate.format(DATE_FORMAT),
          status: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateModifiacation: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a StatusValidationsHistorique', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addStatusValidationsHistoriqueToCollectionIfMissing', () => {
      it('should add a StatusValidationsHistorique to an empty array', () => {
        const statusValidationsHistorique: IStatusValidationsHistorique = { id: 123 };
        expectedResult = service.addStatusValidationsHistoriqueToCollectionIfMissing([], statusValidationsHistorique);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(statusValidationsHistorique);
      });

      it('should not add a StatusValidationsHistorique to an array that contains it', () => {
        const statusValidationsHistorique: IStatusValidationsHistorique = { id: 123 };
        const statusValidationsHistoriqueCollection: IStatusValidationsHistorique[] = [
          {
            ...statusValidationsHistorique,
          },
          { id: 456 },
        ];
        expectedResult = service.addStatusValidationsHistoriqueToCollectionIfMissing(
          statusValidationsHistoriqueCollection,
          statusValidationsHistorique
        );
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a StatusValidationsHistorique to an array that doesn't contain it", () => {
        const statusValidationsHistorique: IStatusValidationsHistorique = { id: 123 };
        const statusValidationsHistoriqueCollection: IStatusValidationsHistorique[] = [{ id: 456 }];
        expectedResult = service.addStatusValidationsHistoriqueToCollectionIfMissing(
          statusValidationsHistoriqueCollection,
          statusValidationsHistorique
        );
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(statusValidationsHistorique);
      });

      it('should add only unique StatusValidationsHistorique to an array', () => {
        const statusValidationsHistoriqueArray: IStatusValidationsHistorique[] = [{ id: 123 }, { id: 456 }, { id: 85743 }];
        const statusValidationsHistoriqueCollection: IStatusValidationsHistorique[] = [{ id: 123 }];
        expectedResult = service.addStatusValidationsHistoriqueToCollectionIfMissing(
          statusValidationsHistoriqueCollection,
          ...statusValidationsHistoriqueArray
        );
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const statusValidationsHistorique: IStatusValidationsHistorique = { id: 123 };
        const statusValidationsHistorique2: IStatusValidationsHistorique = { id: 456 };
        expectedResult = service.addStatusValidationsHistoriqueToCollectionIfMissing(
          [],
          statusValidationsHistorique,
          statusValidationsHistorique2
        );
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(statusValidationsHistorique);
        expect(expectedResult).toContain(statusValidationsHistorique2);
      });

      it('should accept null and undefined values', () => {
        const statusValidationsHistorique: IStatusValidationsHistorique = { id: 123 };
        expectedResult = service.addStatusValidationsHistoriqueToCollectionIfMissing([], null, statusValidationsHistorique, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(statusValidationsHistorique);
      });

      it('should return initial array if no StatusValidationsHistorique is added', () => {
        const statusValidationsHistoriqueCollection: IStatusValidationsHistorique[] = [{ id: 123 }];
        expectedResult = service.addStatusValidationsHistoriqueToCollectionIfMissing(
          statusValidationsHistoriqueCollection,
          undefined,
          null
        );
        expect(expectedResult).toEqual(statusValidationsHistoriqueCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
