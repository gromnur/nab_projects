import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IStatusValidationsHistorique, getStatusValidationsHistoriqueIdentifier } from '../status-validations-historique.model';

export type EntityResponseType = HttpResponse<IStatusValidationsHistorique>;
export type EntityArrayResponseType = HttpResponse<IStatusValidationsHistorique[]>;

@Injectable({ providedIn: 'root' })
export class StatusValidationsHistoriqueService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/status-validations-historiques');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(statusValidationsHistorique: IStatusValidationsHistorique): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(statusValidationsHistorique);
    return this.http
      .post<IStatusValidationsHistorique>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(statusValidationsHistorique: IStatusValidationsHistorique): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(statusValidationsHistorique);
    return this.http
      .put<IStatusValidationsHistorique>(
        `${this.resourceUrl}/${getStatusValidationsHistoriqueIdentifier(statusValidationsHistorique) as number}`,
        copy,
        { observe: 'response' }
      )
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(statusValidationsHistorique: IStatusValidationsHistorique): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(statusValidationsHistorique);
    return this.http
      .patch<IStatusValidationsHistorique>(
        `${this.resourceUrl}/${getStatusValidationsHistoriqueIdentifier(statusValidationsHistorique) as number}`,
        copy,
        { observe: 'response' }
      )
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IStatusValidationsHistorique>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IStatusValidationsHistorique[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addStatusValidationsHistoriqueToCollectionIfMissing(
    statusValidationsHistoriqueCollection: IStatusValidationsHistorique[],
    ...statusValidationsHistoriquesToCheck: (IStatusValidationsHistorique | null | undefined)[]
  ): IStatusValidationsHistorique[] {
    const statusValidationsHistoriques: IStatusValidationsHistorique[] = statusValidationsHistoriquesToCheck.filter(isPresent);
    if (statusValidationsHistoriques.length > 0) {
      const statusValidationsHistoriqueCollectionIdentifiers = statusValidationsHistoriqueCollection.map(
        statusValidationsHistoriqueItem => getStatusValidationsHistoriqueIdentifier(statusValidationsHistoriqueItem)!
      );
      const statusValidationsHistoriquesToAdd = statusValidationsHistoriques.filter(statusValidationsHistoriqueItem => {
        const statusValidationsHistoriqueIdentifier = getStatusValidationsHistoriqueIdentifier(statusValidationsHistoriqueItem);
        if (
          statusValidationsHistoriqueIdentifier == null ||
          statusValidationsHistoriqueCollectionIdentifiers.includes(statusValidationsHistoriqueIdentifier)
        ) {
          return false;
        }
        statusValidationsHistoriqueCollectionIdentifiers.push(statusValidationsHistoriqueIdentifier);
        return true;
      });
      return [...statusValidationsHistoriquesToAdd, ...statusValidationsHistoriqueCollection];
    }
    return statusValidationsHistoriqueCollection;
  }

  protected convertDateFromClient(statusValidationsHistorique: IStatusValidationsHistorique): IStatusValidationsHistorique {
    return Object.assign({}, statusValidationsHistorique, {
      dateModifiacation: statusValidationsHistorique.dateModifiacation?.isValid()
        ? statusValidationsHistorique.dateModifiacation.format(DATE_FORMAT)
        : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateModifiacation = res.body.dateModifiacation ? dayjs(res.body.dateModifiacation) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((statusValidationsHistorique: IStatusValidationsHistorique) => {
        statusValidationsHistorique.dateModifiacation = statusValidationsHistorique.dateModifiacation
          ? dayjs(statusValidationsHistorique.dateModifiacation)
          : undefined;
      });
    }
    return res;
  }
}
