import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { StatusValidationsHistoriqueDetailComponent } from './status-validations-historique-detail.component';

describe('StatusValidationsHistorique Management Detail Component', () => {
  let comp: StatusValidationsHistoriqueDetailComponent;
  let fixture: ComponentFixture<StatusValidationsHistoriqueDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [StatusValidationsHistoriqueDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ statusValidationsHistorique: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(StatusValidationsHistoriqueDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(StatusValidationsHistoriqueDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load statusValidationsHistorique on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.statusValidationsHistorique).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
