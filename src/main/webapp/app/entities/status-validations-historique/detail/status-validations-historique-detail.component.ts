import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IStatusValidationsHistorique } from '../status-validations-historique.model';

@Component({
  selector: 'jhi-status-validations-historique-detail',
  templateUrl: './status-validations-historique-detail.component.html',
})
export class StatusValidationsHistoriqueDetailComponent implements OnInit {
  statusValidationsHistorique: IStatusValidationsHistorique | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ statusValidationsHistorique }) => {
      this.statusValidationsHistorique = statusValidationsHistorique;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
