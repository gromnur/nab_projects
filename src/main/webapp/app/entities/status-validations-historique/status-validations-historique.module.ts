import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { StatusValidationsHistoriqueComponent } from './list/status-validations-historique.component';
import { StatusValidationsHistoriqueDetailComponent } from './detail/status-validations-historique-detail.component';
import { StatusValidationsHistoriqueUpdateComponent } from './update/status-validations-historique-update.component';
import { StatusValidationsHistoriqueDeleteDialogComponent } from './delete/status-validations-historique-delete-dialog.component';
import { StatusValidationsHistoriqueRoutingModule } from './route/status-validations-historique-routing.module';

@NgModule({
  imports: [SharedModule, StatusValidationsHistoriqueRoutingModule],
  declarations: [
    StatusValidationsHistoriqueComponent,
    StatusValidationsHistoriqueDetailComponent,
    StatusValidationsHistoriqueUpdateComponent,
    StatusValidationsHistoriqueDeleteDialogComponent,
  ],
  entryComponents: [StatusValidationsHistoriqueDeleteDialogComponent],
})
export class StatusValidationsHistoriqueModule {}
