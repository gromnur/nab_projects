import * as dayjs from 'dayjs';
import { IMissions } from 'app/entities/missions/missions.model';
import { ICreatifs } from 'app/entities/creatifs/creatifs.model';
import { StatusValidations } from 'app/entities/enumerations/status-validations.model';

export interface IStatusValidationsHistorique {
  id?: number;
  dateModifiacation?: dayjs.Dayjs | null;
  status?: StatusValidations | null;
  missions?: IMissions[] | null;
  creatifs?: ICreatifs[] | null;
}

export class StatusValidationsHistorique implements IStatusValidationsHistorique {
  constructor(
    public id?: number,
    public dateModifiacation?: dayjs.Dayjs | null,
    public status?: StatusValidations | null,
    public missions?: IMissions[] | null,
    public creatifs?: ICreatifs[] | null
  ) {}
}

export function getStatusValidationsHistoriqueIdentifier(statusValidationsHistorique: IStatusValidationsHistorique): number | undefined {
  return statusValidationsHistorique.id;
}
