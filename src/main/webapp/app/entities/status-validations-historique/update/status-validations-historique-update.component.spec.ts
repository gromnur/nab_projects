jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { StatusValidationsHistoriqueService } from '../service/status-validations-historique.service';
import { IStatusValidationsHistorique, StatusValidationsHistorique } from '../status-validations-historique.model';

import { StatusValidationsHistoriqueUpdateComponent } from './status-validations-historique-update.component';

describe('StatusValidationsHistorique Management Update Component', () => {
  let comp: StatusValidationsHistoriqueUpdateComponent;
  let fixture: ComponentFixture<StatusValidationsHistoriqueUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let statusValidationsHistoriqueService: StatusValidationsHistoriqueService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [StatusValidationsHistoriqueUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(StatusValidationsHistoriqueUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(StatusValidationsHistoriqueUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    statusValidationsHistoriqueService = TestBed.inject(StatusValidationsHistoriqueService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const statusValidationsHistorique: IStatusValidationsHistorique = { id: 456 };

      activatedRoute.data = of({ statusValidationsHistorique });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(statusValidationsHistorique));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<StatusValidationsHistorique>>();
      const statusValidationsHistorique = { id: 123 };
      jest.spyOn(statusValidationsHistoriqueService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ statusValidationsHistorique });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: statusValidationsHistorique }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(statusValidationsHistoriqueService.update).toHaveBeenCalledWith(statusValidationsHistorique);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<StatusValidationsHistorique>>();
      const statusValidationsHistorique = new StatusValidationsHistorique();
      jest.spyOn(statusValidationsHistoriqueService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ statusValidationsHistorique });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: statusValidationsHistorique }));
      saveSubject.complete();

      // THEN
      expect(statusValidationsHistoriqueService.create).toHaveBeenCalledWith(statusValidationsHistorique);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<StatusValidationsHistorique>>();
      const statusValidationsHistorique = { id: 123 };
      jest.spyOn(statusValidationsHistoriqueService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ statusValidationsHistorique });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(statusValidationsHistoriqueService.update).toHaveBeenCalledWith(statusValidationsHistorique);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
