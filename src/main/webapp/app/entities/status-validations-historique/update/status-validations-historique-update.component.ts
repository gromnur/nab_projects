import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IStatusValidationsHistorique, StatusValidationsHistorique } from '../status-validations-historique.model';
import { StatusValidationsHistoriqueService } from '../service/status-validations-historique.service';
import { StatusValidations } from 'app/entities/enumerations/status-validations.model';

@Component({
  selector: 'jhi-status-validations-historique-update',
  templateUrl: './status-validations-historique-update.component.html',
})
export class StatusValidationsHistoriqueUpdateComponent implements OnInit {
  isSaving = false;
  statusValidationsValues = Object.keys(StatusValidations);

  editForm = this.fb.group({
    id: [],
    dateModifiacation: [],
    status: [],
  });

  constructor(
    protected statusValidationsHistoriqueService: StatusValidationsHistoriqueService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ statusValidationsHistorique }) => {
      this.updateForm(statusValidationsHistorique);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const statusValidationsHistorique = this.createFromForm();
    if (statusValidationsHistorique.id !== undefined) {
      this.subscribeToSaveResponse(this.statusValidationsHistoriqueService.update(statusValidationsHistorique));
    } else {
      this.subscribeToSaveResponse(this.statusValidationsHistoriqueService.create(statusValidationsHistorique));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IStatusValidationsHistorique>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(statusValidationsHistorique: IStatusValidationsHistorique): void {
    this.editForm.patchValue({
      id: statusValidationsHistorique.id,
      dateModifiacation: statusValidationsHistorique.dateModifiacation,
      status: statusValidationsHistorique.status,
    });
  }

  protected createFromForm(): IStatusValidationsHistorique {
    return {
      ...new StatusValidationsHistorique(),
      id: this.editForm.get(['id'])!.value,
      dateModifiacation: this.editForm.get(['dateModifiacation'])!.value,
      status: this.editForm.get(['status'])!.value,
    };
  }
}
