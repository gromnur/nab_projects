import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { CompetancesComponent } from './list/competances.component';
import { CompetancesDetailComponent } from './detail/competances-detail.component';
import { CompetancesUpdateComponent } from './update/competances-update.component';
import { CompetancesDeleteDialogComponent } from './delete/competances-delete-dialog.component';
import { CompetancesRoutingModule } from './route/competances-routing.module';

@NgModule({
  imports: [SharedModule, CompetancesRoutingModule],
  declarations: [CompetancesComponent, CompetancesDetailComponent, CompetancesUpdateComponent, CompetancesDeleteDialogComponent],
  entryComponents: [CompetancesDeleteDialogComponent],
})
export class CompetancesModule {}
