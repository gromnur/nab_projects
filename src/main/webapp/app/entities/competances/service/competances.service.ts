import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICompetances, getCompetancesIdentifier } from '../competances.model';

export type EntityResponseType = HttpResponse<ICompetances>;
export type EntityArrayResponseType = HttpResponse<ICompetances[]>;

@Injectable({ providedIn: 'root' })
export class CompetancesService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/competances');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(competances: ICompetances): Observable<EntityResponseType> {
    return this.http.post<ICompetances>(this.resourceUrl, competances, { observe: 'response' });
  }

  update(competances: ICompetances): Observable<EntityResponseType> {
    return this.http.put<ICompetances>(`${this.resourceUrl}/${getCompetancesIdentifier(competances) as number}`, competances, {
      observe: 'response',
    });
  }

  partialUpdate(competances: ICompetances): Observable<EntityResponseType> {
    return this.http.patch<ICompetances>(`${this.resourceUrl}/${getCompetancesIdentifier(competances) as number}`, competances, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICompetances>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICompetances[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addCompetancesToCollectionIfMissing(
    competancesCollection: ICompetances[],
    ...competancesToCheck: (ICompetances | null | undefined)[]
  ): ICompetances[] {
    const competances: ICompetances[] = competancesToCheck.filter(isPresent);
    if (competances.length > 0) {
      const competancesCollectionIdentifiers = competancesCollection.map(competancesItem => getCompetancesIdentifier(competancesItem)!);
      const competancesToAdd = competances.filter(competancesItem => {
        const competancesIdentifier = getCompetancesIdentifier(competancesItem);
        if (competancesIdentifier == null || competancesCollectionIdentifiers.includes(competancesIdentifier)) {
          return false;
        }
        competancesCollectionIdentifiers.push(competancesIdentifier);
        return true;
      });
      return [...competancesToAdd, ...competancesCollection];
    }
    return competancesCollection;
  }
}
