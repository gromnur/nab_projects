import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ICompetances, Competances } from '../competances.model';

import { CompetancesService } from './competances.service';

describe('Competances Service', () => {
  let service: CompetancesService;
  let httpMock: HttpTestingController;
  let elemDefault: ICompetances;
  let expectedResult: ICompetances | ICompetances[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(CompetancesService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      nom: 'AAAAAAA',
      description: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Competances', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new Competances()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Competances', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nom: 'BBBBBB',
          description: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Competances', () => {
      const patchObject = Object.assign(
        {
          description: 'BBBBBB',
        },
        new Competances()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Competances', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nom: 'BBBBBB',
          description: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Competances', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addCompetancesToCollectionIfMissing', () => {
      it('should add a Competances to an empty array', () => {
        const competances: ICompetances = { id: 123 };
        expectedResult = service.addCompetancesToCollectionIfMissing([], competances);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(competances);
      });

      it('should not add a Competances to an array that contains it', () => {
        const competances: ICompetances = { id: 123 };
        const competancesCollection: ICompetances[] = [
          {
            ...competances,
          },
          { id: 456 },
        ];
        expectedResult = service.addCompetancesToCollectionIfMissing(competancesCollection, competances);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Competances to an array that doesn't contain it", () => {
        const competances: ICompetances = { id: 123 };
        const competancesCollection: ICompetances[] = [{ id: 456 }];
        expectedResult = service.addCompetancesToCollectionIfMissing(competancesCollection, competances);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(competances);
      });

      it('should add only unique Competances to an array', () => {
        const competancesArray: ICompetances[] = [{ id: 123 }, { id: 456 }, { id: 45624 }];
        const competancesCollection: ICompetances[] = [{ id: 123 }];
        expectedResult = service.addCompetancesToCollectionIfMissing(competancesCollection, ...competancesArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const competances: ICompetances = { id: 123 };
        const competances2: ICompetances = { id: 456 };
        expectedResult = service.addCompetancesToCollectionIfMissing([], competances, competances2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(competances);
        expect(expectedResult).toContain(competances2);
      });

      it('should accept null and undefined values', () => {
        const competances: ICompetances = { id: 123 };
        expectedResult = service.addCompetancesToCollectionIfMissing([], null, competances, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(competances);
      });

      it('should return initial array if no Competances is added', () => {
        const competancesCollection: ICompetances[] = [{ id: 123 }];
        expectedResult = service.addCompetancesToCollectionIfMissing(competancesCollection, undefined, null);
        expect(expectedResult).toEqual(competancesCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
