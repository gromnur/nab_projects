import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ICompetances } from '../competances.model';
import { CompetancesService } from '../service/competances.service';

@Component({
  templateUrl: './competances-delete-dialog.component.html',
})
export class CompetancesDeleteDialogComponent {
  competances?: ICompetances;

  constructor(protected competancesService: CompetancesService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.competancesService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
