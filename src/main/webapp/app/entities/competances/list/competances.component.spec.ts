import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { CompetancesService } from '../service/competances.service';

import { CompetancesComponent } from './competances.component';

describe('Competances Management Component', () => {
  let comp: CompetancesComponent;
  let fixture: ComponentFixture<CompetancesComponent>;
  let service: CompetancesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [CompetancesComponent],
    })
      .overrideTemplate(CompetancesComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CompetancesComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(CompetancesService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.competances?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
