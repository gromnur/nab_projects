import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICompetances } from '../competances.model';
import { CompetancesService } from '../service/competances.service';
import { CompetancesDeleteDialogComponent } from '../delete/competances-delete-dialog.component';

@Component({
  selector: 'jhi-competances',
  templateUrl: './competances.component.html',
})
export class CompetancesComponent implements OnInit {
  competances?: ICompetances[];
  isLoading = false;

  constructor(protected competancesService: CompetancesService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.competancesService.query().subscribe(
      (res: HttpResponse<ICompetances[]>) => {
        this.isLoading = false;
        this.competances = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: ICompetances): number {
    return item.id!;
  }

  delete(competances: ICompetances): void {
    const modalRef = this.modalService.open(CompetancesDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.competances = competances;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
