import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICompetances } from '../competances.model';

@Component({
  selector: 'jhi-competances-detail',
  templateUrl: './competances-detail.component.html',
})
export class CompetancesDetailComponent implements OnInit {
  competances: ICompetances | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ competances }) => {
      this.competances = competances;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
