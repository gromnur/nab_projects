import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CompetancesDetailComponent } from './competances-detail.component';

describe('Competances Management Detail Component', () => {
  let comp: CompetancesDetailComponent;
  let fixture: ComponentFixture<CompetancesDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CompetancesDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ competances: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(CompetancesDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(CompetancesDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load competances on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.competances).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
