jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { CompetancesService } from '../service/competances.service';
import { ICompetances, Competances } from '../competances.model';
import { ICreatifs } from 'app/entities/creatifs/creatifs.model';
import { CreatifsService } from 'app/entities/creatifs/service/creatifs.service';
import { IMissions } from 'app/entities/missions/missions.model';
import { MissionsService } from 'app/entities/missions/service/missions.service';

import { CompetancesUpdateComponent } from './competances-update.component';

describe('Competances Management Update Component', () => {
  let comp: CompetancesUpdateComponent;
  let fixture: ComponentFixture<CompetancesUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let competancesService: CompetancesService;
  let creatifsService: CreatifsService;
  let missionsService: MissionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [CompetancesUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(CompetancesUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CompetancesUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    competancesService = TestBed.inject(CompetancesService);
    creatifsService = TestBed.inject(CreatifsService);
    missionsService = TestBed.inject(MissionsService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Competances query and add missing value', () => {
      const competances: ICompetances = { id: 456 };
      const parent: ICompetances = { id: 392 };
      competances.parent = parent;

      const competancesCollection: ICompetances[] = [{ id: 92755 }];
      jest.spyOn(competancesService, 'query').mockReturnValue(of(new HttpResponse({ body: competancesCollection })));
      const additionalCompetances = [parent];
      const expectedCollection: ICompetances[] = [...additionalCompetances, ...competancesCollection];
      jest.spyOn(competancesService, 'addCompetancesToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ competances });
      comp.ngOnInit();

      expect(competancesService.query).toHaveBeenCalled();
      expect(competancesService.addCompetancesToCollectionIfMissing).toHaveBeenCalledWith(competancesCollection, ...additionalCompetances);
      expect(comp.competancesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Creatifs query and add missing value', () => {
      const competances: ICompetances = { id: 456 };
      const creatifs: ICreatifs[] = [{ id: 9542 }];
      competances.creatifs = creatifs;

      const creatifsCollection: ICreatifs[] = [{ id: 34064 }];
      jest.spyOn(creatifsService, 'query').mockReturnValue(of(new HttpResponse({ body: creatifsCollection })));
      const additionalCreatifs = [...creatifs];
      const expectedCollection: ICreatifs[] = [...additionalCreatifs, ...creatifsCollection];
      jest.spyOn(creatifsService, 'addCreatifsToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ competances });
      comp.ngOnInit();

      expect(creatifsService.query).toHaveBeenCalled();
      expect(creatifsService.addCreatifsToCollectionIfMissing).toHaveBeenCalledWith(creatifsCollection, ...additionalCreatifs);
      expect(comp.creatifsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Missions query and add missing value', () => {
      const competances: ICompetances = { id: 456 };
      const missions: IMissions[] = [{ id: 913 }];
      competances.missions = missions;

      const missionsCollection: IMissions[] = [{ id: 68883 }];
      jest.spyOn(missionsService, 'query').mockReturnValue(of(new HttpResponse({ body: missionsCollection })));
      const additionalMissions = [...missions];
      const expectedCollection: IMissions[] = [...additionalMissions, ...missionsCollection];
      jest.spyOn(missionsService, 'addMissionsToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ competances });
      comp.ngOnInit();

      expect(missionsService.query).toHaveBeenCalled();
      expect(missionsService.addMissionsToCollectionIfMissing).toHaveBeenCalledWith(missionsCollection, ...additionalMissions);
      expect(comp.missionsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const competances: ICompetances = { id: 456 };
      const parent: ICompetances = { id: 29938 };
      competances.parent = parent;
      const creatifs: ICreatifs = { id: 92874 };
      competances.creatifs = [creatifs];
      const missions: IMissions = { id: 1048 };
      competances.missions = [missions];

      activatedRoute.data = of({ competances });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(competances));
      expect(comp.competancesSharedCollection).toContain(parent);
      expect(comp.creatifsSharedCollection).toContain(creatifs);
      expect(comp.missionsSharedCollection).toContain(missions);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Competances>>();
      const competances = { id: 123 };
      jest.spyOn(competancesService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ competances });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: competances }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(competancesService.update).toHaveBeenCalledWith(competances);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Competances>>();
      const competances = new Competances();
      jest.spyOn(competancesService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ competances });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: competances }));
      saveSubject.complete();

      // THEN
      expect(competancesService.create).toHaveBeenCalledWith(competances);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Competances>>();
      const competances = { id: 123 };
      jest.spyOn(competancesService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ competances });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(competancesService.update).toHaveBeenCalledWith(competances);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackCompetancesById', () => {
      it('Should return tracked Competances primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCompetancesById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackCreatifsById', () => {
      it('Should return tracked Creatifs primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCreatifsById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackMissionsById', () => {
      it('Should return tracked Missions primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackMissionsById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });

  describe('Getting selected relationships', () => {
    describe('getSelectedCreatifs', () => {
      it('Should return option if no Creatifs is selected', () => {
        const option = { id: 123 };
        const result = comp.getSelectedCreatifs(option);
        expect(result === option).toEqual(true);
      });

      it('Should return selected Creatifs for according option', () => {
        const option = { id: 123 };
        const selected = { id: 123 };
        const selected2 = { id: 456 };
        const result = comp.getSelectedCreatifs(option, [selected2, selected]);
        expect(result === selected).toEqual(true);
        expect(result === selected2).toEqual(false);
        expect(result === option).toEqual(false);
      });

      it('Should return option if this Creatifs is not selected', () => {
        const option = { id: 123 };
        const selected = { id: 456 };
        const result = comp.getSelectedCreatifs(option, [selected]);
        expect(result === option).toEqual(true);
        expect(result === selected).toEqual(false);
      });
    });

    describe('getSelectedMissions', () => {
      it('Should return option if no Missions is selected', () => {
        const option = { id: 123 };
        const result = comp.getSelectedMissions(option);
        expect(result === option).toEqual(true);
      });

      it('Should return selected Missions for according option', () => {
        const option = { id: 123 };
        const selected = { id: 123 };
        const selected2 = { id: 456 };
        const result = comp.getSelectedMissions(option, [selected2, selected]);
        expect(result === selected).toEqual(true);
        expect(result === selected2).toEqual(false);
        expect(result === option).toEqual(false);
      });

      it('Should return option if this Missions is not selected', () => {
        const option = { id: 123 };
        const selected = { id: 456 };
        const result = comp.getSelectedMissions(option, [selected]);
        expect(result === option).toEqual(true);
        expect(result === selected).toEqual(false);
      });
    });
  });
});
