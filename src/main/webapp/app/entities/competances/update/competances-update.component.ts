import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ICompetances, Competances } from '../competances.model';
import { CompetancesService } from '../service/competances.service';
import { ICreatifs } from 'app/entities/creatifs/creatifs.model';
import { CreatifsService } from 'app/entities/creatifs/service/creatifs.service';
import { IMissions } from 'app/entities/missions/missions.model';
import { MissionsService } from 'app/entities/missions/service/missions.service';

@Component({
  selector: 'jhi-competances-update',
  templateUrl: './competances-update.component.html',
})
export class CompetancesUpdateComponent implements OnInit {
  isSaving = false;

  competancesSharedCollection: ICompetances[] = [];
  creatifsSharedCollection: ICreatifs[] = [];
  missionsSharedCollection: IMissions[] = [];

  editForm = this.fb.group({
    id: [],
    nom: [null, [Validators.maxLength(50)]],
    description: [],
    missions: [],
    creatifs: [],
    parent: [],
  });

  constructor(
    protected competancesService: CompetancesService,
    protected creatifsService: CreatifsService,
    protected missionsService: MissionsService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ competances }) => {
      this.updateForm(competances);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const competances = this.createFromForm();
    if (competances.id !== undefined) {
      this.subscribeToSaveResponse(this.competancesService.update(competances));
    } else {
      this.subscribeToSaveResponse(this.competancesService.create(competances));
    }
  }

  trackCompetancesById(index: number, item: ICompetances): number {
    return item.id!;
  }

  trackCreatifsById(index: number, item: ICreatifs): number {
    return item.id!;
  }

  trackMissionsById(index: number, item: IMissions): number {
    return item.id!;
  }

  getSelectedCreatifs(option: ICreatifs, selectedVals?: ICreatifs[]): ICreatifs {
    if (selectedVals) {
      for (const selectedVal of selectedVals) {
        if (option.id === selectedVal.id) {
          return selectedVal;
        }
      }
    }
    return option;
  }

  getSelectedMissions(option: IMissions, selectedVals?: IMissions[]): IMissions {
    if (selectedVals) {
      for (const selectedVal of selectedVals) {
        if (option.id === selectedVal.id) {
          return selectedVal;
        }
      }
    }
    return option;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICompetances>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(competances: ICompetances): void {
    this.editForm.patchValue({
      id: competances.id,
      nom: competances.nom,
      description: competances.description,
      missions: competances.missions,
      creatifs: competances.creatifs,
      parent: competances.parent,
    });

    this.competancesSharedCollection = this.competancesService.addCompetancesToCollectionIfMissing(
      this.competancesSharedCollection,
      competances.parent
    );
    this.creatifsSharedCollection = this.creatifsService.addCreatifsToCollectionIfMissing(
      this.creatifsSharedCollection,
      ...(competances.creatifs ?? [])
    );
    this.missionsSharedCollection = this.missionsService.addMissionsToCollectionIfMissing(
      this.missionsSharedCollection,
      ...(competances.missions ?? [])
    );
  }

  protected loadRelationshipsOptions(): void {
    this.competancesService
      .query()
      .pipe(map((res: HttpResponse<ICompetances[]>) => res.body ?? []))
      .pipe(
        map((competances: ICompetances[]) =>
          this.competancesService.addCompetancesToCollectionIfMissing(competances, this.editForm.get('parent')!.value)
        )
      )
      .subscribe((competances: ICompetances[]) => (this.competancesSharedCollection = competances));

    this.creatifsService
      .query()
      .pipe(map((res: HttpResponse<ICreatifs[]>) => res.body ?? []))
      .pipe(
        map((creatifs: ICreatifs[]) =>
          this.creatifsService.addCreatifsToCollectionIfMissing(creatifs, ...(this.editForm.get('creatifs')!.value ?? []))
        )
      )
      .subscribe((creatifs: ICreatifs[]) => (this.creatifsSharedCollection = creatifs));

    this.missionsService
      .query()
      .pipe(map((res: HttpResponse<IMissions[]>) => res.body ?? []))
      .pipe(
        map((missions: IMissions[]) =>
          this.missionsService.addMissionsToCollectionIfMissing(missions, ...(this.editForm.get('missions')!.value ?? []))
        )
      )
      .subscribe((missions: IMissions[]) => (this.missionsSharedCollection = missions));
  }

  protected createFromForm(): ICompetances {
    return {
      ...new Competances(),
      id: this.editForm.get(['id'])!.value,
      nom: this.editForm.get(['nom'])!.value,
      description: this.editForm.get(['description'])!.value,
      missions: this.editForm.get(['missions'])!.value,
      creatifs: this.editForm.get(['creatifs'])!.value,
      parent: this.editForm.get(['parent'])!.value,
    };
  }
}
