import { ICreatifs } from 'app/entities/creatifs/creatifs.model';
import { IMissions } from 'app/entities/missions/missions.model';

export interface ICompetances {
  id?: number;
  nom?: string | null;
  description?: string | null;
  childs?: ICompetances[] | null;
  competancePrincipalCreatifs?: ICreatifs[] | null;
  missions?: IMissions[] | null;
  creatifs?: ICreatifs[] | null;
  parent?: ICompetances | null;
}

export class Competances implements ICompetances {
  constructor(
    public id?: number,
    public nom?: string | null,
    public description?: string | null,
    public childs?: ICompetances[] | null,
    public competancePrincipalCreatifs?: ICreatifs[] | null,
    public missions?: IMissions[] | null,
    public creatifs?: ICreatifs[] | null,
    public parent?: ICompetances | null
  ) {}
}

export function getCompetancesIdentifier(competances: ICompetances): number | undefined {
  return competances.id;
}
