import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ICompetances, Competances } from '../competances.model';
import { CompetancesService } from '../service/competances.service';

@Injectable({ providedIn: 'root' })
export class CompetancesRoutingResolveService implements Resolve<ICompetances> {
  constructor(protected service: CompetancesService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICompetances> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((competances: HttpResponse<Competances>) => {
          if (competances.body) {
            return of(competances.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Competances());
  }
}
