jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { ICompetances, Competances } from '../competances.model';
import { CompetancesService } from '../service/competances.service';

import { CompetancesRoutingResolveService } from './competances-routing-resolve.service';

describe('Competances routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: CompetancesRoutingResolveService;
  let service: CompetancesService;
  let resultCompetances: ICompetances | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(CompetancesRoutingResolveService);
    service = TestBed.inject(CompetancesService);
    resultCompetances = undefined;
  });

  describe('resolve', () => {
    it('should return ICompetances returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultCompetances = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultCompetances).toEqual({ id: 123 });
    });

    it('should return new ICompetances if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultCompetances = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultCompetances).toEqual(new Competances());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as Competances })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultCompetances = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultCompetances).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
