import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { CompetancesComponent } from '../list/competances.component';
import { CompetancesDetailComponent } from '../detail/competances-detail.component';
import { CompetancesUpdateComponent } from '../update/competances-update.component';
import { CompetancesRoutingResolveService } from './competances-routing-resolve.service';

const competancesRoute: Routes = [
  {
    path: '',
    component: CompetancesComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CompetancesDetailComponent,
    resolve: {
      competances: CompetancesRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CompetancesUpdateComponent,
    resolve: {
      competances: CompetancesRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CompetancesUpdateComponent,
    resolve: {
      competances: CompetancesRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(competancesRoute)],
  exports: [RouterModule],
})
export class CompetancesRoutingModule {}
