jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IEtatMissionsHistorique, EtatMissionsHistorique } from '../etat-missions-historique.model';
import { EtatMissionsHistoriqueService } from '../service/etat-missions-historique.service';

import { EtatMissionsHistoriqueRoutingResolveService } from './etat-missions-historique-routing-resolve.service';

describe('EtatMissionsHistorique routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: EtatMissionsHistoriqueRoutingResolveService;
  let service: EtatMissionsHistoriqueService;
  let resultEtatMissionsHistorique: IEtatMissionsHistorique | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(EtatMissionsHistoriqueRoutingResolveService);
    service = TestBed.inject(EtatMissionsHistoriqueService);
    resultEtatMissionsHistorique = undefined;
  });

  describe('resolve', () => {
    it('should return IEtatMissionsHistorique returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultEtatMissionsHistorique = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultEtatMissionsHistorique).toEqual({ id: 123 });
    });

    it('should return new IEtatMissionsHistorique if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultEtatMissionsHistorique = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultEtatMissionsHistorique).toEqual(new EtatMissionsHistorique());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as EtatMissionsHistorique })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultEtatMissionsHistorique = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultEtatMissionsHistorique).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
