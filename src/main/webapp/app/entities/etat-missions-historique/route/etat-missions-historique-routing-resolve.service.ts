import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IEtatMissionsHistorique, EtatMissionsHistorique } from '../etat-missions-historique.model';
import { EtatMissionsHistoriqueService } from '../service/etat-missions-historique.service';

@Injectable({ providedIn: 'root' })
export class EtatMissionsHistoriqueRoutingResolveService implements Resolve<IEtatMissionsHistorique> {
  constructor(protected service: EtatMissionsHistoriqueService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IEtatMissionsHistorique> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((etatMissionsHistorique: HttpResponse<EtatMissionsHistorique>) => {
          if (etatMissionsHistorique.body) {
            return of(etatMissionsHistorique.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new EtatMissionsHistorique());
  }
}
