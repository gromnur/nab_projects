import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { EtatMissionsHistoriqueComponent } from '../list/etat-missions-historique.component';
import { EtatMissionsHistoriqueDetailComponent } from '../detail/etat-missions-historique-detail.component';
import { EtatMissionsHistoriqueUpdateComponent } from '../update/etat-missions-historique-update.component';
import { EtatMissionsHistoriqueRoutingResolveService } from './etat-missions-historique-routing-resolve.service';

const etatMissionsHistoriqueRoute: Routes = [
  {
    path: '',
    component: EtatMissionsHistoriqueComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: EtatMissionsHistoriqueDetailComponent,
    resolve: {
      etatMissionsHistorique: EtatMissionsHistoriqueRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: EtatMissionsHistoriqueUpdateComponent,
    resolve: {
      etatMissionsHistorique: EtatMissionsHistoriqueRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: EtatMissionsHistoriqueUpdateComponent,
    resolve: {
      etatMissionsHistorique: EtatMissionsHistoriqueRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(etatMissionsHistoriqueRoute)],
  exports: [RouterModule],
})
export class EtatMissionsHistoriqueRoutingModule {}
