import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_FORMAT } from 'app/config/input.constants';
import { EtatMissions } from 'app/entities/enumerations/etat-missions.model';
import { IEtatMissionsHistorique, EtatMissionsHistorique } from '../etat-missions-historique.model';

import { EtatMissionsHistoriqueService } from './etat-missions-historique.service';

describe('EtatMissionsHistorique Service', () => {
  let service: EtatMissionsHistoriqueService;
  let httpMock: HttpTestingController;
  let elemDefault: IEtatMissionsHistorique;
  let expectedResult: IEtatMissionsHistorique | IEtatMissionsHistorique[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(EtatMissionsHistoriqueService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      dateModification: currentDate,
      etat: EtatMissions.CHOIX_CREATIF,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          dateModification: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a EtatMissionsHistorique', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          dateModification: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateModification: currentDate,
        },
        returnedFromService
      );

      service.create(new EtatMissionsHistorique()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a EtatMissionsHistorique', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          dateModification: currentDate.format(DATE_FORMAT),
          etat: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateModification: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a EtatMissionsHistorique', () => {
      const patchObject = Object.assign(
        {
          dateModification: currentDate.format(DATE_FORMAT),
          etat: 'BBBBBB',
        },
        new EtatMissionsHistorique()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          dateModification: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of EtatMissionsHistorique', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          dateModification: currentDate.format(DATE_FORMAT),
          etat: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateModification: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a EtatMissionsHistorique', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addEtatMissionsHistoriqueToCollectionIfMissing', () => {
      it('should add a EtatMissionsHistorique to an empty array', () => {
        const etatMissionsHistorique: IEtatMissionsHistorique = { id: 123 };
        expectedResult = service.addEtatMissionsHistoriqueToCollectionIfMissing([], etatMissionsHistorique);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(etatMissionsHistorique);
      });

      it('should not add a EtatMissionsHistorique to an array that contains it', () => {
        const etatMissionsHistorique: IEtatMissionsHistorique = { id: 123 };
        const etatMissionsHistoriqueCollection: IEtatMissionsHistorique[] = [
          {
            ...etatMissionsHistorique,
          },
          { id: 456 },
        ];
        expectedResult = service.addEtatMissionsHistoriqueToCollectionIfMissing(etatMissionsHistoriqueCollection, etatMissionsHistorique);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a EtatMissionsHistorique to an array that doesn't contain it", () => {
        const etatMissionsHistorique: IEtatMissionsHistorique = { id: 123 };
        const etatMissionsHistoriqueCollection: IEtatMissionsHistorique[] = [{ id: 456 }];
        expectedResult = service.addEtatMissionsHistoriqueToCollectionIfMissing(etatMissionsHistoriqueCollection, etatMissionsHistorique);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(etatMissionsHistorique);
      });

      it('should add only unique EtatMissionsHistorique to an array', () => {
        const etatMissionsHistoriqueArray: IEtatMissionsHistorique[] = [{ id: 123 }, { id: 456 }, { id: 51703 }];
        const etatMissionsHistoriqueCollection: IEtatMissionsHistorique[] = [{ id: 123 }];
        expectedResult = service.addEtatMissionsHistoriqueToCollectionIfMissing(
          etatMissionsHistoriqueCollection,
          ...etatMissionsHistoriqueArray
        );
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const etatMissionsHistorique: IEtatMissionsHistorique = { id: 123 };
        const etatMissionsHistorique2: IEtatMissionsHistorique = { id: 456 };
        expectedResult = service.addEtatMissionsHistoriqueToCollectionIfMissing([], etatMissionsHistorique, etatMissionsHistorique2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(etatMissionsHistorique);
        expect(expectedResult).toContain(etatMissionsHistorique2);
      });

      it('should accept null and undefined values', () => {
        const etatMissionsHistorique: IEtatMissionsHistorique = { id: 123 };
        expectedResult = service.addEtatMissionsHistoriqueToCollectionIfMissing([], null, etatMissionsHistorique, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(etatMissionsHistorique);
      });

      it('should return initial array if no EtatMissionsHistorique is added', () => {
        const etatMissionsHistoriqueCollection: IEtatMissionsHistorique[] = [{ id: 123 }];
        expectedResult = service.addEtatMissionsHistoriqueToCollectionIfMissing(etatMissionsHistoriqueCollection, undefined, null);
        expect(expectedResult).toEqual(etatMissionsHistoriqueCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
