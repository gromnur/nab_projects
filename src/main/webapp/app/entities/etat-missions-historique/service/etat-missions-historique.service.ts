import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IEtatMissionsHistorique, getEtatMissionsHistoriqueIdentifier } from '../etat-missions-historique.model';

export type EntityResponseType = HttpResponse<IEtatMissionsHistorique>;
export type EntityArrayResponseType = HttpResponse<IEtatMissionsHistorique[]>;

@Injectable({ providedIn: 'root' })
export class EtatMissionsHistoriqueService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/etat-missions-historiques');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(etatMissionsHistorique: IEtatMissionsHistorique): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(etatMissionsHistorique);
    return this.http
      .post<IEtatMissionsHistorique>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(etatMissionsHistorique: IEtatMissionsHistorique): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(etatMissionsHistorique);
    return this.http
      .put<IEtatMissionsHistorique>(`${this.resourceUrl}/${getEtatMissionsHistoriqueIdentifier(etatMissionsHistorique) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(etatMissionsHistorique: IEtatMissionsHistorique): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(etatMissionsHistorique);
    return this.http
      .patch<IEtatMissionsHistorique>(
        `${this.resourceUrl}/${getEtatMissionsHistoriqueIdentifier(etatMissionsHistorique) as number}`,
        copy,
        { observe: 'response' }
      )
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IEtatMissionsHistorique>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IEtatMissionsHistorique[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addEtatMissionsHistoriqueToCollectionIfMissing(
    etatMissionsHistoriqueCollection: IEtatMissionsHistorique[],
    ...etatMissionsHistoriquesToCheck: (IEtatMissionsHistorique | null | undefined)[]
  ): IEtatMissionsHistorique[] {
    const etatMissionsHistoriques: IEtatMissionsHistorique[] = etatMissionsHistoriquesToCheck.filter(isPresent);
    if (etatMissionsHistoriques.length > 0) {
      const etatMissionsHistoriqueCollectionIdentifiers = etatMissionsHistoriqueCollection.map(
        etatMissionsHistoriqueItem => getEtatMissionsHistoriqueIdentifier(etatMissionsHistoriqueItem)!
      );
      const etatMissionsHistoriquesToAdd = etatMissionsHistoriques.filter(etatMissionsHistoriqueItem => {
        const etatMissionsHistoriqueIdentifier = getEtatMissionsHistoriqueIdentifier(etatMissionsHistoriqueItem);
        if (
          etatMissionsHistoriqueIdentifier == null ||
          etatMissionsHistoriqueCollectionIdentifiers.includes(etatMissionsHistoriqueIdentifier)
        ) {
          return false;
        }
        etatMissionsHistoriqueCollectionIdentifiers.push(etatMissionsHistoriqueIdentifier);
        return true;
      });
      return [...etatMissionsHistoriquesToAdd, ...etatMissionsHistoriqueCollection];
    }
    return etatMissionsHistoriqueCollection;
  }

  protected convertDateFromClient(etatMissionsHistorique: IEtatMissionsHistorique): IEtatMissionsHistorique {
    return Object.assign({}, etatMissionsHistorique, {
      dateModification: etatMissionsHistorique.dateModification?.isValid()
        ? etatMissionsHistorique.dateModification.format(DATE_FORMAT)
        : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateModification = res.body.dateModification ? dayjs(res.body.dateModification) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((etatMissionsHistorique: IEtatMissionsHistorique) => {
        etatMissionsHistorique.dateModification = etatMissionsHistorique.dateModification
          ? dayjs(etatMissionsHistorique.dateModification)
          : undefined;
      });
    }
    return res;
  }
}
