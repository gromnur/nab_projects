import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { EtatMissionsHistoriqueComponent } from './list/etat-missions-historique.component';
import { EtatMissionsHistoriqueDetailComponent } from './detail/etat-missions-historique-detail.component';
import { EtatMissionsHistoriqueUpdateComponent } from './update/etat-missions-historique-update.component';
import { EtatMissionsHistoriqueDeleteDialogComponent } from './delete/etat-missions-historique-delete-dialog.component';
import { EtatMissionsHistoriqueRoutingModule } from './route/etat-missions-historique-routing.module';

@NgModule({
  imports: [SharedModule, EtatMissionsHistoriqueRoutingModule],
  declarations: [
    EtatMissionsHistoriqueComponent,
    EtatMissionsHistoriqueDetailComponent,
    EtatMissionsHistoriqueUpdateComponent,
    EtatMissionsHistoriqueDeleteDialogComponent,
  ],
  entryComponents: [EtatMissionsHistoriqueDeleteDialogComponent],
})
export class EtatMissionsHistoriqueModule {}
