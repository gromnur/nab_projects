import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IEtatMissionsHistorique } from '../etat-missions-historique.model';

@Component({
  selector: 'jhi-etat-missions-historique-detail',
  templateUrl: './etat-missions-historique-detail.component.html',
})
export class EtatMissionsHistoriqueDetailComponent implements OnInit {
  etatMissionsHistorique: IEtatMissionsHistorique | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ etatMissionsHistorique }) => {
      this.etatMissionsHistorique = etatMissionsHistorique;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
