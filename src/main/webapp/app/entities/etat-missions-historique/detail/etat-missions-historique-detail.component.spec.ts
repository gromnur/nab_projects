import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EtatMissionsHistoriqueDetailComponent } from './etat-missions-historique-detail.component';

describe('EtatMissionsHistorique Management Detail Component', () => {
  let comp: EtatMissionsHistoriqueDetailComponent;
  let fixture: ComponentFixture<EtatMissionsHistoriqueDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EtatMissionsHistoriqueDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ etatMissionsHistorique: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(EtatMissionsHistoriqueDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(EtatMissionsHistoriqueDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load etatMissionsHistorique on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.etatMissionsHistorique).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
