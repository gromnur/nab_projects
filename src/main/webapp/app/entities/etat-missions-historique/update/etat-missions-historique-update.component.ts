import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IEtatMissionsHistorique, EtatMissionsHistorique } from '../etat-missions-historique.model';
import { EtatMissionsHistoriqueService } from '../service/etat-missions-historique.service';
import { EtatMissions } from 'app/entities/enumerations/etat-missions.model';

@Component({
  selector: 'jhi-etat-missions-historique-update',
  templateUrl: './etat-missions-historique-update.component.html',
})
export class EtatMissionsHistoriqueUpdateComponent implements OnInit {
  isSaving = false;
  etatMissionsValues = Object.keys(EtatMissions);

  editForm = this.fb.group({
    id: [],
    dateModification: [],
    etat: [],
  });

  constructor(
    protected etatMissionsHistoriqueService: EtatMissionsHistoriqueService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ etatMissionsHistorique }) => {
      this.updateForm(etatMissionsHistorique);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const etatMissionsHistorique = this.createFromForm();
    if (etatMissionsHistorique.id !== undefined) {
      this.subscribeToSaveResponse(this.etatMissionsHistoriqueService.update(etatMissionsHistorique));
    } else {
      this.subscribeToSaveResponse(this.etatMissionsHistoriqueService.create(etatMissionsHistorique));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEtatMissionsHistorique>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(etatMissionsHistorique: IEtatMissionsHistorique): void {
    this.editForm.patchValue({
      id: etatMissionsHistorique.id,
      dateModification: etatMissionsHistorique.dateModification,
      etat: etatMissionsHistorique.etat,
    });
  }

  protected createFromForm(): IEtatMissionsHistorique {
    return {
      ...new EtatMissionsHistorique(),
      id: this.editForm.get(['id'])!.value,
      dateModification: this.editForm.get(['dateModification'])!.value,
      etat: this.editForm.get(['etat'])!.value,
    };
  }
}
