jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { EtatMissionsHistoriqueService } from '../service/etat-missions-historique.service';
import { IEtatMissionsHistorique, EtatMissionsHistorique } from '../etat-missions-historique.model';

import { EtatMissionsHistoriqueUpdateComponent } from './etat-missions-historique-update.component';

describe('EtatMissionsHistorique Management Update Component', () => {
  let comp: EtatMissionsHistoriqueUpdateComponent;
  let fixture: ComponentFixture<EtatMissionsHistoriqueUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let etatMissionsHistoriqueService: EtatMissionsHistoriqueService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [EtatMissionsHistoriqueUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(EtatMissionsHistoriqueUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(EtatMissionsHistoriqueUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    etatMissionsHistoriqueService = TestBed.inject(EtatMissionsHistoriqueService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const etatMissionsHistorique: IEtatMissionsHistorique = { id: 456 };

      activatedRoute.data = of({ etatMissionsHistorique });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(etatMissionsHistorique));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<EtatMissionsHistorique>>();
      const etatMissionsHistorique = { id: 123 };
      jest.spyOn(etatMissionsHistoriqueService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ etatMissionsHistorique });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: etatMissionsHistorique }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(etatMissionsHistoriqueService.update).toHaveBeenCalledWith(etatMissionsHistorique);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<EtatMissionsHistorique>>();
      const etatMissionsHistorique = new EtatMissionsHistorique();
      jest.spyOn(etatMissionsHistoriqueService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ etatMissionsHistorique });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: etatMissionsHistorique }));
      saveSubject.complete();

      // THEN
      expect(etatMissionsHistoriqueService.create).toHaveBeenCalledWith(etatMissionsHistorique);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<EtatMissionsHistorique>>();
      const etatMissionsHistorique = { id: 123 };
      jest.spyOn(etatMissionsHistoriqueService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ etatMissionsHistorique });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(etatMissionsHistoriqueService.update).toHaveBeenCalledWith(etatMissionsHistorique);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
