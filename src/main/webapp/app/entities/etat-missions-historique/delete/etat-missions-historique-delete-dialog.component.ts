import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IEtatMissionsHistorique } from '../etat-missions-historique.model';
import { EtatMissionsHistoriqueService } from '../service/etat-missions-historique.service';

@Component({
  templateUrl: './etat-missions-historique-delete-dialog.component.html',
})
export class EtatMissionsHistoriqueDeleteDialogComponent {
  etatMissionsHistorique?: IEtatMissionsHistorique;

  constructor(protected etatMissionsHistoriqueService: EtatMissionsHistoriqueService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.etatMissionsHistoriqueService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
