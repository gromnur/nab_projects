import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IEtatMissionsHistorique } from '../etat-missions-historique.model';
import { EtatMissionsHistoriqueService } from '../service/etat-missions-historique.service';
import { EtatMissionsHistoriqueDeleteDialogComponent } from '../delete/etat-missions-historique-delete-dialog.component';

@Component({
  selector: 'jhi-etat-missions-historique',
  templateUrl: './etat-missions-historique.component.html',
})
export class EtatMissionsHistoriqueComponent implements OnInit {
  etatMissionsHistoriques?: IEtatMissionsHistorique[];
  isLoading = false;

  constructor(protected etatMissionsHistoriqueService: EtatMissionsHistoriqueService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.etatMissionsHistoriqueService.query().subscribe(
      (res: HttpResponse<IEtatMissionsHistorique[]>) => {
        this.isLoading = false;
        this.etatMissionsHistoriques = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IEtatMissionsHistorique): number {
    return item.id!;
  }

  delete(etatMissionsHistorique: IEtatMissionsHistorique): void {
    const modalRef = this.modalService.open(EtatMissionsHistoriqueDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.etatMissionsHistorique = etatMissionsHistorique;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
