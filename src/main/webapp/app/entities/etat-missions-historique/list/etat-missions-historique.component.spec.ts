import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { EtatMissionsHistoriqueService } from '../service/etat-missions-historique.service';

import { EtatMissionsHistoriqueComponent } from './etat-missions-historique.component';

describe('EtatMissionsHistorique Management Component', () => {
  let comp: EtatMissionsHistoriqueComponent;
  let fixture: ComponentFixture<EtatMissionsHistoriqueComponent>;
  let service: EtatMissionsHistoriqueService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [EtatMissionsHistoriqueComponent],
    })
      .overrideTemplate(EtatMissionsHistoriqueComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(EtatMissionsHistoriqueComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(EtatMissionsHistoriqueService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.etatMissionsHistoriques?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
