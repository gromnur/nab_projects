import * as dayjs from 'dayjs';
import { IMissions } from 'app/entities/missions/missions.model';
import { EtatMissions } from 'app/entities/enumerations/etat-missions.model';

export interface IEtatMissionsHistorique {
  id?: number;
  dateModification?: dayjs.Dayjs | null;
  etat?: EtatMissions | null;
  missions?: IMissions[] | null;
}

export class EtatMissionsHistorique implements IEtatMissionsHistorique {
  constructor(
    public id?: number,
    public dateModification?: dayjs.Dayjs | null,
    public etat?: EtatMissions | null,
    public missions?: IMissions[] | null
  ) {}
}

export function getEtatMissionsHistoriqueIdentifier(etatMissionsHistorique: IEtatMissionsHistorique): number | undefined {
  return etatMissionsHistorique.id;
}
