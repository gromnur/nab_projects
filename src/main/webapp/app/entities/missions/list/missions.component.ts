import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IMissions } from '../missions.model';
import { MissionsService } from '../service/missions.service';
import { MissionsDeleteDialogComponent } from '../delete/missions-delete-dialog.component';

@Component({
  selector: 'jhi-missions',
  templateUrl: './missions.component.html',
})
export class MissionsComponent implements OnInit {
  missions?: IMissions[];
  isLoading = false;

  constructor(protected missionsService: MissionsService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.missionsService.query().subscribe(
      (res: HttpResponse<IMissions[]>) => {
        this.isLoading = false;
        this.missions = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IMissions): number {
    return item.id!;
  }

  delete(missions: IMissions): void {
    const modalRef = this.modalService.open(MissionsDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.missions = missions;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
