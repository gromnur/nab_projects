import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { MissionsService } from '../service/missions.service';

import { MissionsComponent } from './missions.component';

describe('Missions Management Component', () => {
  let comp: MissionsComponent;
  let fixture: ComponentFixture<MissionsComponent>;
  let service: MissionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [MissionsComponent],
    })
      .overrideTemplate(MissionsComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(MissionsComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(MissionsService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.missions?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
