import * as dayjs from 'dayjs';
import { IPhotos } from 'app/entities/photos/photos.model';
import { IEtatMissionsHistorique } from 'app/entities/etat-missions-historique/etat-missions-historique.model';
import { IPrestataires } from 'app/entities/prestataires/prestataires.model';
import { ICreatifs } from 'app/entities/creatifs/creatifs.model';
import { IStatusValidationsHistorique } from 'app/entities/status-validations-historique/status-validations-historique.model';
import { ICompetances } from 'app/entities/competances/competances.model';

export interface IMissions {
  id?: number;
  nom?: string | null;
  lieu?: string | null;
  prix?: number | null;
  dateDebutMission?: dayjs.Dayjs | null;
  dateFinMission?: dayjs.Dayjs | null;
  dateCreationMission?: dayjs.Dayjs | null;
  photos?: IPhotos[] | null;
  etatMissionsHistorique?: IEtatMissionsHistorique | null;
  prestataire?: IPrestataires | null;
  creatif?: ICreatifs | null;
  statusValidationsHistorique?: IStatusValidationsHistorique | null;
  competances?: ICompetances[] | null;
}

export class Missions implements IMissions {
  constructor(
    public id?: number,
    public nom?: string | null,
    public lieu?: string | null,
    public prix?: number | null,
    public dateDebutMission?: dayjs.Dayjs | null,
    public dateFinMission?: dayjs.Dayjs | null,
    public dateCreationMission?: dayjs.Dayjs | null,
    public photos?: IPhotos[] | null,
    public etatMissionsHistorique?: IEtatMissionsHistorique | null,
    public prestataire?: IPrestataires | null,
    public creatif?: ICreatifs | null,
    public statusValidationsHistorique?: IStatusValidationsHistorique | null,
    public competances?: ICompetances[] | null
  ) {}
}

export function getMissionsIdentifier(missions: IMissions): number | undefined {
  return missions.id;
}
