import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IMissions, getMissionsIdentifier } from '../missions.model';

export type EntityResponseType = HttpResponse<IMissions>;
export type EntityArrayResponseType = HttpResponse<IMissions[]>;

@Injectable({ providedIn: 'root' })
export class MissionsService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/missions');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(missions: IMissions): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(missions);
    return this.http
      .post<IMissions>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(missions: IMissions): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(missions);
    return this.http
      .put<IMissions>(`${this.resourceUrl}/${getMissionsIdentifier(missions) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(missions: IMissions): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(missions);
    return this.http
      .patch<IMissions>(`${this.resourceUrl}/${getMissionsIdentifier(missions) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IMissions>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IMissions[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addMissionsToCollectionIfMissing(missionsCollection: IMissions[], ...missionsToCheck: (IMissions | null | undefined)[]): IMissions[] {
    const missions: IMissions[] = missionsToCheck.filter(isPresent);
    if (missions.length > 0) {
      const missionsCollectionIdentifiers = missionsCollection.map(missionsItem => getMissionsIdentifier(missionsItem)!);
      const missionsToAdd = missions.filter(missionsItem => {
        const missionsIdentifier = getMissionsIdentifier(missionsItem);
        if (missionsIdentifier == null || missionsCollectionIdentifiers.includes(missionsIdentifier)) {
          return false;
        }
        missionsCollectionIdentifiers.push(missionsIdentifier);
        return true;
      });
      return [...missionsToAdd, ...missionsCollection];
    }
    return missionsCollection;
  }

  protected convertDateFromClient(missions: IMissions): IMissions {
    return Object.assign({}, missions, {
      dateDebutMission: missions.dateDebutMission?.isValid() ? missions.dateDebutMission.format(DATE_FORMAT) : undefined,
      dateFinMission: missions.dateFinMission?.isValid() ? missions.dateFinMission.format(DATE_FORMAT) : undefined,
      dateCreationMission: missions.dateCreationMission?.isValid() ? missions.dateCreationMission.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateDebutMission = res.body.dateDebutMission ? dayjs(res.body.dateDebutMission) : undefined;
      res.body.dateFinMission = res.body.dateFinMission ? dayjs(res.body.dateFinMission) : undefined;
      res.body.dateCreationMission = res.body.dateCreationMission ? dayjs(res.body.dateCreationMission) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((missions: IMissions) => {
        missions.dateDebutMission = missions.dateDebutMission ? dayjs(missions.dateDebutMission) : undefined;
        missions.dateFinMission = missions.dateFinMission ? dayjs(missions.dateFinMission) : undefined;
        missions.dateCreationMission = missions.dateCreationMission ? dayjs(missions.dateCreationMission) : undefined;
      });
    }
    return res;
  }
}
