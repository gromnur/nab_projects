import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IMissions, Missions } from '../missions.model';

import { MissionsService } from './missions.service';

describe('Missions Service', () => {
  let service: MissionsService;
  let httpMock: HttpTestingController;
  let elemDefault: IMissions;
  let expectedResult: IMissions | IMissions[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(MissionsService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      nom: 'AAAAAAA',
      lieu: 'AAAAAAA',
      prix: 0,
      dateDebutMission: currentDate,
      dateFinMission: currentDate,
      dateCreationMission: currentDate,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          dateDebutMission: currentDate.format(DATE_FORMAT),
          dateFinMission: currentDate.format(DATE_FORMAT),
          dateCreationMission: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Missions', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          dateDebutMission: currentDate.format(DATE_FORMAT),
          dateFinMission: currentDate.format(DATE_FORMAT),
          dateCreationMission: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateDebutMission: currentDate,
          dateFinMission: currentDate,
          dateCreationMission: currentDate,
        },
        returnedFromService
      );

      service.create(new Missions()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Missions', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nom: 'BBBBBB',
          lieu: 'BBBBBB',
          prix: 1,
          dateDebutMission: currentDate.format(DATE_FORMAT),
          dateFinMission: currentDate.format(DATE_FORMAT),
          dateCreationMission: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateDebutMission: currentDate,
          dateFinMission: currentDate,
          dateCreationMission: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Missions', () => {
      const patchObject = Object.assign(
        {
          dateDebutMission: currentDate.format(DATE_FORMAT),
          dateFinMission: currentDate.format(DATE_FORMAT),
          dateCreationMission: currentDate.format(DATE_FORMAT),
        },
        new Missions()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          dateDebutMission: currentDate,
          dateFinMission: currentDate,
          dateCreationMission: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Missions', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nom: 'BBBBBB',
          lieu: 'BBBBBB',
          prix: 1,
          dateDebutMission: currentDate.format(DATE_FORMAT),
          dateFinMission: currentDate.format(DATE_FORMAT),
          dateCreationMission: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateDebutMission: currentDate,
          dateFinMission: currentDate,
          dateCreationMission: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Missions', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addMissionsToCollectionIfMissing', () => {
      it('should add a Missions to an empty array', () => {
        const missions: IMissions = { id: 123 };
        expectedResult = service.addMissionsToCollectionIfMissing([], missions);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(missions);
      });

      it('should not add a Missions to an array that contains it', () => {
        const missions: IMissions = { id: 123 };
        const missionsCollection: IMissions[] = [
          {
            ...missions,
          },
          { id: 456 },
        ];
        expectedResult = service.addMissionsToCollectionIfMissing(missionsCollection, missions);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Missions to an array that doesn't contain it", () => {
        const missions: IMissions = { id: 123 };
        const missionsCollection: IMissions[] = [{ id: 456 }];
        expectedResult = service.addMissionsToCollectionIfMissing(missionsCollection, missions);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(missions);
      });

      it('should add only unique Missions to an array', () => {
        const missionsArray: IMissions[] = [{ id: 123 }, { id: 456 }, { id: 57234 }];
        const missionsCollection: IMissions[] = [{ id: 123 }];
        expectedResult = service.addMissionsToCollectionIfMissing(missionsCollection, ...missionsArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const missions: IMissions = { id: 123 };
        const missions2: IMissions = { id: 456 };
        expectedResult = service.addMissionsToCollectionIfMissing([], missions, missions2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(missions);
        expect(expectedResult).toContain(missions2);
      });

      it('should accept null and undefined values', () => {
        const missions: IMissions = { id: 123 };
        expectedResult = service.addMissionsToCollectionIfMissing([], null, missions, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(missions);
      });

      it('should return initial array if no Missions is added', () => {
        const missionsCollection: IMissions[] = [{ id: 123 }];
        expectedResult = service.addMissionsToCollectionIfMissing(missionsCollection, undefined, null);
        expect(expectedResult).toEqual(missionsCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
