import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IMissions, Missions } from '../missions.model';
import { MissionsService } from '../service/missions.service';
import { IEtatMissionsHistorique } from 'app/entities/etat-missions-historique/etat-missions-historique.model';
import { EtatMissionsHistoriqueService } from 'app/entities/etat-missions-historique/service/etat-missions-historique.service';
import { IPrestataires } from 'app/entities/prestataires/prestataires.model';
import { PrestatairesService } from 'app/entities/prestataires/service/prestataires.service';
import { ICreatifs } from 'app/entities/creatifs/creatifs.model';
import { CreatifsService } from 'app/entities/creatifs/service/creatifs.service';
import { IStatusValidationsHistorique } from 'app/entities/status-validations-historique/status-validations-historique.model';
import { StatusValidationsHistoriqueService } from 'app/entities/status-validations-historique/service/status-validations-historique.service';

@Component({
  selector: 'jhi-missions-update',
  templateUrl: './missions-update.component.html',
})
export class MissionsUpdateComponent implements OnInit {
  isSaving = false;

  etatMissionsHistoriquesSharedCollection: IEtatMissionsHistorique[] = [];
  prestatairesSharedCollection: IPrestataires[] = [];
  creatifsSharedCollection: ICreatifs[] = [];
  statusValidationsHistoriquesSharedCollection: IStatusValidationsHistorique[] = [];

  editForm = this.fb.group({
    id: [],
    nom: [null, [Validators.maxLength(50)]],
    lieu: [null, [Validators.maxLength(50)]],
    prix: [],
    dateDebutMission: [],
    dateFinMission: [],
    dateCreationMission: [],
    etatMissionsHistorique: [],
    prestataire: [],
    creatif: [],
    statusValidationsHistorique: [],
  });

  constructor(
    protected missionsService: MissionsService,
    protected etatMissionsHistoriqueService: EtatMissionsHistoriqueService,
    protected prestatairesService: PrestatairesService,
    protected creatifsService: CreatifsService,
    protected statusValidationsHistoriqueService: StatusValidationsHistoriqueService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ missions }) => {
      this.updateForm(missions);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const missions = this.createFromForm();
    if (missions.id !== undefined) {
      this.subscribeToSaveResponse(this.missionsService.update(missions));
    } else {
      this.subscribeToSaveResponse(this.missionsService.create(missions));
    }
  }

  trackEtatMissionsHistoriqueById(index: number, item: IEtatMissionsHistorique): number {
    return item.id!;
  }

  trackPrestatairesById(index: number, item: IPrestataires): number {
    return item.id!;
  }

  trackCreatifsById(index: number, item: ICreatifs): number {
    return item.id!;
  }

  trackStatusValidationsHistoriqueById(index: number, item: IStatusValidationsHistorique): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMissions>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(missions: IMissions): void {
    this.editForm.patchValue({
      id: missions.id,
      nom: missions.nom,
      lieu: missions.lieu,
      prix: missions.prix,
      dateDebutMission: missions.dateDebutMission,
      dateFinMission: missions.dateFinMission,
      dateCreationMission: missions.dateCreationMission,
      etatMissionsHistorique: missions.etatMissionsHistorique,
      prestataire: missions.prestataire,
      creatif: missions.creatif,
      statusValidationsHistorique: missions.statusValidationsHistorique,
    });

    this.etatMissionsHistoriquesSharedCollection = this.etatMissionsHistoriqueService.addEtatMissionsHistoriqueToCollectionIfMissing(
      this.etatMissionsHistoriquesSharedCollection,
      missions.etatMissionsHistorique
    );
    this.prestatairesSharedCollection = this.prestatairesService.addPrestatairesToCollectionIfMissing(
      this.prestatairesSharedCollection,
      missions.prestataire
    );
    this.creatifsSharedCollection = this.creatifsService.addCreatifsToCollectionIfMissing(this.creatifsSharedCollection, missions.creatif);
    this.statusValidationsHistoriquesSharedCollection =
      this.statusValidationsHistoriqueService.addStatusValidationsHistoriqueToCollectionIfMissing(
        this.statusValidationsHistoriquesSharedCollection,
        missions.statusValidationsHistorique
      );
  }

  protected loadRelationshipsOptions(): void {
    this.etatMissionsHistoriqueService
      .query()
      .pipe(map((res: HttpResponse<IEtatMissionsHistorique[]>) => res.body ?? []))
      .pipe(
        map((etatMissionsHistoriques: IEtatMissionsHistorique[]) =>
          this.etatMissionsHistoriqueService.addEtatMissionsHistoriqueToCollectionIfMissing(
            etatMissionsHistoriques,
            this.editForm.get('etatMissionsHistorique')!.value
          )
        )
      )
      .subscribe(
        (etatMissionsHistoriques: IEtatMissionsHistorique[]) => (this.etatMissionsHistoriquesSharedCollection = etatMissionsHistoriques)
      );

    this.prestatairesService
      .query()
      .pipe(map((res: HttpResponse<IPrestataires[]>) => res.body ?? []))
      .pipe(
        map((prestataires: IPrestataires[]) =>
          this.prestatairesService.addPrestatairesToCollectionIfMissing(prestataires, this.editForm.get('prestataire')!.value)
        )
      )
      .subscribe((prestataires: IPrestataires[]) => (this.prestatairesSharedCollection = prestataires));

    this.creatifsService
      .query()
      .pipe(map((res: HttpResponse<ICreatifs[]>) => res.body ?? []))
      .pipe(
        map((creatifs: ICreatifs[]) => this.creatifsService.addCreatifsToCollectionIfMissing(creatifs, this.editForm.get('creatif')!.value))
      )
      .subscribe((creatifs: ICreatifs[]) => (this.creatifsSharedCollection = creatifs));

    this.statusValidationsHistoriqueService
      .query()
      .pipe(map((res: HttpResponse<IStatusValidationsHistorique[]>) => res.body ?? []))
      .pipe(
        map((statusValidationsHistoriques: IStatusValidationsHistorique[]) =>
          this.statusValidationsHistoriqueService.addStatusValidationsHistoriqueToCollectionIfMissing(
            statusValidationsHistoriques,
            this.editForm.get('statusValidationsHistorique')!.value
          )
        )
      )
      .subscribe(
        (statusValidationsHistoriques: IStatusValidationsHistorique[]) =>
          (this.statusValidationsHistoriquesSharedCollection = statusValidationsHistoriques)
      );
  }

  protected createFromForm(): IMissions {
    return {
      ...new Missions(),
      id: this.editForm.get(['id'])!.value,
      nom: this.editForm.get(['nom'])!.value,
      lieu: this.editForm.get(['lieu'])!.value,
      prix: this.editForm.get(['prix'])!.value,
      dateDebutMission: this.editForm.get(['dateDebutMission'])!.value,
      dateFinMission: this.editForm.get(['dateFinMission'])!.value,
      dateCreationMission: this.editForm.get(['dateCreationMission'])!.value,
      etatMissionsHistorique: this.editForm.get(['etatMissionsHistorique'])!.value,
      prestataire: this.editForm.get(['prestataire'])!.value,
      creatif: this.editForm.get(['creatif'])!.value,
      statusValidationsHistorique: this.editForm.get(['statusValidationsHistorique'])!.value,
    };
  }
}
