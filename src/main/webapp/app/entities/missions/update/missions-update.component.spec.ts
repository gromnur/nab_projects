jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { MissionsService } from '../service/missions.service';
import { IMissions, Missions } from '../missions.model';
import { IEtatMissionsHistorique } from 'app/entities/etat-missions-historique/etat-missions-historique.model';
import { EtatMissionsHistoriqueService } from 'app/entities/etat-missions-historique/service/etat-missions-historique.service';
import { IPrestataires } from 'app/entities/prestataires/prestataires.model';
import { PrestatairesService } from 'app/entities/prestataires/service/prestataires.service';
import { ICreatifs } from 'app/entities/creatifs/creatifs.model';
import { CreatifsService } from 'app/entities/creatifs/service/creatifs.service';
import { IStatusValidationsHistorique } from 'app/entities/status-validations-historique/status-validations-historique.model';
import { StatusValidationsHistoriqueService } from 'app/entities/status-validations-historique/service/status-validations-historique.service';

import { MissionsUpdateComponent } from './missions-update.component';

describe('Missions Management Update Component', () => {
  let comp: MissionsUpdateComponent;
  let fixture: ComponentFixture<MissionsUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let missionsService: MissionsService;
  let etatMissionsHistoriqueService: EtatMissionsHistoriqueService;
  let prestatairesService: PrestatairesService;
  let creatifsService: CreatifsService;
  let statusValidationsHistoriqueService: StatusValidationsHistoriqueService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [MissionsUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(MissionsUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(MissionsUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    missionsService = TestBed.inject(MissionsService);
    etatMissionsHistoriqueService = TestBed.inject(EtatMissionsHistoriqueService);
    prestatairesService = TestBed.inject(PrestatairesService);
    creatifsService = TestBed.inject(CreatifsService);
    statusValidationsHistoriqueService = TestBed.inject(StatusValidationsHistoriqueService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call EtatMissionsHistorique query and add missing value', () => {
      const missions: IMissions = { id: 456 };
      const etatMissionsHistorique: IEtatMissionsHistorique = { id: 92274 };
      missions.etatMissionsHistorique = etatMissionsHistorique;

      const etatMissionsHistoriqueCollection: IEtatMissionsHistorique[] = [{ id: 2755 }];
      jest.spyOn(etatMissionsHistoriqueService, 'query').mockReturnValue(of(new HttpResponse({ body: etatMissionsHistoriqueCollection })));
      const additionalEtatMissionsHistoriques = [etatMissionsHistorique];
      const expectedCollection: IEtatMissionsHistorique[] = [...additionalEtatMissionsHistoriques, ...etatMissionsHistoriqueCollection];
      jest.spyOn(etatMissionsHistoriqueService, 'addEtatMissionsHistoriqueToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ missions });
      comp.ngOnInit();

      expect(etatMissionsHistoriqueService.query).toHaveBeenCalled();
      expect(etatMissionsHistoriqueService.addEtatMissionsHistoriqueToCollectionIfMissing).toHaveBeenCalledWith(
        etatMissionsHistoriqueCollection,
        ...additionalEtatMissionsHistoriques
      );
      expect(comp.etatMissionsHistoriquesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Prestataires query and add missing value', () => {
      const missions: IMissions = { id: 456 };
      const prestataire: IPrestataires = { id: 50880 };
      missions.prestataire = prestataire;

      const prestatairesCollection: IPrestataires[] = [{ id: 43175 }];
      jest.spyOn(prestatairesService, 'query').mockReturnValue(of(new HttpResponse({ body: prestatairesCollection })));
      const additionalPrestataires = [prestataire];
      const expectedCollection: IPrestataires[] = [...additionalPrestataires, ...prestatairesCollection];
      jest.spyOn(prestatairesService, 'addPrestatairesToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ missions });
      comp.ngOnInit();

      expect(prestatairesService.query).toHaveBeenCalled();
      expect(prestatairesService.addPrestatairesToCollectionIfMissing).toHaveBeenCalledWith(
        prestatairesCollection,
        ...additionalPrestataires
      );
      expect(comp.prestatairesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Creatifs query and add missing value', () => {
      const missions: IMissions = { id: 456 };
      const creatif: ICreatifs = { id: 20355 };
      missions.creatif = creatif;

      const creatifsCollection: ICreatifs[] = [{ id: 63324 }];
      jest.spyOn(creatifsService, 'query').mockReturnValue(of(new HttpResponse({ body: creatifsCollection })));
      const additionalCreatifs = [creatif];
      const expectedCollection: ICreatifs[] = [...additionalCreatifs, ...creatifsCollection];
      jest.spyOn(creatifsService, 'addCreatifsToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ missions });
      comp.ngOnInit();

      expect(creatifsService.query).toHaveBeenCalled();
      expect(creatifsService.addCreatifsToCollectionIfMissing).toHaveBeenCalledWith(creatifsCollection, ...additionalCreatifs);
      expect(comp.creatifsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call StatusValidationsHistorique query and add missing value', () => {
      const missions: IMissions = { id: 456 };
      const statusValidationsHistorique: IStatusValidationsHistorique = { id: 65355 };
      missions.statusValidationsHistorique = statusValidationsHistorique;

      const statusValidationsHistoriqueCollection: IStatusValidationsHistorique[] = [{ id: 77627 }];
      jest
        .spyOn(statusValidationsHistoriqueService, 'query')
        .mockReturnValue(of(new HttpResponse({ body: statusValidationsHistoriqueCollection })));
      const additionalStatusValidationsHistoriques = [statusValidationsHistorique];
      const expectedCollection: IStatusValidationsHistorique[] = [
        ...additionalStatusValidationsHistoriques,
        ...statusValidationsHistoriqueCollection,
      ];
      jest
        .spyOn(statusValidationsHistoriqueService, 'addStatusValidationsHistoriqueToCollectionIfMissing')
        .mockReturnValue(expectedCollection);

      activatedRoute.data = of({ missions });
      comp.ngOnInit();

      expect(statusValidationsHistoriqueService.query).toHaveBeenCalled();
      expect(statusValidationsHistoriqueService.addStatusValidationsHistoriqueToCollectionIfMissing).toHaveBeenCalledWith(
        statusValidationsHistoriqueCollection,
        ...additionalStatusValidationsHistoriques
      );
      expect(comp.statusValidationsHistoriquesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const missions: IMissions = { id: 456 };
      const etatMissionsHistorique: IEtatMissionsHistorique = { id: 21830 };
      missions.etatMissionsHistorique = etatMissionsHistorique;
      const prestataire: IPrestataires = { id: 43223 };
      missions.prestataire = prestataire;
      const creatif: ICreatifs = { id: 18674 };
      missions.creatif = creatif;
      const statusValidationsHistorique: IStatusValidationsHistorique = { id: 50402 };
      missions.statusValidationsHistorique = statusValidationsHistorique;

      activatedRoute.data = of({ missions });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(missions));
      expect(comp.etatMissionsHistoriquesSharedCollection).toContain(etatMissionsHistorique);
      expect(comp.prestatairesSharedCollection).toContain(prestataire);
      expect(comp.creatifsSharedCollection).toContain(creatif);
      expect(comp.statusValidationsHistoriquesSharedCollection).toContain(statusValidationsHistorique);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Missions>>();
      const missions = { id: 123 };
      jest.spyOn(missionsService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ missions });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: missions }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(missionsService.update).toHaveBeenCalledWith(missions);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Missions>>();
      const missions = new Missions();
      jest.spyOn(missionsService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ missions });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: missions }));
      saveSubject.complete();

      // THEN
      expect(missionsService.create).toHaveBeenCalledWith(missions);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Missions>>();
      const missions = { id: 123 };
      jest.spyOn(missionsService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ missions });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(missionsService.update).toHaveBeenCalledWith(missions);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackEtatMissionsHistoriqueById', () => {
      it('Should return tracked EtatMissionsHistorique primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackEtatMissionsHistoriqueById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackPrestatairesById', () => {
      it('Should return tracked Prestataires primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackPrestatairesById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackCreatifsById', () => {
      it('Should return tracked Creatifs primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCreatifsById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackStatusValidationsHistoriqueById', () => {
      it('Should return tracked StatusValidationsHistorique primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackStatusValidationsHistoriqueById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
