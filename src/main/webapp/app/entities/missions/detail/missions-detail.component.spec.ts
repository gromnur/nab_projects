import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MissionsDetailComponent } from './missions-detail.component';

describe('Missions Management Detail Component', () => {
  let comp: MissionsDetailComponent;
  let fixture: ComponentFixture<MissionsDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MissionsDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ missions: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(MissionsDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(MissionsDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load missions on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.missions).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
