import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMissions } from '../missions.model';

@Component({
  selector: 'jhi-missions-detail',
  templateUrl: './missions-detail.component.html',
})
export class MissionsDetailComponent implements OnInit {
  missions: IMissions | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ missions }) => {
      this.missions = missions;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
