import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { MissionsComponent } from './list/missions.component';
import { MissionsDetailComponent } from './detail/missions-detail.component';
import { MissionsUpdateComponent } from './update/missions-update.component';
import { MissionsDeleteDialogComponent } from './delete/missions-delete-dialog.component';
import { MissionsRoutingModule } from './route/missions-routing.module';

@NgModule({
  imports: [SharedModule, MissionsRoutingModule],
  declarations: [MissionsComponent, MissionsDetailComponent, MissionsUpdateComponent, MissionsDeleteDialogComponent],
  entryComponents: [MissionsDeleteDialogComponent],
})
export class MissionsModule {}
