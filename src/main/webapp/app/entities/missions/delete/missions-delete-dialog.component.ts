import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IMissions } from '../missions.model';
import { MissionsService } from '../service/missions.service';

@Component({
  templateUrl: './missions-delete-dialog.component.html',
})
export class MissionsDeleteDialogComponent {
  missions?: IMissions;

  constructor(protected missionsService: MissionsService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.missionsService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
