import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IMissions, Missions } from '../missions.model';
import { MissionsService } from '../service/missions.service';

@Injectable({ providedIn: 'root' })
export class MissionsRoutingResolveService implements Resolve<IMissions> {
  constructor(protected service: MissionsService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMissions> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((missions: HttpResponse<Missions>) => {
          if (missions.body) {
            return of(missions.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Missions());
  }
}
