jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IMissions, Missions } from '../missions.model';
import { MissionsService } from '../service/missions.service';

import { MissionsRoutingResolveService } from './missions-routing-resolve.service';

describe('Missions routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: MissionsRoutingResolveService;
  let service: MissionsService;
  let resultMissions: IMissions | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(MissionsRoutingResolveService);
    service = TestBed.inject(MissionsService);
    resultMissions = undefined;
  });

  describe('resolve', () => {
    it('should return IMissions returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultMissions = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultMissions).toEqual({ id: 123 });
    });

    it('should return new IMissions if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultMissions = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultMissions).toEqual(new Missions());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as Missions })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultMissions = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultMissions).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
