import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { MissionsComponent } from '../list/missions.component';
import { MissionsDetailComponent } from '../detail/missions-detail.component';
import { MissionsUpdateComponent } from '../update/missions-update.component';
import { MissionsRoutingResolveService } from './missions-routing-resolve.service';

const missionsRoute: Routes = [
  {
    path: '',
    component: MissionsComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: MissionsDetailComponent,
    resolve: {
      missions: MissionsRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: MissionsUpdateComponent,
    resolve: {
      missions: MissionsRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: MissionsUpdateComponent,
    resolve: {
      missions: MissionsRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(missionsRoute)],
  exports: [RouterModule],
})
export class MissionsRoutingModule {}
