export enum StatusValidations {
  VALIDE = 'VALIDE',

  REFUSE = 'REFUSE',

  ATTENTE = 'ATTENTE',

  TERMINE = 'TERMINE',
}
