export enum EtatMissions {
  CHOIX_CREATIF = 'CHOIX_CREATIF',

  NON_DETAILLEE = 'NON_DETAILLEE',

  EN_COURS = 'EN_COURS',

  FINI = 'FINI',

  ANNULEE = 'ANNULEE',
}
