import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'creatifs',
        data: { pageTitle: 'nabAppApp.creatifs.home.title' },
        loadChildren: () => import('./creatifs/creatifs.module').then(m => m.CreatifsModule),
      },
      {
        path: 'competances',
        data: { pageTitle: 'nabAppApp.competances.home.title' },
        loadChildren: () => import('./competances/competances.module').then(m => m.CompetancesModule),
      },
      {
        path: 'missions',
        data: { pageTitle: 'nabAppApp.missions.home.title' },
        loadChildren: () => import('./missions/missions.module').then(m => m.MissionsModule),
      },
      {
        path: 'prestataires',
        data: { pageTitle: 'nabAppApp.prestataires.home.title' },
        loadChildren: () => import('./prestataires/prestataires.module').then(m => m.PrestatairesModule),
      },
      {
        path: 'photos',
        data: { pageTitle: 'nabAppApp.photos.home.title' },
        loadChildren: () => import('./photos/photos.module').then(m => m.PhotosModule),
      },
      {
        path: 'status-creatifs-historique',
        data: { pageTitle: 'nabAppApp.statusCreatifsHistorique.home.title' },
        loadChildren: () =>
          import('./status-creatifs-historique/status-creatifs-historique.module').then(m => m.StatusCreatifsHistoriqueModule),
      },
      {
        path: 'etat-missions-historique',
        data: { pageTitle: 'nabAppApp.etatMissionsHistorique.home.title' },
        loadChildren: () => import('./etat-missions-historique/etat-missions-historique.module').then(m => m.EtatMissionsHistoriqueModule),
      },
      {
        path: 'status-validations-historique',
        data: { pageTitle: 'nabAppApp.statusValidationsHistorique.home.title' },
        loadChildren: () =>
          import('./status-validations-historique/status-validations-historique.module').then(m => m.StatusValidationsHistoriqueModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
