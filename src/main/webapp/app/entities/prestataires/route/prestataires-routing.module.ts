import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { PrestatairesComponent } from '../list/prestataires.component';
import { PrestatairesDetailComponent } from '../detail/prestataires-detail.component';
import { PrestatairesUpdateComponent } from '../update/prestataires-update.component';
import { PrestatairesRoutingResolveService } from './prestataires-routing-resolve.service';

const prestatairesRoute: Routes = [
  {
    path: '',
    component: PrestatairesComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PrestatairesDetailComponent,
    resolve: {
      prestataires: PrestatairesRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PrestatairesUpdateComponent,
    resolve: {
      prestataires: PrestatairesRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PrestatairesUpdateComponent,
    resolve: {
      prestataires: PrestatairesRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(prestatairesRoute)],
  exports: [RouterModule],
})
export class PrestatairesRoutingModule {}
