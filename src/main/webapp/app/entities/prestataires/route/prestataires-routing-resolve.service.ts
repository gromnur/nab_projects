import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IPrestataires, Prestataires } from '../prestataires.model';
import { PrestatairesService } from '../service/prestataires.service';

@Injectable({ providedIn: 'root' })
export class PrestatairesRoutingResolveService implements Resolve<IPrestataires> {
  constructor(protected service: PrestatairesService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPrestataires> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((prestataires: HttpResponse<Prestataires>) => {
          if (prestataires.body) {
            return of(prestataires.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Prestataires());
  }
}
