jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IPrestataires, Prestataires } from '../prestataires.model';
import { PrestatairesService } from '../service/prestataires.service';

import { PrestatairesRoutingResolveService } from './prestataires-routing-resolve.service';

describe('Prestataires routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: PrestatairesRoutingResolveService;
  let service: PrestatairesService;
  let resultPrestataires: IPrestataires | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(PrestatairesRoutingResolveService);
    service = TestBed.inject(PrestatairesService);
    resultPrestataires = undefined;
  });

  describe('resolve', () => {
    it('should return IPrestataires returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultPrestataires = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultPrestataires).toEqual({ id: 123 });
    });

    it('should return new IPrestataires if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultPrestataires = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultPrestataires).toEqual(new Prestataires());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as Prestataires })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultPrestataires = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultPrestataires).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
