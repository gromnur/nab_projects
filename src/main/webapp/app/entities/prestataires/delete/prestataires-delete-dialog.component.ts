import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IPrestataires } from '../prestataires.model';
import { PrestatairesService } from '../service/prestataires.service';

@Component({
  templateUrl: './prestataires-delete-dialog.component.html',
})
export class PrestatairesDeleteDialogComponent {
  prestataires?: IPrestataires;

  constructor(protected prestatairesService: PrestatairesService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.prestatairesService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
