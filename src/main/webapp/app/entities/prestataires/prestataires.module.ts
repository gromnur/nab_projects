import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { PrestatairesComponent } from './list/prestataires.component';
import { PrestatairesDetailComponent } from './detail/prestataires-detail.component';
import { PrestatairesUpdateComponent } from './update/prestataires-update.component';
import { PrestatairesDeleteDialogComponent } from './delete/prestataires-delete-dialog.component';
import { PrestatairesRoutingModule } from './route/prestataires-routing.module';

@NgModule({
  imports: [SharedModule, PrestatairesRoutingModule],
  declarations: [PrestatairesComponent, PrestatairesDetailComponent, PrestatairesUpdateComponent, PrestatairesDeleteDialogComponent],
  entryComponents: [PrestatairesDeleteDialogComponent],
})
export class PrestatairesModule {}
