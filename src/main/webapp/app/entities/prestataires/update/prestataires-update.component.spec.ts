jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { PrestatairesService } from '../service/prestataires.service';
import { IPrestataires, Prestataires } from '../prestataires.model';

import { PrestatairesUpdateComponent } from './prestataires-update.component';

describe('Prestataires Management Update Component', () => {
  let comp: PrestatairesUpdateComponent;
  let fixture: ComponentFixture<PrestatairesUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let prestatairesService: PrestatairesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [PrestatairesUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(PrestatairesUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PrestatairesUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    prestatairesService = TestBed.inject(PrestatairesService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const prestataires: IPrestataires = { id: 456 };

      activatedRoute.data = of({ prestataires });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(prestataires));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Prestataires>>();
      const prestataires = { id: 123 };
      jest.spyOn(prestatairesService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ prestataires });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: prestataires }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(prestatairesService.update).toHaveBeenCalledWith(prestataires);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Prestataires>>();
      const prestataires = new Prestataires();
      jest.spyOn(prestatairesService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ prestataires });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: prestataires }));
      saveSubject.complete();

      // THEN
      expect(prestatairesService.create).toHaveBeenCalledWith(prestataires);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Prestataires>>();
      const prestataires = { id: 123 };
      jest.spyOn(prestatairesService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ prestataires });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(prestatairesService.update).toHaveBeenCalledWith(prestataires);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
