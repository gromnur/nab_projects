import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IPrestataires, Prestataires } from '../prestataires.model';
import { PrestatairesService } from '../service/prestataires.service';

@Component({
  selector: 'jhi-prestataires-update',
  templateUrl: './prestataires-update.component.html',
})
export class PrestatairesUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    nomSociete: [null, [Validators.maxLength(50)]],
    nom: [null, [Validators.maxLength(50)]],
    prenom: [null, [Validators.maxLength(50)]],
    mail: [null, [Validators.maxLength(50)]],
    adresse: [null, [Validators.maxLength(100)]],
    microEntreprise: [],
    instagram: [null, [Validators.maxLength(50)]],
  });

  constructor(protected prestatairesService: PrestatairesService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ prestataires }) => {
      this.updateForm(prestataires);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const prestataires = this.createFromForm();
    if (prestataires.id !== undefined) {
      this.subscribeToSaveResponse(this.prestatairesService.update(prestataires));
    } else {
      this.subscribeToSaveResponse(this.prestatairesService.create(prestataires));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPrestataires>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(prestataires: IPrestataires): void {
    this.editForm.patchValue({
      id: prestataires.id,
      nomSociete: prestataires.nomSociete,
      nom: prestataires.nom,
      prenom: prestataires.prenom,
      mail: prestataires.mail,
      adresse: prestataires.adresse,
      microEntreprise: prestataires.microEntreprise,
      instagram: prestataires.instagram,
    });
  }

  protected createFromForm(): IPrestataires {
    return {
      ...new Prestataires(),
      id: this.editForm.get(['id'])!.value,
      nomSociete: this.editForm.get(['nomSociete'])!.value,
      nom: this.editForm.get(['nom'])!.value,
      prenom: this.editForm.get(['prenom'])!.value,
      mail: this.editForm.get(['mail'])!.value,
      adresse: this.editForm.get(['adresse'])!.value,
      microEntreprise: this.editForm.get(['microEntreprise'])!.value,
      instagram: this.editForm.get(['instagram'])!.value,
    };
  }
}
