import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPrestataires } from '../prestataires.model';

@Component({
  selector: 'jhi-prestataires-detail',
  templateUrl: './prestataires-detail.component.html',
})
export class PrestatairesDetailComponent implements OnInit {
  prestataires: IPrestataires | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ prestataires }) => {
      this.prestataires = prestataires;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
