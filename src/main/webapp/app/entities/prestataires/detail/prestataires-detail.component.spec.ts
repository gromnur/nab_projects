import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PrestatairesDetailComponent } from './prestataires-detail.component';

describe('Prestataires Management Detail Component', () => {
  let comp: PrestatairesDetailComponent;
  let fixture: ComponentFixture<PrestatairesDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PrestatairesDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ prestataires: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(PrestatairesDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(PrestatairesDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load prestataires on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.prestataires).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
