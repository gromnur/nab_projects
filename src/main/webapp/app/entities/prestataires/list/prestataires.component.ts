import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IPrestataires } from '../prestataires.model';
import { PrestatairesService } from '../service/prestataires.service';
import { PrestatairesDeleteDialogComponent } from '../delete/prestataires-delete-dialog.component';

@Component({
  selector: 'jhi-prestataires',
  templateUrl: './prestataires.component.html',
})
export class PrestatairesComponent implements OnInit {
  prestataires?: IPrestataires[];
  isLoading = false;

  constructor(protected prestatairesService: PrestatairesService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.prestatairesService.query().subscribe(
      (res: HttpResponse<IPrestataires[]>) => {
        this.isLoading = false;
        this.prestataires = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IPrestataires): number {
    return item.id!;
  }

  delete(prestataires: IPrestataires): void {
    const modalRef = this.modalService.open(PrestatairesDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.prestataires = prestataires;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
