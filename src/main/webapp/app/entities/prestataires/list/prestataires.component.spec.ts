import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { PrestatairesService } from '../service/prestataires.service';

import { PrestatairesComponent } from './prestataires.component';

describe('Prestataires Management Component', () => {
  let comp: PrestatairesComponent;
  let fixture: ComponentFixture<PrestatairesComponent>;
  let service: PrestatairesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [PrestatairesComponent],
    })
      .overrideTemplate(PrestatairesComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PrestatairesComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(PrestatairesService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.prestataires?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
