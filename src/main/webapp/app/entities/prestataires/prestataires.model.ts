import { IMissions } from 'app/entities/missions/missions.model';

export interface IPrestataires {
  id?: number;
  nomSociete?: string | null;
  nom?: string | null;
  prenom?: string | null;
  mail?: string | null;
  adresse?: string | null;
  microEntreprise?: boolean | null;
  instagram?: string | null;
  missions?: IMissions[] | null;
}

export class Prestataires implements IPrestataires {
  constructor(
    public id?: number,
    public nomSociete?: string | null,
    public nom?: string | null,
    public prenom?: string | null,
    public mail?: string | null,
    public adresse?: string | null,
    public microEntreprise?: boolean | null,
    public instagram?: string | null,
    public missions?: IMissions[] | null
  ) {
    this.microEntreprise = this.microEntreprise ?? false;
  }
}

export function getPrestatairesIdentifier(prestataires: IPrestataires): number | undefined {
  return prestataires.id;
}
