import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPrestataires, getPrestatairesIdentifier } from '../prestataires.model';

export type EntityResponseType = HttpResponse<IPrestataires>;
export type EntityArrayResponseType = HttpResponse<IPrestataires[]>;

@Injectable({ providedIn: 'root' })
export class PrestatairesService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/prestataires');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(prestataires: IPrestataires): Observable<EntityResponseType> {
    return this.http.post<IPrestataires>(this.resourceUrl, prestataires, { observe: 'response' });
  }

  update(prestataires: IPrestataires): Observable<EntityResponseType> {
    return this.http.put<IPrestataires>(`${this.resourceUrl}/${getPrestatairesIdentifier(prestataires) as number}`, prestataires, {
      observe: 'response',
    });
  }

  partialUpdate(prestataires: IPrestataires): Observable<EntityResponseType> {
    return this.http.patch<IPrestataires>(`${this.resourceUrl}/${getPrestatairesIdentifier(prestataires) as number}`, prestataires, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPrestataires>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPrestataires[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addPrestatairesToCollectionIfMissing(
    prestatairesCollection: IPrestataires[],
    ...prestatairesToCheck: (IPrestataires | null | undefined)[]
  ): IPrestataires[] {
    const prestataires: IPrestataires[] = prestatairesToCheck.filter(isPresent);
    if (prestataires.length > 0) {
      const prestatairesCollectionIdentifiers = prestatairesCollection.map(
        prestatairesItem => getPrestatairesIdentifier(prestatairesItem)!
      );
      const prestatairesToAdd = prestataires.filter(prestatairesItem => {
        const prestatairesIdentifier = getPrestatairesIdentifier(prestatairesItem);
        if (prestatairesIdentifier == null || prestatairesCollectionIdentifiers.includes(prestatairesIdentifier)) {
          return false;
        }
        prestatairesCollectionIdentifiers.push(prestatairesIdentifier);
        return true;
      });
      return [...prestatairesToAdd, ...prestatairesCollection];
    }
    return prestatairesCollection;
  }
}
