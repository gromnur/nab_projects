import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IPrestataires, Prestataires } from '../prestataires.model';

import { PrestatairesService } from './prestataires.service';

describe('Prestataires Service', () => {
  let service: PrestatairesService;
  let httpMock: HttpTestingController;
  let elemDefault: IPrestataires;
  let expectedResult: IPrestataires | IPrestataires[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(PrestatairesService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      nomSociete: 'AAAAAAA',
      nom: 'AAAAAAA',
      prenom: 'AAAAAAA',
      mail: 'AAAAAAA',
      adresse: 'AAAAAAA',
      microEntreprise: false,
      instagram: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Prestataires', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new Prestataires()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Prestataires', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nomSociete: 'BBBBBB',
          nom: 'BBBBBB',
          prenom: 'BBBBBB',
          mail: 'BBBBBB',
          adresse: 'BBBBBB',
          microEntreprise: true,
          instagram: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Prestataires', () => {
      const patchObject = Object.assign(
        {
          nomSociete: 'BBBBBB',
          nom: 'BBBBBB',
          mail: 'BBBBBB',
          microEntreprise: true,
        },
        new Prestataires()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Prestataires', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nomSociete: 'BBBBBB',
          nom: 'BBBBBB',
          prenom: 'BBBBBB',
          mail: 'BBBBBB',
          adresse: 'BBBBBB',
          microEntreprise: true,
          instagram: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Prestataires', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addPrestatairesToCollectionIfMissing', () => {
      it('should add a Prestataires to an empty array', () => {
        const prestataires: IPrestataires = { id: 123 };
        expectedResult = service.addPrestatairesToCollectionIfMissing([], prestataires);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(prestataires);
      });

      it('should not add a Prestataires to an array that contains it', () => {
        const prestataires: IPrestataires = { id: 123 };
        const prestatairesCollection: IPrestataires[] = [
          {
            ...prestataires,
          },
          { id: 456 },
        ];
        expectedResult = service.addPrestatairesToCollectionIfMissing(prestatairesCollection, prestataires);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Prestataires to an array that doesn't contain it", () => {
        const prestataires: IPrestataires = { id: 123 };
        const prestatairesCollection: IPrestataires[] = [{ id: 456 }];
        expectedResult = service.addPrestatairesToCollectionIfMissing(prestatairesCollection, prestataires);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(prestataires);
      });

      it('should add only unique Prestataires to an array', () => {
        const prestatairesArray: IPrestataires[] = [{ id: 123 }, { id: 456 }, { id: 82601 }];
        const prestatairesCollection: IPrestataires[] = [{ id: 123 }];
        expectedResult = service.addPrestatairesToCollectionIfMissing(prestatairesCollection, ...prestatairesArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const prestataires: IPrestataires = { id: 123 };
        const prestataires2: IPrestataires = { id: 456 };
        expectedResult = service.addPrestatairesToCollectionIfMissing([], prestataires, prestataires2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(prestataires);
        expect(expectedResult).toContain(prestataires2);
      });

      it('should accept null and undefined values', () => {
        const prestataires: IPrestataires = { id: 123 };
        expectedResult = service.addPrestatairesToCollectionIfMissing([], null, prestataires, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(prestataires);
      });

      it('should return initial array if no Prestataires is added', () => {
        const prestatairesCollection: IPrestataires[] = [{ id: 123 }];
        expectedResult = service.addPrestatairesToCollectionIfMissing(prestatairesCollection, undefined, null);
        expect(expectedResult).toEqual(prestatairesCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
