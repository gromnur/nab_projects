import { IMissions } from 'app/entities/missions/missions.model';

export interface IPhotos {
  id?: number;
  lien?: string | null;
  mission?: IMissions | null;
}

export class Photos implements IPhotos {
  constructor(public id?: number, public lien?: string | null, public mission?: IMissions | null) {}
}

export function getPhotosIdentifier(photos: IPhotos): number | undefined {
  return photos.id;
}
