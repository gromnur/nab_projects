import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PhotosDetailComponent } from './photos-detail.component';

describe('Photos Management Detail Component', () => {
  let comp: PhotosDetailComponent;
  let fixture: ComponentFixture<PhotosDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PhotosDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ photos: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(PhotosDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(PhotosDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load photos on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.photos).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
