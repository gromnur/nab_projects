import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IPhotos, Photos } from '../photos.model';

import { PhotosService } from './photos.service';

describe('Photos Service', () => {
  let service: PhotosService;
  let httpMock: HttpTestingController;
  let elemDefault: IPhotos;
  let expectedResult: IPhotos | IPhotos[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(PhotosService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      lien: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Photos', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new Photos()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Photos', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          lien: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Photos', () => {
      const patchObject = Object.assign(
        {
          lien: 'BBBBBB',
        },
        new Photos()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Photos', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          lien: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Photos', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addPhotosToCollectionIfMissing', () => {
      it('should add a Photos to an empty array', () => {
        const photos: IPhotos = { id: 123 };
        expectedResult = service.addPhotosToCollectionIfMissing([], photos);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(photos);
      });

      it('should not add a Photos to an array that contains it', () => {
        const photos: IPhotos = { id: 123 };
        const photosCollection: IPhotos[] = [
          {
            ...photos,
          },
          { id: 456 },
        ];
        expectedResult = service.addPhotosToCollectionIfMissing(photosCollection, photos);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Photos to an array that doesn't contain it", () => {
        const photos: IPhotos = { id: 123 };
        const photosCollection: IPhotos[] = [{ id: 456 }];
        expectedResult = service.addPhotosToCollectionIfMissing(photosCollection, photos);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(photos);
      });

      it('should add only unique Photos to an array', () => {
        const photosArray: IPhotos[] = [{ id: 123 }, { id: 456 }, { id: 22613 }];
        const photosCollection: IPhotos[] = [{ id: 123 }];
        expectedResult = service.addPhotosToCollectionIfMissing(photosCollection, ...photosArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const photos: IPhotos = { id: 123 };
        const photos2: IPhotos = { id: 456 };
        expectedResult = service.addPhotosToCollectionIfMissing([], photos, photos2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(photos);
        expect(expectedResult).toContain(photos2);
      });

      it('should accept null and undefined values', () => {
        const photos: IPhotos = { id: 123 };
        expectedResult = service.addPhotosToCollectionIfMissing([], null, photos, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(photos);
      });

      it('should return initial array if no Photos is added', () => {
        const photosCollection: IPhotos[] = [{ id: 123 }];
        expectedResult = service.addPhotosToCollectionIfMissing(photosCollection, undefined, null);
        expect(expectedResult).toEqual(photosCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
