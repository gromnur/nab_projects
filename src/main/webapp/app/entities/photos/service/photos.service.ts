import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPhotos, getPhotosIdentifier } from '../photos.model';

export type EntityResponseType = HttpResponse<IPhotos>;
export type EntityArrayResponseType = HttpResponse<IPhotos[]>;

@Injectable({ providedIn: 'root' })
export class PhotosService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/photos');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(photos: IPhotos): Observable<EntityResponseType> {
    return this.http.post<IPhotos>(this.resourceUrl, photos, { observe: 'response' });
  }

  update(photos: IPhotos): Observable<EntityResponseType> {
    return this.http.put<IPhotos>(`${this.resourceUrl}/${getPhotosIdentifier(photos) as number}`, photos, { observe: 'response' });
  }

  partialUpdate(photos: IPhotos): Observable<EntityResponseType> {
    return this.http.patch<IPhotos>(`${this.resourceUrl}/${getPhotosIdentifier(photos) as number}`, photos, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPhotos>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPhotos[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addPhotosToCollectionIfMissing(photosCollection: IPhotos[], ...photosToCheck: (IPhotos | null | undefined)[]): IPhotos[] {
    const photos: IPhotos[] = photosToCheck.filter(isPresent);
    if (photos.length > 0) {
      const photosCollectionIdentifiers = photosCollection.map(photosItem => getPhotosIdentifier(photosItem)!);
      const photosToAdd = photos.filter(photosItem => {
        const photosIdentifier = getPhotosIdentifier(photosItem);
        if (photosIdentifier == null || photosCollectionIdentifiers.includes(photosIdentifier)) {
          return false;
        }
        photosCollectionIdentifiers.push(photosIdentifier);
        return true;
      });
      return [...photosToAdd, ...photosCollection];
    }
    return photosCollection;
  }
}
