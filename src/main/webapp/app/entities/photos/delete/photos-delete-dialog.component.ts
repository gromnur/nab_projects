import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IPhotos } from '../photos.model';
import { PhotosService } from '../service/photos.service';

@Component({
  templateUrl: './photos-delete-dialog.component.html',
})
export class PhotosDeleteDialogComponent {
  photos?: IPhotos;

  constructor(protected photosService: PhotosService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.photosService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
