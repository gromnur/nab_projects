import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { PhotosService } from '../service/photos.service';

import { PhotosComponent } from './photos.component';

describe('Photos Management Component', () => {
  let comp: PhotosComponent;
  let fixture: ComponentFixture<PhotosComponent>;
  let service: PhotosService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [PhotosComponent],
    })
      .overrideTemplate(PhotosComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PhotosComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(PhotosService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.photos?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
