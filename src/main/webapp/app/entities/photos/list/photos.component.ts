import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IPhotos } from '../photos.model';
import { PhotosService } from '../service/photos.service';
import { PhotosDeleteDialogComponent } from '../delete/photos-delete-dialog.component';

@Component({
  selector: 'jhi-photos',
  templateUrl: './photos.component.html',
})
export class PhotosComponent implements OnInit {
  photos?: IPhotos[];
  isLoading = false;

  constructor(protected photosService: PhotosService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.photosService.query().subscribe(
      (res: HttpResponse<IPhotos[]>) => {
        this.isLoading = false;
        this.photos = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IPhotos): number {
    return item.id!;
  }

  delete(photos: IPhotos): void {
    const modalRef = this.modalService.open(PhotosDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.photos = photos;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
