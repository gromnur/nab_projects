import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IPhotos, Photos } from '../photos.model';
import { PhotosService } from '../service/photos.service';

@Injectable({ providedIn: 'root' })
export class PhotosRoutingResolveService implements Resolve<IPhotos> {
  constructor(protected service: PhotosService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPhotos> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((photos: HttpResponse<Photos>) => {
          if (photos.body) {
            return of(photos.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Photos());
  }
}
