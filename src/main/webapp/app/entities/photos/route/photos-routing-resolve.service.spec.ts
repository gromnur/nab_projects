jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IPhotos, Photos } from '../photos.model';
import { PhotosService } from '../service/photos.service';

import { PhotosRoutingResolveService } from './photos-routing-resolve.service';

describe('Photos routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: PhotosRoutingResolveService;
  let service: PhotosService;
  let resultPhotos: IPhotos | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(PhotosRoutingResolveService);
    service = TestBed.inject(PhotosService);
    resultPhotos = undefined;
  });

  describe('resolve', () => {
    it('should return IPhotos returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultPhotos = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultPhotos).toEqual({ id: 123 });
    });

    it('should return new IPhotos if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultPhotos = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultPhotos).toEqual(new Photos());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as Photos })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultPhotos = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultPhotos).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
