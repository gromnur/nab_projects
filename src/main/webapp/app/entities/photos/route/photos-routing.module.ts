import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { PhotosComponent } from '../list/photos.component';
import { PhotosDetailComponent } from '../detail/photos-detail.component';
import { PhotosUpdateComponent } from '../update/photos-update.component';
import { PhotosRoutingResolveService } from './photos-routing-resolve.service';

const photosRoute: Routes = [
  {
    path: '',
    component: PhotosComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PhotosDetailComponent,
    resolve: {
      photos: PhotosRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PhotosUpdateComponent,
    resolve: {
      photos: PhotosRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PhotosUpdateComponent,
    resolve: {
      photos: PhotosRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(photosRoute)],
  exports: [RouterModule],
})
export class PhotosRoutingModule {}
