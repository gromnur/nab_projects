jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { PhotosService } from '../service/photos.service';
import { IPhotos, Photos } from '../photos.model';
import { IMissions } from 'app/entities/missions/missions.model';
import { MissionsService } from 'app/entities/missions/service/missions.service';

import { PhotosUpdateComponent } from './photos-update.component';

describe('Photos Management Update Component', () => {
  let comp: PhotosUpdateComponent;
  let fixture: ComponentFixture<PhotosUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let photosService: PhotosService;
  let missionsService: MissionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [PhotosUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(PhotosUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PhotosUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    photosService = TestBed.inject(PhotosService);
    missionsService = TestBed.inject(MissionsService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Missions query and add missing value', () => {
      const photos: IPhotos = { id: 456 };
      const mission: IMissions = { id: 80528 };
      photos.mission = mission;

      const missionsCollection: IMissions[] = [{ id: 21729 }];
      jest.spyOn(missionsService, 'query').mockReturnValue(of(new HttpResponse({ body: missionsCollection })));
      const additionalMissions = [mission];
      const expectedCollection: IMissions[] = [...additionalMissions, ...missionsCollection];
      jest.spyOn(missionsService, 'addMissionsToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ photos });
      comp.ngOnInit();

      expect(missionsService.query).toHaveBeenCalled();
      expect(missionsService.addMissionsToCollectionIfMissing).toHaveBeenCalledWith(missionsCollection, ...additionalMissions);
      expect(comp.missionsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const photos: IPhotos = { id: 456 };
      const mission: IMissions = { id: 47381 };
      photos.mission = mission;

      activatedRoute.data = of({ photos });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(photos));
      expect(comp.missionsSharedCollection).toContain(mission);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Photos>>();
      const photos = { id: 123 };
      jest.spyOn(photosService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ photos });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: photos }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(photosService.update).toHaveBeenCalledWith(photos);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Photos>>();
      const photos = new Photos();
      jest.spyOn(photosService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ photos });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: photos }));
      saveSubject.complete();

      // THEN
      expect(photosService.create).toHaveBeenCalledWith(photos);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Photos>>();
      const photos = { id: 123 };
      jest.spyOn(photosService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ photos });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(photosService.update).toHaveBeenCalledWith(photos);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackMissionsById', () => {
      it('Should return tracked Missions primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackMissionsById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
