import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IPhotos, Photos } from '../photos.model';
import { PhotosService } from '../service/photos.service';
import { IMissions } from 'app/entities/missions/missions.model';
import { MissionsService } from 'app/entities/missions/service/missions.service';

@Component({
  selector: 'jhi-photos-update',
  templateUrl: './photos-update.component.html',
})
export class PhotosUpdateComponent implements OnInit {
  isSaving = false;

  missionsSharedCollection: IMissions[] = [];

  editForm = this.fb.group({
    id: [],
    lien: [],
    mission: [],
  });

  constructor(
    protected photosService: PhotosService,
    protected missionsService: MissionsService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ photos }) => {
      this.updateForm(photos);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const photos = this.createFromForm();
    if (photos.id !== undefined) {
      this.subscribeToSaveResponse(this.photosService.update(photos));
    } else {
      this.subscribeToSaveResponse(this.photosService.create(photos));
    }
  }

  trackMissionsById(index: number, item: IMissions): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPhotos>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(photos: IPhotos): void {
    this.editForm.patchValue({
      id: photos.id,
      lien: photos.lien,
      mission: photos.mission,
    });

    this.missionsSharedCollection = this.missionsService.addMissionsToCollectionIfMissing(this.missionsSharedCollection, photos.mission);
  }

  protected loadRelationshipsOptions(): void {
    this.missionsService
      .query()
      .pipe(map((res: HttpResponse<IMissions[]>) => res.body ?? []))
      .pipe(
        map((missions: IMissions[]) => this.missionsService.addMissionsToCollectionIfMissing(missions, this.editForm.get('mission')!.value))
      )
      .subscribe((missions: IMissions[]) => (this.missionsSharedCollection = missions));
  }

  protected createFromForm(): IPhotos {
    return {
      ...new Photos(),
      id: this.editForm.get(['id'])!.value,
      lien: this.editForm.get(['lien'])!.value,
      mission: this.editForm.get(['mission'])!.value,
    };
  }
}
