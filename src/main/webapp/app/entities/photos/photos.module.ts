import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { PhotosComponent } from './list/photos.component';
import { PhotosDetailComponent } from './detail/photos-detail.component';
import { PhotosUpdateComponent } from './update/photos-update.component';
import { PhotosDeleteDialogComponent } from './delete/photos-delete-dialog.component';
import { PhotosRoutingModule } from './route/photos-routing.module';

@NgModule({
  imports: [SharedModule, PhotosRoutingModule],
  declarations: [PhotosComponent, PhotosDetailComponent, PhotosUpdateComponent, PhotosDeleteDialogComponent],
  entryComponents: [PhotosDeleteDialogComponent],
})
export class PhotosModule {}
