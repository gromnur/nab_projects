import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IStatusCreatifsHistorique, StatusCreatifsHistorique } from '../status-creatifs-historique.model';
import { StatusCreatifsHistoriqueService } from '../service/status-creatifs-historique.service';
import { StatusCreatifs } from 'app/entities/enumerations/status-creatifs.model';

@Component({
  selector: 'jhi-status-creatifs-historique-update',
  templateUrl: './status-creatifs-historique-update.component.html',
})
export class StatusCreatifsHistoriqueUpdateComponent implements OnInit {
  isSaving = false;
  statusCreatifsValues = Object.keys(StatusCreatifs);

  editForm = this.fb.group({
    id: [],
    dateModification: [],
    status: [],
  });

  constructor(
    protected statusCreatifsHistoriqueService: StatusCreatifsHistoriqueService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ statusCreatifsHistorique }) => {
      this.updateForm(statusCreatifsHistorique);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const statusCreatifsHistorique = this.createFromForm();
    if (statusCreatifsHistorique.id !== undefined) {
      this.subscribeToSaveResponse(this.statusCreatifsHistoriqueService.update(statusCreatifsHistorique));
    } else {
      this.subscribeToSaveResponse(this.statusCreatifsHistoriqueService.create(statusCreatifsHistorique));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IStatusCreatifsHistorique>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(statusCreatifsHistorique: IStatusCreatifsHistorique): void {
    this.editForm.patchValue({
      id: statusCreatifsHistorique.id,
      dateModification: statusCreatifsHistorique.dateModification,
      status: statusCreatifsHistorique.status,
    });
  }

  protected createFromForm(): IStatusCreatifsHistorique {
    return {
      ...new StatusCreatifsHistorique(),
      id: this.editForm.get(['id'])!.value,
      dateModification: this.editForm.get(['dateModification'])!.value,
      status: this.editForm.get(['status'])!.value,
    };
  }
}
