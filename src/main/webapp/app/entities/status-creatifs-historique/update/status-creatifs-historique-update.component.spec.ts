jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { StatusCreatifsHistoriqueService } from '../service/status-creatifs-historique.service';
import { IStatusCreatifsHistorique, StatusCreatifsHistorique } from '../status-creatifs-historique.model';

import { StatusCreatifsHistoriqueUpdateComponent } from './status-creatifs-historique-update.component';

describe('StatusCreatifsHistorique Management Update Component', () => {
  let comp: StatusCreatifsHistoriqueUpdateComponent;
  let fixture: ComponentFixture<StatusCreatifsHistoriqueUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let statusCreatifsHistoriqueService: StatusCreatifsHistoriqueService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [StatusCreatifsHistoriqueUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(StatusCreatifsHistoriqueUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(StatusCreatifsHistoriqueUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    statusCreatifsHistoriqueService = TestBed.inject(StatusCreatifsHistoriqueService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const statusCreatifsHistorique: IStatusCreatifsHistorique = { id: 456 };

      activatedRoute.data = of({ statusCreatifsHistorique });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(statusCreatifsHistorique));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<StatusCreatifsHistorique>>();
      const statusCreatifsHistorique = { id: 123 };
      jest.spyOn(statusCreatifsHistoriqueService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ statusCreatifsHistorique });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: statusCreatifsHistorique }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(statusCreatifsHistoriqueService.update).toHaveBeenCalledWith(statusCreatifsHistorique);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<StatusCreatifsHistorique>>();
      const statusCreatifsHistorique = new StatusCreatifsHistorique();
      jest.spyOn(statusCreatifsHistoriqueService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ statusCreatifsHistorique });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: statusCreatifsHistorique }));
      saveSubject.complete();

      // THEN
      expect(statusCreatifsHistoriqueService.create).toHaveBeenCalledWith(statusCreatifsHistorique);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<StatusCreatifsHistorique>>();
      const statusCreatifsHistorique = { id: 123 };
      jest.spyOn(statusCreatifsHistoriqueService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ statusCreatifsHistorique });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(statusCreatifsHistoriqueService.update).toHaveBeenCalledWith(statusCreatifsHistorique);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
