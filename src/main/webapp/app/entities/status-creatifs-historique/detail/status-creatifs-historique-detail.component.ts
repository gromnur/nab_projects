import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IStatusCreatifsHistorique } from '../status-creatifs-historique.model';

@Component({
  selector: 'jhi-status-creatifs-historique-detail',
  templateUrl: './status-creatifs-historique-detail.component.html',
})
export class StatusCreatifsHistoriqueDetailComponent implements OnInit {
  statusCreatifsHistorique: IStatusCreatifsHistorique | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ statusCreatifsHistorique }) => {
      this.statusCreatifsHistorique = statusCreatifsHistorique;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
