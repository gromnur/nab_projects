import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { StatusCreatifsHistoriqueDetailComponent } from './status-creatifs-historique-detail.component';

describe('StatusCreatifsHistorique Management Detail Component', () => {
  let comp: StatusCreatifsHistoriqueDetailComponent;
  let fixture: ComponentFixture<StatusCreatifsHistoriqueDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [StatusCreatifsHistoriqueDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ statusCreatifsHistorique: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(StatusCreatifsHistoriqueDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(StatusCreatifsHistoriqueDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load statusCreatifsHistorique on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.statusCreatifsHistorique).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
