import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IStatusCreatifsHistorique } from '../status-creatifs-historique.model';
import { StatusCreatifsHistoriqueService } from '../service/status-creatifs-historique.service';

@Component({
  templateUrl: './status-creatifs-historique-delete-dialog.component.html',
})
export class StatusCreatifsHistoriqueDeleteDialogComponent {
  statusCreatifsHistorique?: IStatusCreatifsHistorique;

  constructor(protected statusCreatifsHistoriqueService: StatusCreatifsHistoriqueService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.statusCreatifsHistoriqueService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
