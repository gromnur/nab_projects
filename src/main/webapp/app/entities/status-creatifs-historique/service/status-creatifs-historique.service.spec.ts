import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_FORMAT } from 'app/config/input.constants';
import { StatusCreatifs } from 'app/entities/enumerations/status-creatifs.model';
import { IStatusCreatifsHistorique, StatusCreatifsHistorique } from '../status-creatifs-historique.model';

import { StatusCreatifsHistoriqueService } from './status-creatifs-historique.service';

describe('StatusCreatifsHistorique Service', () => {
  let service: StatusCreatifsHistoriqueService;
  let httpMock: HttpTestingController;
  let elemDefault: IStatusCreatifsHistorique;
  let expectedResult: IStatusCreatifsHistorique | IStatusCreatifsHistorique[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(StatusCreatifsHistoriqueService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      dateModification: currentDate,
      status: StatusCreatifs.ACTIF,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          dateModification: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a StatusCreatifsHistorique', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          dateModification: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateModification: currentDate,
        },
        returnedFromService
      );

      service.create(new StatusCreatifsHistorique()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a StatusCreatifsHistorique', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          dateModification: currentDate.format(DATE_FORMAT),
          status: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateModification: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a StatusCreatifsHistorique', () => {
      const patchObject = Object.assign(
        {
          dateModification: currentDate.format(DATE_FORMAT),
        },
        new StatusCreatifsHistorique()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          dateModification: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of StatusCreatifsHistorique', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          dateModification: currentDate.format(DATE_FORMAT),
          status: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateModification: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a StatusCreatifsHistorique', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addStatusCreatifsHistoriqueToCollectionIfMissing', () => {
      it('should add a StatusCreatifsHistorique to an empty array', () => {
        const statusCreatifsHistorique: IStatusCreatifsHistorique = { id: 123 };
        expectedResult = service.addStatusCreatifsHistoriqueToCollectionIfMissing([], statusCreatifsHistorique);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(statusCreatifsHistorique);
      });

      it('should not add a StatusCreatifsHistorique to an array that contains it', () => {
        const statusCreatifsHistorique: IStatusCreatifsHistorique = { id: 123 };
        const statusCreatifsHistoriqueCollection: IStatusCreatifsHistorique[] = [
          {
            ...statusCreatifsHistorique,
          },
          { id: 456 },
        ];
        expectedResult = service.addStatusCreatifsHistoriqueToCollectionIfMissing(
          statusCreatifsHistoriqueCollection,
          statusCreatifsHistorique
        );
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a StatusCreatifsHistorique to an array that doesn't contain it", () => {
        const statusCreatifsHistorique: IStatusCreatifsHistorique = { id: 123 };
        const statusCreatifsHistoriqueCollection: IStatusCreatifsHistorique[] = [{ id: 456 }];
        expectedResult = service.addStatusCreatifsHistoriqueToCollectionIfMissing(
          statusCreatifsHistoriqueCollection,
          statusCreatifsHistorique
        );
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(statusCreatifsHistorique);
      });

      it('should add only unique StatusCreatifsHistorique to an array', () => {
        const statusCreatifsHistoriqueArray: IStatusCreatifsHistorique[] = [{ id: 123 }, { id: 456 }, { id: 66890 }];
        const statusCreatifsHistoriqueCollection: IStatusCreatifsHistorique[] = [{ id: 123 }];
        expectedResult = service.addStatusCreatifsHistoriqueToCollectionIfMissing(
          statusCreatifsHistoriqueCollection,
          ...statusCreatifsHistoriqueArray
        );
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const statusCreatifsHistorique: IStatusCreatifsHistorique = { id: 123 };
        const statusCreatifsHistorique2: IStatusCreatifsHistorique = { id: 456 };
        expectedResult = service.addStatusCreatifsHistoriqueToCollectionIfMissing([], statusCreatifsHistorique, statusCreatifsHistorique2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(statusCreatifsHistorique);
        expect(expectedResult).toContain(statusCreatifsHistorique2);
      });

      it('should accept null and undefined values', () => {
        const statusCreatifsHistorique: IStatusCreatifsHistorique = { id: 123 };
        expectedResult = service.addStatusCreatifsHistoriqueToCollectionIfMissing([], null, statusCreatifsHistorique, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(statusCreatifsHistorique);
      });

      it('should return initial array if no StatusCreatifsHistorique is added', () => {
        const statusCreatifsHistoriqueCollection: IStatusCreatifsHistorique[] = [{ id: 123 }];
        expectedResult = service.addStatusCreatifsHistoriqueToCollectionIfMissing(statusCreatifsHistoriqueCollection, undefined, null);
        expect(expectedResult).toEqual(statusCreatifsHistoriqueCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
