import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IStatusCreatifsHistorique, getStatusCreatifsHistoriqueIdentifier } from '../status-creatifs-historique.model';

export type EntityResponseType = HttpResponse<IStatusCreatifsHistorique>;
export type EntityArrayResponseType = HttpResponse<IStatusCreatifsHistorique[]>;

@Injectable({ providedIn: 'root' })
export class StatusCreatifsHistoriqueService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/status-creatifs-historiques');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(statusCreatifsHistorique: IStatusCreatifsHistorique): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(statusCreatifsHistorique);
    return this.http
      .post<IStatusCreatifsHistorique>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(statusCreatifsHistorique: IStatusCreatifsHistorique): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(statusCreatifsHistorique);
    return this.http
      .put<IStatusCreatifsHistorique>(
        `${this.resourceUrl}/${getStatusCreatifsHistoriqueIdentifier(statusCreatifsHistorique) as number}`,
        copy,
        { observe: 'response' }
      )
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(statusCreatifsHistorique: IStatusCreatifsHistorique): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(statusCreatifsHistorique);
    return this.http
      .patch<IStatusCreatifsHistorique>(
        `${this.resourceUrl}/${getStatusCreatifsHistoriqueIdentifier(statusCreatifsHistorique) as number}`,
        copy,
        { observe: 'response' }
      )
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IStatusCreatifsHistorique>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IStatusCreatifsHistorique[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addStatusCreatifsHistoriqueToCollectionIfMissing(
    statusCreatifsHistoriqueCollection: IStatusCreatifsHistorique[],
    ...statusCreatifsHistoriquesToCheck: (IStatusCreatifsHistorique | null | undefined)[]
  ): IStatusCreatifsHistorique[] {
    const statusCreatifsHistoriques: IStatusCreatifsHistorique[] = statusCreatifsHistoriquesToCheck.filter(isPresent);
    if (statusCreatifsHistoriques.length > 0) {
      const statusCreatifsHistoriqueCollectionIdentifiers = statusCreatifsHistoriqueCollection.map(
        statusCreatifsHistoriqueItem => getStatusCreatifsHistoriqueIdentifier(statusCreatifsHistoriqueItem)!
      );
      const statusCreatifsHistoriquesToAdd = statusCreatifsHistoriques.filter(statusCreatifsHistoriqueItem => {
        const statusCreatifsHistoriqueIdentifier = getStatusCreatifsHistoriqueIdentifier(statusCreatifsHistoriqueItem);
        if (
          statusCreatifsHistoriqueIdentifier == null ||
          statusCreatifsHistoriqueCollectionIdentifiers.includes(statusCreatifsHistoriqueIdentifier)
        ) {
          return false;
        }
        statusCreatifsHistoriqueCollectionIdentifiers.push(statusCreatifsHistoriqueIdentifier);
        return true;
      });
      return [...statusCreatifsHistoriquesToAdd, ...statusCreatifsHistoriqueCollection];
    }
    return statusCreatifsHistoriqueCollection;
  }

  protected convertDateFromClient(statusCreatifsHistorique: IStatusCreatifsHistorique): IStatusCreatifsHistorique {
    return Object.assign({}, statusCreatifsHistorique, {
      dateModification: statusCreatifsHistorique.dateModification?.isValid()
        ? statusCreatifsHistorique.dateModification.format(DATE_FORMAT)
        : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateModification = res.body.dateModification ? dayjs(res.body.dateModification) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((statusCreatifsHistorique: IStatusCreatifsHistorique) => {
        statusCreatifsHistorique.dateModification = statusCreatifsHistorique.dateModification
          ? dayjs(statusCreatifsHistorique.dateModification)
          : undefined;
      });
    }
    return res;
  }
}
