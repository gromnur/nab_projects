import * as dayjs from 'dayjs';
import { ICreatifs } from 'app/entities/creatifs/creatifs.model';
import { StatusCreatifs } from 'app/entities/enumerations/status-creatifs.model';

export interface IStatusCreatifsHistorique {
  id?: number;
  dateModification?: dayjs.Dayjs | null;
  status?: StatusCreatifs | null;
  creatifs?: ICreatifs[] | null;
}

export class StatusCreatifsHistorique implements IStatusCreatifsHistorique {
  constructor(
    public id?: number,
    public dateModification?: dayjs.Dayjs | null,
    public status?: StatusCreatifs | null,
    public creatifs?: ICreatifs[] | null
  ) {}
}

export function getStatusCreatifsHistoriqueIdentifier(statusCreatifsHistorique: IStatusCreatifsHistorique): number | undefined {
  return statusCreatifsHistorique.id;
}
