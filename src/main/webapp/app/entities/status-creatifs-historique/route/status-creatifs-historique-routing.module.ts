import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { StatusCreatifsHistoriqueComponent } from '../list/status-creatifs-historique.component';
import { StatusCreatifsHistoriqueDetailComponent } from '../detail/status-creatifs-historique-detail.component';
import { StatusCreatifsHistoriqueUpdateComponent } from '../update/status-creatifs-historique-update.component';
import { StatusCreatifsHistoriqueRoutingResolveService } from './status-creatifs-historique-routing-resolve.service';

const statusCreatifsHistoriqueRoute: Routes = [
  {
    path: '',
    component: StatusCreatifsHistoriqueComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: StatusCreatifsHistoriqueDetailComponent,
    resolve: {
      statusCreatifsHistorique: StatusCreatifsHistoriqueRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: StatusCreatifsHistoriqueUpdateComponent,
    resolve: {
      statusCreatifsHistorique: StatusCreatifsHistoriqueRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: StatusCreatifsHistoriqueUpdateComponent,
    resolve: {
      statusCreatifsHistorique: StatusCreatifsHistoriqueRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(statusCreatifsHistoriqueRoute)],
  exports: [RouterModule],
})
export class StatusCreatifsHistoriqueRoutingModule {}
