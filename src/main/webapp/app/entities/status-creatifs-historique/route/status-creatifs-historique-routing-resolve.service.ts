import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IStatusCreatifsHistorique, StatusCreatifsHistorique } from '../status-creatifs-historique.model';
import { StatusCreatifsHistoriqueService } from '../service/status-creatifs-historique.service';

@Injectable({ providedIn: 'root' })
export class StatusCreatifsHistoriqueRoutingResolveService implements Resolve<IStatusCreatifsHistorique> {
  constructor(protected service: StatusCreatifsHistoriqueService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IStatusCreatifsHistorique> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((statusCreatifsHistorique: HttpResponse<StatusCreatifsHistorique>) => {
          if (statusCreatifsHistorique.body) {
            return of(statusCreatifsHistorique.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new StatusCreatifsHistorique());
  }
}
