jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IStatusCreatifsHistorique, StatusCreatifsHistorique } from '../status-creatifs-historique.model';
import { StatusCreatifsHistoriqueService } from '../service/status-creatifs-historique.service';

import { StatusCreatifsHistoriqueRoutingResolveService } from './status-creatifs-historique-routing-resolve.service';

describe('StatusCreatifsHistorique routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: StatusCreatifsHistoriqueRoutingResolveService;
  let service: StatusCreatifsHistoriqueService;
  let resultStatusCreatifsHistorique: IStatusCreatifsHistorique | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(StatusCreatifsHistoriqueRoutingResolveService);
    service = TestBed.inject(StatusCreatifsHistoriqueService);
    resultStatusCreatifsHistorique = undefined;
  });

  describe('resolve', () => {
    it('should return IStatusCreatifsHistorique returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultStatusCreatifsHistorique = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultStatusCreatifsHistorique).toEqual({ id: 123 });
    });

    it('should return new IStatusCreatifsHistorique if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultStatusCreatifsHistorique = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultStatusCreatifsHistorique).toEqual(new StatusCreatifsHistorique());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as StatusCreatifsHistorique })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultStatusCreatifsHistorique = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultStatusCreatifsHistorique).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
