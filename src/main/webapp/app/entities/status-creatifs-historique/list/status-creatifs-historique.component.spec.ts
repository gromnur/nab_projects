import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { StatusCreatifsHistoriqueService } from '../service/status-creatifs-historique.service';

import { StatusCreatifsHistoriqueComponent } from './status-creatifs-historique.component';

describe('StatusCreatifsHistorique Management Component', () => {
  let comp: StatusCreatifsHistoriqueComponent;
  let fixture: ComponentFixture<StatusCreatifsHistoriqueComponent>;
  let service: StatusCreatifsHistoriqueService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [StatusCreatifsHistoriqueComponent],
    })
      .overrideTemplate(StatusCreatifsHistoriqueComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(StatusCreatifsHistoriqueComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(StatusCreatifsHistoriqueService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.statusCreatifsHistoriques?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
