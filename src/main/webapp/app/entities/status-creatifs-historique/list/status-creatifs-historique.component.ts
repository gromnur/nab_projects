import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IStatusCreatifsHistorique } from '../status-creatifs-historique.model';
import { StatusCreatifsHistoriqueService } from '../service/status-creatifs-historique.service';
import { StatusCreatifsHistoriqueDeleteDialogComponent } from '../delete/status-creatifs-historique-delete-dialog.component';

@Component({
  selector: 'jhi-status-creatifs-historique',
  templateUrl: './status-creatifs-historique.component.html',
})
export class StatusCreatifsHistoriqueComponent implements OnInit {
  statusCreatifsHistoriques?: IStatusCreatifsHistorique[];
  isLoading = false;

  constructor(protected statusCreatifsHistoriqueService: StatusCreatifsHistoriqueService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.statusCreatifsHistoriqueService.query().subscribe(
      (res: HttpResponse<IStatusCreatifsHistorique[]>) => {
        this.isLoading = false;
        this.statusCreatifsHistoriques = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IStatusCreatifsHistorique): number {
    return item.id!;
  }

  delete(statusCreatifsHistorique: IStatusCreatifsHistorique): void {
    const modalRef = this.modalService.open(StatusCreatifsHistoriqueDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.statusCreatifsHistorique = statusCreatifsHistorique;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
