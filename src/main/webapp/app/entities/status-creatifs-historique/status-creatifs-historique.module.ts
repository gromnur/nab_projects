import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { StatusCreatifsHistoriqueComponent } from './list/status-creatifs-historique.component';
import { StatusCreatifsHistoriqueDetailComponent } from './detail/status-creatifs-historique-detail.component';
import { StatusCreatifsHistoriqueUpdateComponent } from './update/status-creatifs-historique-update.component';
import { StatusCreatifsHistoriqueDeleteDialogComponent } from './delete/status-creatifs-historique-delete-dialog.component';
import { StatusCreatifsHistoriqueRoutingModule } from './route/status-creatifs-historique-routing.module';

@NgModule({
  imports: [SharedModule, StatusCreatifsHistoriqueRoutingModule],
  declarations: [
    StatusCreatifsHistoriqueComponent,
    StatusCreatifsHistoriqueDetailComponent,
    StatusCreatifsHistoriqueUpdateComponent,
    StatusCreatifsHistoriqueDeleteDialogComponent,
  ],
  entryComponents: [StatusCreatifsHistoriqueDeleteDialogComponent],
})
export class StatusCreatifsHistoriqueModule {}
