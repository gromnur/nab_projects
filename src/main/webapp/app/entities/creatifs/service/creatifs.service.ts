import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICreatifs, getCreatifsIdentifier } from '../creatifs.model';

export type EntityResponseType = HttpResponse<ICreatifs>;
export type EntityArrayResponseType = HttpResponse<ICreatifs[]>;

@Injectable({ providedIn: 'root' })
export class CreatifsService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/creatifs');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(creatifs: ICreatifs): Observable<EntityResponseType> {
    return this.http.post<ICreatifs>(this.resourceUrl, creatifs, { observe: 'response' });
  }

  update(creatifs: ICreatifs): Observable<EntityResponseType> {
    return this.http.put<ICreatifs>(`${this.resourceUrl}/${getCreatifsIdentifier(creatifs) as number}`, creatifs, { observe: 'response' });
  }

  partialUpdate(creatifs: ICreatifs): Observable<EntityResponseType> {
    return this.http.patch<ICreatifs>(`${this.resourceUrl}/${getCreatifsIdentifier(creatifs) as number}`, creatifs, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICreatifs>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICreatifs[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addCreatifsToCollectionIfMissing(creatifsCollection: ICreatifs[], ...creatifsToCheck: (ICreatifs | null | undefined)[]): ICreatifs[] {
    const creatifs: ICreatifs[] = creatifsToCheck.filter(isPresent);
    if (creatifs.length > 0) {
      const creatifsCollectionIdentifiers = creatifsCollection.map(creatifsItem => getCreatifsIdentifier(creatifsItem)!);
      const creatifsToAdd = creatifs.filter(creatifsItem => {
        const creatifsIdentifier = getCreatifsIdentifier(creatifsItem);
        if (creatifsIdentifier == null || creatifsCollectionIdentifiers.includes(creatifsIdentifier)) {
          return false;
        }
        creatifsCollectionIdentifiers.push(creatifsIdentifier);
        return true;
      });
      return [...creatifsToAdd, ...creatifsCollection];
    }
    return creatifsCollection;
  }
}
