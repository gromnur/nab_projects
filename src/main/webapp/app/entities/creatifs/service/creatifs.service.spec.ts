import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ICreatifs, Creatifs } from '../creatifs.model';

import { CreatifsService } from './creatifs.service';

describe('Creatifs Service', () => {
  let service: CreatifsService;
  let httpMock: HttpTestingController;
  let elemDefault: ICreatifs;
  let expectedResult: ICreatifs | ICreatifs[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(CreatifsService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      nom: 'AAAAAAA',
      mail: 'AAAAAAA',
      adresseSiegeSocial: 'AAAAAAA',
      telephonne: 'AAAAAAA',
      instagram: 'AAAAAAA',
      prenom: 'AAAAAAA',
      microEntreprise: false,
      dateRecrutement: 'AAAAAAA',
      siret: 'AAAAAAA',
      siren: 'AAAAAAA',
      ville: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Creatifs', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new Creatifs()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Creatifs', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nom: 'BBBBBB',
          mail: 'BBBBBB',
          adresseSiegeSocial: 'BBBBBB',
          telephonne: 'BBBBBB',
          instagram: 'BBBBBB',
          prenom: 'BBBBBB',
          microEntreprise: true,
          dateRecrutement: 'BBBBBB',
          siret: 'BBBBBB',
          siren: 'BBBBBB',
          ville: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Creatifs', () => {
      const patchObject = Object.assign(
        {
          mail: 'BBBBBB',
          prenom: 'BBBBBB',
          dateRecrutement: 'BBBBBB',
          siren: 'BBBBBB',
        },
        new Creatifs()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Creatifs', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nom: 'BBBBBB',
          mail: 'BBBBBB',
          adresseSiegeSocial: 'BBBBBB',
          telephonne: 'BBBBBB',
          instagram: 'BBBBBB',
          prenom: 'BBBBBB',
          microEntreprise: true,
          dateRecrutement: 'BBBBBB',
          siret: 'BBBBBB',
          siren: 'BBBBBB',
          ville: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Creatifs', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addCreatifsToCollectionIfMissing', () => {
      it('should add a Creatifs to an empty array', () => {
        const creatifs: ICreatifs = { id: 123 };
        expectedResult = service.addCreatifsToCollectionIfMissing([], creatifs);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(creatifs);
      });

      it('should not add a Creatifs to an array that contains it', () => {
        const creatifs: ICreatifs = { id: 123 };
        const creatifsCollection: ICreatifs[] = [
          {
            ...creatifs,
          },
          { id: 456 },
        ];
        expectedResult = service.addCreatifsToCollectionIfMissing(creatifsCollection, creatifs);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Creatifs to an array that doesn't contain it", () => {
        const creatifs: ICreatifs = { id: 123 };
        const creatifsCollection: ICreatifs[] = [{ id: 456 }];
        expectedResult = service.addCreatifsToCollectionIfMissing(creatifsCollection, creatifs);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(creatifs);
      });

      it('should add only unique Creatifs to an array', () => {
        const creatifsArray: ICreatifs[] = [{ id: 123 }, { id: 456 }, { id: 91829 }];
        const creatifsCollection: ICreatifs[] = [{ id: 123 }];
        expectedResult = service.addCreatifsToCollectionIfMissing(creatifsCollection, ...creatifsArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const creatifs: ICreatifs = { id: 123 };
        const creatifs2: ICreatifs = { id: 456 };
        expectedResult = service.addCreatifsToCollectionIfMissing([], creatifs, creatifs2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(creatifs);
        expect(expectedResult).toContain(creatifs2);
      });

      it('should accept null and undefined values', () => {
        const creatifs: ICreatifs = { id: 123 };
        expectedResult = service.addCreatifsToCollectionIfMissing([], null, creatifs, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(creatifs);
      });

      it('should return initial array if no Creatifs is added', () => {
        const creatifsCollection: ICreatifs[] = [{ id: 123 }];
        expectedResult = service.addCreatifsToCollectionIfMissing(creatifsCollection, undefined, null);
        expect(expectedResult).toEqual(creatifsCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
