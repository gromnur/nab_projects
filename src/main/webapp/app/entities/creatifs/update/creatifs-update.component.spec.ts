jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { CreatifsService } from '../service/creatifs.service';
import { ICreatifs, Creatifs } from '../creatifs.model';
import { ICompetances } from 'app/entities/competances/competances.model';
import { CompetancesService } from 'app/entities/competances/service/competances.service';
import { IStatusCreatifsHistorique } from 'app/entities/status-creatifs-historique/status-creatifs-historique.model';
import { StatusCreatifsHistoriqueService } from 'app/entities/status-creatifs-historique/service/status-creatifs-historique.service';
import { IStatusValidationsHistorique } from 'app/entities/status-validations-historique/status-validations-historique.model';
import { StatusValidationsHistoriqueService } from 'app/entities/status-validations-historique/service/status-validations-historique.service';

import { CreatifsUpdateComponent } from './creatifs-update.component';

describe('Creatifs Management Update Component', () => {
  let comp: CreatifsUpdateComponent;
  let fixture: ComponentFixture<CreatifsUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let creatifsService: CreatifsService;
  let competancesService: CompetancesService;
  let statusCreatifsHistoriqueService: StatusCreatifsHistoriqueService;
  let statusValidationsHistoriqueService: StatusValidationsHistoriqueService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [CreatifsUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(CreatifsUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CreatifsUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    creatifsService = TestBed.inject(CreatifsService);
    competancesService = TestBed.inject(CompetancesService);
    statusCreatifsHistoriqueService = TestBed.inject(StatusCreatifsHistoriqueService);
    statusValidationsHistoriqueService = TestBed.inject(StatusValidationsHistoriqueService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Competances query and add missing value', () => {
      const creatifs: ICreatifs = { id: 456 };
      const competancePrincipal: ICompetances = { id: 5422 };
      creatifs.competancePrincipal = competancePrincipal;

      const competancesCollection: ICompetances[] = [{ id: 46380 }];
      jest.spyOn(competancesService, 'query').mockReturnValue(of(new HttpResponse({ body: competancesCollection })));
      const additionalCompetances = [competancePrincipal];
      const expectedCollection: ICompetances[] = [...additionalCompetances, ...competancesCollection];
      jest.spyOn(competancesService, 'addCompetancesToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ creatifs });
      comp.ngOnInit();

      expect(competancesService.query).toHaveBeenCalled();
      expect(competancesService.addCompetancesToCollectionIfMissing).toHaveBeenCalledWith(competancesCollection, ...additionalCompetances);
      expect(comp.competancesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call StatusCreatifsHistorique query and add missing value', () => {
      const creatifs: ICreatifs = { id: 456 };
      const statusCreatifsHistorique: IStatusCreatifsHistorique = { id: 5050 };
      creatifs.statusCreatifsHistorique = statusCreatifsHistorique;

      const statusCreatifsHistoriqueCollection: IStatusCreatifsHistorique[] = [{ id: 76685 }];
      jest
        .spyOn(statusCreatifsHistoriqueService, 'query')
        .mockReturnValue(of(new HttpResponse({ body: statusCreatifsHistoriqueCollection })));
      const additionalStatusCreatifsHistoriques = [statusCreatifsHistorique];
      const expectedCollection: IStatusCreatifsHistorique[] = [
        ...additionalStatusCreatifsHistoriques,
        ...statusCreatifsHistoriqueCollection,
      ];
      jest.spyOn(statusCreatifsHistoriqueService, 'addStatusCreatifsHistoriqueToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ creatifs });
      comp.ngOnInit();

      expect(statusCreatifsHistoriqueService.query).toHaveBeenCalled();
      expect(statusCreatifsHistoriqueService.addStatusCreatifsHistoriqueToCollectionIfMissing).toHaveBeenCalledWith(
        statusCreatifsHistoriqueCollection,
        ...additionalStatusCreatifsHistoriques
      );
      expect(comp.statusCreatifsHistoriquesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call StatusValidationsHistorique query and add missing value', () => {
      const creatifs: ICreatifs = { id: 456 };
      const statusValidationsHistorique: IStatusValidationsHistorique = { id: 43609 };
      creatifs.statusValidationsHistorique = statusValidationsHistorique;

      const statusValidationsHistoriqueCollection: IStatusValidationsHistorique[] = [{ id: 61014 }];
      jest
        .spyOn(statusValidationsHistoriqueService, 'query')
        .mockReturnValue(of(new HttpResponse({ body: statusValidationsHistoriqueCollection })));
      const additionalStatusValidationsHistoriques = [statusValidationsHistorique];
      const expectedCollection: IStatusValidationsHistorique[] = [
        ...additionalStatusValidationsHistoriques,
        ...statusValidationsHistoriqueCollection,
      ];
      jest
        .spyOn(statusValidationsHistoriqueService, 'addStatusValidationsHistoriqueToCollectionIfMissing')
        .mockReturnValue(expectedCollection);

      activatedRoute.data = of({ creatifs });
      comp.ngOnInit();

      expect(statusValidationsHistoriqueService.query).toHaveBeenCalled();
      expect(statusValidationsHistoriqueService.addStatusValidationsHistoriqueToCollectionIfMissing).toHaveBeenCalledWith(
        statusValidationsHistoriqueCollection,
        ...additionalStatusValidationsHistoriques
      );
      expect(comp.statusValidationsHistoriquesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const creatifs: ICreatifs = { id: 456 };
      const competancePrincipal: ICompetances = { id: 10200 };
      creatifs.competancePrincipal = competancePrincipal;
      const statusCreatifsHistorique: IStatusCreatifsHistorique = { id: 62160 };
      creatifs.statusCreatifsHistorique = statusCreatifsHistorique;
      const statusValidationsHistorique: IStatusValidationsHistorique = { id: 73989 };
      creatifs.statusValidationsHistorique = statusValidationsHistorique;

      activatedRoute.data = of({ creatifs });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(creatifs));
      expect(comp.competancesSharedCollection).toContain(competancePrincipal);
      expect(comp.statusCreatifsHistoriquesSharedCollection).toContain(statusCreatifsHistorique);
      expect(comp.statusValidationsHistoriquesSharedCollection).toContain(statusValidationsHistorique);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Creatifs>>();
      const creatifs = { id: 123 };
      jest.spyOn(creatifsService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ creatifs });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: creatifs }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(creatifsService.update).toHaveBeenCalledWith(creatifs);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Creatifs>>();
      const creatifs = new Creatifs();
      jest.spyOn(creatifsService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ creatifs });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: creatifs }));
      saveSubject.complete();

      // THEN
      expect(creatifsService.create).toHaveBeenCalledWith(creatifs);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Creatifs>>();
      const creatifs = { id: 123 };
      jest.spyOn(creatifsService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ creatifs });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(creatifsService.update).toHaveBeenCalledWith(creatifs);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackCompetancesById', () => {
      it('Should return tracked Competances primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCompetancesById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackStatusCreatifsHistoriqueById', () => {
      it('Should return tracked StatusCreatifsHistorique primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackStatusCreatifsHistoriqueById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackStatusValidationsHistoriqueById', () => {
      it('Should return tracked StatusValidationsHistorique primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackStatusValidationsHistoriqueById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
