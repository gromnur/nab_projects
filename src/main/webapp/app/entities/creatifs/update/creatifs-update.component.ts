import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ICreatifs, Creatifs } from '../creatifs.model';
import { CreatifsService } from '../service/creatifs.service';
import { ICompetances } from 'app/entities/competances/competances.model';
import { CompetancesService } from 'app/entities/competances/service/competances.service';
import { IStatusCreatifsHistorique } from 'app/entities/status-creatifs-historique/status-creatifs-historique.model';
import { StatusCreatifsHistoriqueService } from 'app/entities/status-creatifs-historique/service/status-creatifs-historique.service';
import { IStatusValidationsHistorique } from 'app/entities/status-validations-historique/status-validations-historique.model';
import { StatusValidationsHistoriqueService } from 'app/entities/status-validations-historique/service/status-validations-historique.service';

@Component({
  selector: 'jhi-creatifs-update',
  templateUrl: './creatifs-update.component.html',
})
export class CreatifsUpdateComponent implements OnInit {
  isSaving = false;

  competancesSharedCollection: ICompetances[] = [];
  statusCreatifsHistoriquesSharedCollection: IStatusCreatifsHistorique[] = [];
  statusValidationsHistoriquesSharedCollection: IStatusValidationsHistorique[] = [];

  editForm = this.fb.group({
    id: [],
    nom: [null, [Validators.maxLength(50)]],
    mail: [null, [Validators.maxLength(50)]],
    adresseSiegeSocial: [null, [Validators.maxLength(50)]],
    telephonne: [null, [Validators.maxLength(10)]],
    instagram: [null, [Validators.maxLength(50)]],
    prenom: [null, [Validators.maxLength(50)]],
    microEntreprise: [],
    dateRecrutement: [null, [Validators.maxLength(50)]],
    siret: [null, [Validators.maxLength(50)]],
    siren: [null, [Validators.maxLength(50)]],
    ville: [null, [Validators.maxLength(50)]],
    competancePrincipal: [],
    statusCreatifsHistorique: [],
    statusValidationsHistorique: [],
  });

  constructor(
    protected creatifsService: CreatifsService,
    protected competancesService: CompetancesService,
    protected statusCreatifsHistoriqueService: StatusCreatifsHistoriqueService,
    protected statusValidationsHistoriqueService: StatusValidationsHistoriqueService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ creatifs }) => {
      this.updateForm(creatifs);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const creatifs = this.createFromForm();
    if (creatifs.id !== undefined) {
      this.subscribeToSaveResponse(this.creatifsService.update(creatifs));
    } else {
      this.subscribeToSaveResponse(this.creatifsService.create(creatifs));
    }
  }

  trackCompetancesById(index: number, item: ICompetances): number {
    return item.id!;
  }

  trackStatusCreatifsHistoriqueById(index: number, item: IStatusCreatifsHistorique): number {
    return item.id!;
  }

  trackStatusValidationsHistoriqueById(index: number, item: IStatusValidationsHistorique): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICreatifs>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(creatifs: ICreatifs): void {
    this.editForm.patchValue({
      id: creatifs.id,
      nom: creatifs.nom,
      mail: creatifs.mail,
      adresseSiegeSocial: creatifs.adresseSiegeSocial,
      telephonne: creatifs.telephonne,
      instagram: creatifs.instagram,
      prenom: creatifs.prenom,
      microEntreprise: creatifs.microEntreprise,
      dateRecrutement: creatifs.dateRecrutement,
      siret: creatifs.siret,
      siren: creatifs.siren,
      ville: creatifs.ville,
      competancePrincipal: creatifs.competancePrincipal,
      statusCreatifsHistorique: creatifs.statusCreatifsHistorique,
      statusValidationsHistorique: creatifs.statusValidationsHistorique,
    });

    this.competancesSharedCollection = this.competancesService.addCompetancesToCollectionIfMissing(
      this.competancesSharedCollection,
      creatifs.competancePrincipal
    );
    this.statusCreatifsHistoriquesSharedCollection = this.statusCreatifsHistoriqueService.addStatusCreatifsHistoriqueToCollectionIfMissing(
      this.statusCreatifsHistoriquesSharedCollection,
      creatifs.statusCreatifsHistorique
    );
    this.statusValidationsHistoriquesSharedCollection =
      this.statusValidationsHistoriqueService.addStatusValidationsHistoriqueToCollectionIfMissing(
        this.statusValidationsHistoriquesSharedCollection,
        creatifs.statusValidationsHistorique
      );
  }

  protected loadRelationshipsOptions(): void {
    this.competancesService
      .query()
      .pipe(map((res: HttpResponse<ICompetances[]>) => res.body ?? []))
      .pipe(
        map((competances: ICompetances[]) =>
          this.competancesService.addCompetancesToCollectionIfMissing(competances, this.editForm.get('competancePrincipal')!.value)
        )
      )
      .subscribe((competances: ICompetances[]) => (this.competancesSharedCollection = competances));

    this.statusCreatifsHistoriqueService
      .query()
      .pipe(map((res: HttpResponse<IStatusCreatifsHistorique[]>) => res.body ?? []))
      .pipe(
        map((statusCreatifsHistoriques: IStatusCreatifsHistorique[]) =>
          this.statusCreatifsHistoriqueService.addStatusCreatifsHistoriqueToCollectionIfMissing(
            statusCreatifsHistoriques,
            this.editForm.get('statusCreatifsHistorique')!.value
          )
        )
      )
      .subscribe(
        (statusCreatifsHistoriques: IStatusCreatifsHistorique[]) =>
          (this.statusCreatifsHistoriquesSharedCollection = statusCreatifsHistoriques)
      );

    this.statusValidationsHistoriqueService
      .query()
      .pipe(map((res: HttpResponse<IStatusValidationsHistorique[]>) => res.body ?? []))
      .pipe(
        map((statusValidationsHistoriques: IStatusValidationsHistorique[]) =>
          this.statusValidationsHistoriqueService.addStatusValidationsHistoriqueToCollectionIfMissing(
            statusValidationsHistoriques,
            this.editForm.get('statusValidationsHistorique')!.value
          )
        )
      )
      .subscribe(
        (statusValidationsHistoriques: IStatusValidationsHistorique[]) =>
          (this.statusValidationsHistoriquesSharedCollection = statusValidationsHistoriques)
      );
  }

  protected createFromForm(): ICreatifs {
    return {
      ...new Creatifs(),
      id: this.editForm.get(['id'])!.value,
      nom: this.editForm.get(['nom'])!.value,
      mail: this.editForm.get(['mail'])!.value,
      adresseSiegeSocial: this.editForm.get(['adresseSiegeSocial'])!.value,
      telephonne: this.editForm.get(['telephonne'])!.value,
      instagram: this.editForm.get(['instagram'])!.value,
      prenom: this.editForm.get(['prenom'])!.value,
      microEntreprise: this.editForm.get(['microEntreprise'])!.value,
      dateRecrutement: this.editForm.get(['dateRecrutement'])!.value,
      siret: this.editForm.get(['siret'])!.value,
      siren: this.editForm.get(['siren'])!.value,
      ville: this.editForm.get(['ville'])!.value,
      competancePrincipal: this.editForm.get(['competancePrincipal'])!.value,
      statusCreatifsHistorique: this.editForm.get(['statusCreatifsHistorique'])!.value,
      statusValidationsHistorique: this.editForm.get(['statusValidationsHistorique'])!.value,
    };
  }
}
