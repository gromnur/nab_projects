import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ICreatifs, Creatifs } from '../creatifs.model';
import { CreatifsService } from '../service/creatifs.service';

@Injectable({ providedIn: 'root' })
export class CreatifsRoutingResolveService implements Resolve<ICreatifs> {
  constructor(protected service: CreatifsService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICreatifs> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((creatifs: HttpResponse<Creatifs>) => {
          if (creatifs.body) {
            return of(creatifs.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Creatifs());
  }
}
