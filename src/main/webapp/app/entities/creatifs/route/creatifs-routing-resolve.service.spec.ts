jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { ICreatifs, Creatifs } from '../creatifs.model';
import { CreatifsService } from '../service/creatifs.service';

import { CreatifsRoutingResolveService } from './creatifs-routing-resolve.service';

describe('Creatifs routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: CreatifsRoutingResolveService;
  let service: CreatifsService;
  let resultCreatifs: ICreatifs | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(CreatifsRoutingResolveService);
    service = TestBed.inject(CreatifsService);
    resultCreatifs = undefined;
  });

  describe('resolve', () => {
    it('should return ICreatifs returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultCreatifs = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultCreatifs).toEqual({ id: 123 });
    });

    it('should return new ICreatifs if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultCreatifs = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultCreatifs).toEqual(new Creatifs());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as Creatifs })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultCreatifs = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultCreatifs).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
