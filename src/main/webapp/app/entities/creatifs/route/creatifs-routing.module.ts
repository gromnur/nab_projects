import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { CreatifsComponent } from '../list/creatifs.component';
import { CreatifsDetailComponent } from '../detail/creatifs-detail.component';
import { CreatifsUpdateComponent } from '../update/creatifs-update.component';
import { CreatifsRoutingResolveService } from './creatifs-routing-resolve.service';

const creatifsRoute: Routes = [
  {
    path: '',
    component: CreatifsComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CreatifsDetailComponent,
    resolve: {
      creatifs: CreatifsRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CreatifsUpdateComponent,
    resolve: {
      creatifs: CreatifsRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CreatifsUpdateComponent,
    resolve: {
      creatifs: CreatifsRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(creatifsRoute)],
  exports: [RouterModule],
})
export class CreatifsRoutingModule {}
