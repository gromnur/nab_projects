import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CreatifsDetailComponent } from './creatifs-detail.component';

describe('Creatifs Management Detail Component', () => {
  let comp: CreatifsDetailComponent;
  let fixture: ComponentFixture<CreatifsDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CreatifsDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ creatifs: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(CreatifsDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(CreatifsDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load creatifs on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.creatifs).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
