import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICreatifs } from '../creatifs.model';

@Component({
  selector: 'jhi-creatifs-detail',
  templateUrl: './creatifs-detail.component.html',
})
export class CreatifsDetailComponent implements OnInit {
  creatifs: ICreatifs | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ creatifs }) => {
      this.creatifs = creatifs;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
