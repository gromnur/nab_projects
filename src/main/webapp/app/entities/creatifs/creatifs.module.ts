import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { CreatifsComponent } from './list/creatifs.component';
import { CreatifsDetailComponent } from './detail/creatifs-detail.component';
import { CreatifsUpdateComponent } from './update/creatifs-update.component';
import { CreatifsDeleteDialogComponent } from './delete/creatifs-delete-dialog.component';
import { CreatifsRoutingModule } from './route/creatifs-routing.module';

@NgModule({
  imports: [SharedModule, CreatifsRoutingModule],
  declarations: [CreatifsComponent, CreatifsDetailComponent, CreatifsUpdateComponent, CreatifsDeleteDialogComponent],
  entryComponents: [CreatifsDeleteDialogComponent],
})
export class CreatifsModule {}
