import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { CreatifsService } from '../service/creatifs.service';

import { CreatifsComponent } from './creatifs.component';

describe('Creatifs Management Component', () => {
  let comp: CreatifsComponent;
  let fixture: ComponentFixture<CreatifsComponent>;
  let service: CreatifsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [CreatifsComponent],
    })
      .overrideTemplate(CreatifsComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CreatifsComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(CreatifsService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.creatifs?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
