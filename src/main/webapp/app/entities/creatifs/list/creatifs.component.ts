import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICreatifs } from '../creatifs.model';
import { CreatifsService } from '../service/creatifs.service';
import { CreatifsDeleteDialogComponent } from '../delete/creatifs-delete-dialog.component';

@Component({
  selector: 'jhi-creatifs',
  templateUrl: './creatifs.component.html',
})
export class CreatifsComponent implements OnInit {
  creatifs?: ICreatifs[];
  isLoading = false;

  constructor(protected creatifsService: CreatifsService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.creatifsService.query().subscribe(
      (res: HttpResponse<ICreatifs[]>) => {
        this.isLoading = false;
        this.creatifs = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: ICreatifs): number {
    return item.id!;
  }

  delete(creatifs: ICreatifs): void {
    const modalRef = this.modalService.open(CreatifsDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.creatifs = creatifs;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
