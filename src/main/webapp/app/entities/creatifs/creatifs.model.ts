import { IMissions } from 'app/entities/missions/missions.model';
import { ICompetances } from 'app/entities/competances/competances.model';
import { IStatusCreatifsHistorique } from 'app/entities/status-creatifs-historique/status-creatifs-historique.model';
import { IStatusValidationsHistorique } from 'app/entities/status-validations-historique/status-validations-historique.model';

export interface ICreatifs {
  id?: number;
  nom?: string | null;
  mail?: string | null;
  adresseSiegeSocial?: string | null;
  telephonne?: string | null;
  instagram?: string | null;
  prenom?: string | null;
  microEntreprise?: boolean | null;
  dateRecrutement?: string | null;
  siret?: string | null;
  siren?: string | null;
  ville?: string | null;
  missions?: IMissions[] | null;
  competancePrincipal?: ICompetances | null;
  statusCreatifsHistorique?: IStatusCreatifsHistorique | null;
  statusValidationsHistorique?: IStatusValidationsHistorique | null;
  competances?: ICompetances[] | null;
}

export class Creatifs implements ICreatifs {
  constructor(
    public id?: number,
    public nom?: string | null,
    public mail?: string | null,
    public adresseSiegeSocial?: string | null,
    public telephonne?: string | null,
    public instagram?: string | null,
    public prenom?: string | null,
    public microEntreprise?: boolean | null,
    public dateRecrutement?: string | null,
    public siret?: string | null,
    public siren?: string | null,
    public ville?: string | null,
    public missions?: IMissions[] | null,
    public competancePrincipal?: ICompetances | null,
    public statusCreatifsHistorique?: IStatusCreatifsHistorique | null,
    public statusValidationsHistorique?: IStatusValidationsHistorique | null,
    public competances?: ICompetances[] | null
  ) {
    this.microEntreprise = this.microEntreprise ?? false;
  }
}

export function getCreatifsIdentifier(creatifs: ICreatifs): number | undefined {
  return creatifs.id;
}
