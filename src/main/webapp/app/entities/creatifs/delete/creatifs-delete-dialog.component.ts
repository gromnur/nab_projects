import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ICreatifs } from '../creatifs.model';
import { CreatifsService } from '../service/creatifs.service';

@Component({
  templateUrl: './creatifs-delete-dialog.component.html',
})
export class CreatifsDeleteDialogComponent {
  creatifs?: ICreatifs;

  constructor(protected creatifsService: CreatifsService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.creatifsService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
