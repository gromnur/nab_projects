package com.nabcommunity.web.rest;

import com.nabcommunity.domain.StatusValidationsHistorique;
import com.nabcommunity.repository.StatusValidationsHistoriqueRepository;
import com.nabcommunity.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.nabcommunity.domain.StatusValidationsHistorique}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class StatusValidationsHistoriqueResource {

    private final Logger log = LoggerFactory.getLogger(StatusValidationsHistoriqueResource.class);

    private static final String ENTITY_NAME = "statusValidationsHistorique";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final StatusValidationsHistoriqueRepository statusValidationsHistoriqueRepository;

    public StatusValidationsHistoriqueResource(StatusValidationsHistoriqueRepository statusValidationsHistoriqueRepository) {
        this.statusValidationsHistoriqueRepository = statusValidationsHistoriqueRepository;
    }

    /**
     * {@code POST  /status-validations-historiques} : Create a new statusValidationsHistorique.
     *
     * @param statusValidationsHistorique the statusValidationsHistorique to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new statusValidationsHistorique, or with status {@code 400 (Bad Request)} if the statusValidationsHistorique has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/status-validations-historiques")
    public ResponseEntity<StatusValidationsHistorique> createStatusValidationsHistorique(
        @RequestBody StatusValidationsHistorique statusValidationsHistorique
    ) throws URISyntaxException {
        log.debug("REST request to save StatusValidationsHistorique : {}", statusValidationsHistorique);
        if (statusValidationsHistorique.getId() != null) {
            throw new BadRequestAlertException("A new statusValidationsHistorique cannot already have an ID", ENTITY_NAME, "idexists");
        }
        StatusValidationsHistorique result = statusValidationsHistoriqueRepository.save(statusValidationsHistorique);
        return ResponseEntity
            .created(new URI("/api/status-validations-historiques/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /status-validations-historiques/:id} : Updates an existing statusValidationsHistorique.
     *
     * @param id the id of the statusValidationsHistorique to save.
     * @param statusValidationsHistorique the statusValidationsHistorique to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated statusValidationsHistorique,
     * or with status {@code 400 (Bad Request)} if the statusValidationsHistorique is not valid,
     * or with status {@code 500 (Internal Server Error)} if the statusValidationsHistorique couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/status-validations-historiques/{id}")
    public ResponseEntity<StatusValidationsHistorique> updateStatusValidationsHistorique(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody StatusValidationsHistorique statusValidationsHistorique
    ) throws URISyntaxException {
        log.debug("REST request to update StatusValidationsHistorique : {}, {}", id, statusValidationsHistorique);
        if (statusValidationsHistorique.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, statusValidationsHistorique.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!statusValidationsHistoriqueRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        StatusValidationsHistorique result = statusValidationsHistoriqueRepository.save(statusValidationsHistorique);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, statusValidationsHistorique.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /status-validations-historiques/:id} : Partial updates given fields of an existing statusValidationsHistorique, field will ignore if it is null
     *
     * @param id the id of the statusValidationsHistorique to save.
     * @param statusValidationsHistorique the statusValidationsHistorique to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated statusValidationsHistorique,
     * or with status {@code 400 (Bad Request)} if the statusValidationsHistorique is not valid,
     * or with status {@code 404 (Not Found)} if the statusValidationsHistorique is not found,
     * or with status {@code 500 (Internal Server Error)} if the statusValidationsHistorique couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/status-validations-historiques/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<StatusValidationsHistorique> partialUpdateStatusValidationsHistorique(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody StatusValidationsHistorique statusValidationsHistorique
    ) throws URISyntaxException {
        log.debug("REST request to partial update StatusValidationsHistorique partially : {}, {}", id, statusValidationsHistorique);
        if (statusValidationsHistorique.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, statusValidationsHistorique.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!statusValidationsHistoriqueRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<StatusValidationsHistorique> result = statusValidationsHistoriqueRepository
            .findById(statusValidationsHistorique.getId())
            .map(existingStatusValidationsHistorique -> {
                if (statusValidationsHistorique.getDateModifiacation() != null) {
                    existingStatusValidationsHistorique.setDateModifiacation(statusValidationsHistorique.getDateModifiacation());
                }
                if (statusValidationsHistorique.getStatus() != null) {
                    existingStatusValidationsHistorique.setStatus(statusValidationsHistorique.getStatus());
                }

                return existingStatusValidationsHistorique;
            })
            .map(statusValidationsHistoriqueRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, statusValidationsHistorique.getId().toString())
        );
    }

    /**
     * {@code GET  /status-validations-historiques} : get all the statusValidationsHistoriques.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of statusValidationsHistoriques in body.
     */
    @GetMapping("/status-validations-historiques")
    public List<StatusValidationsHistorique> getAllStatusValidationsHistoriques() {
        log.debug("REST request to get all StatusValidationsHistoriques");
        return statusValidationsHistoriqueRepository.findAll();
    }

    /**
     * {@code GET  /status-validations-historiques/:id} : get the "id" statusValidationsHistorique.
     *
     * @param id the id of the statusValidationsHistorique to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the statusValidationsHistorique, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/status-validations-historiques/{id}")
    public ResponseEntity<StatusValidationsHistorique> getStatusValidationsHistorique(@PathVariable Long id) {
        log.debug("REST request to get StatusValidationsHistorique : {}", id);
        Optional<StatusValidationsHistorique> statusValidationsHistorique = statusValidationsHistoriqueRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(statusValidationsHistorique);
    }

    /**
     * {@code DELETE  /status-validations-historiques/:id} : delete the "id" statusValidationsHistorique.
     *
     * @param id the id of the statusValidationsHistorique to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/status-validations-historiques/{id}")
    public ResponseEntity<Void> deleteStatusValidationsHistorique(@PathVariable Long id) {
        log.debug("REST request to delete StatusValidationsHistorique : {}", id);
        statusValidationsHistoriqueRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
