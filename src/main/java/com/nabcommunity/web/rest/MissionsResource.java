package com.nabcommunity.web.rest;

import com.nabcommunity.domain.Missions;
import com.nabcommunity.repository.MissionsRepository;
import com.nabcommunity.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.nabcommunity.domain.Missions}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class MissionsResource {

    private final Logger log = LoggerFactory.getLogger(MissionsResource.class);

    private static final String ENTITY_NAME = "missions";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MissionsRepository missionsRepository;

    public MissionsResource(MissionsRepository missionsRepository) {
        this.missionsRepository = missionsRepository;
    }

    /**
     * {@code POST  /missions} : Create a new missions.
     *
     * @param missions the missions to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new missions, or with status {@code 400 (Bad Request)} if the missions has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/missions")
    public ResponseEntity<Missions> createMissions(@Valid @RequestBody Missions missions) throws URISyntaxException {
        log.debug("REST request to save Missions : {}", missions);
        if (missions.getId() != null) {
            throw new BadRequestAlertException("A new missions cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Missions result = missionsRepository.save(missions);
        return ResponseEntity
            .created(new URI("/api/missions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /missions/:id} : Updates an existing missions.
     *
     * @param id the id of the missions to save.
     * @param missions the missions to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated missions,
     * or with status {@code 400 (Bad Request)} if the missions is not valid,
     * or with status {@code 500 (Internal Server Error)} if the missions couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/missions/{id}")
    public ResponseEntity<Missions> updateMissions(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody Missions missions
    ) throws URISyntaxException {
        log.debug("REST request to update Missions : {}, {}", id, missions);
        if (missions.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, missions.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!missionsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Missions result = missionsRepository.save(missions);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, missions.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /missions/:id} : Partial updates given fields of an existing missions, field will ignore if it is null
     *
     * @param id the id of the missions to save.
     * @param missions the missions to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated missions,
     * or with status {@code 400 (Bad Request)} if the missions is not valid,
     * or with status {@code 404 (Not Found)} if the missions is not found,
     * or with status {@code 500 (Internal Server Error)} if the missions couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/missions/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Missions> partialUpdateMissions(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody Missions missions
    ) throws URISyntaxException {
        log.debug("REST request to partial update Missions partially : {}, {}", id, missions);
        if (missions.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, missions.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!missionsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Missions> result = missionsRepository
            .findById(missions.getId())
            .map(existingMissions -> {
                if (missions.getNom() != null) {
                    existingMissions.setNom(missions.getNom());
                }
                if (missions.getLieu() != null) {
                    existingMissions.setLieu(missions.getLieu());
                }
                if (missions.getPrix() != null) {
                    existingMissions.setPrix(missions.getPrix());
                }
                if (missions.getDateDebutMission() != null) {
                    existingMissions.setDateDebutMission(missions.getDateDebutMission());
                }
                if (missions.getDateFinMission() != null) {
                    existingMissions.setDateFinMission(missions.getDateFinMission());
                }
                if (missions.getDateCreationMission() != null) {
                    existingMissions.setDateCreationMission(missions.getDateCreationMission());
                }

                return existingMissions;
            })
            .map(missionsRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, missions.getId().toString())
        );
    }

    /**
     * {@code GET  /missions} : get all the missions.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of missions in body.
     */
    @GetMapping("/missions")
    public List<Missions> getAllMissions() {
        log.debug("REST request to get all Missions");
        return missionsRepository.findAll();
    }

    /**
     * {@code GET  /missions/:id} : get the "id" missions.
     *
     * @param id the id of the missions to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the missions, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/missions/{id}")
    public ResponseEntity<Missions> getMissions(@PathVariable Long id) {
        log.debug("REST request to get Missions : {}", id);
        Optional<Missions> missions = missionsRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(missions);
    }

    /**
     * {@code DELETE  /missions/:id} : delete the "id" missions.
     *
     * @param id the id of the missions to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/missions/{id}")
    public ResponseEntity<Void> deleteMissions(@PathVariable Long id) {
        log.debug("REST request to delete Missions : {}", id);
        missionsRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
