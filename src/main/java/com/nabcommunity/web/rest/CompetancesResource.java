package com.nabcommunity.web.rest;

import com.nabcommunity.domain.Competances;
import com.nabcommunity.repository.CompetancesRepository;
import com.nabcommunity.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.nabcommunity.domain.Competances}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class CompetancesResource {

    private final Logger log = LoggerFactory.getLogger(CompetancesResource.class);

    private static final String ENTITY_NAME = "competances";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CompetancesRepository competancesRepository;

    public CompetancesResource(CompetancesRepository competancesRepository) {
        this.competancesRepository = competancesRepository;
    }

    /**
     * {@code POST  /competances} : Create a new competances.
     *
     * @param competances the competances to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new competances, or with status {@code 400 (Bad Request)} if the competances has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/competances")
    public ResponseEntity<Competances> createCompetances(@Valid @RequestBody Competances competances) throws URISyntaxException {
        log.debug("REST request to save Competances : {}", competances);
        if (competances.getId() != null) {
            throw new BadRequestAlertException("A new competances cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Competances result = competancesRepository.save(competances);
        return ResponseEntity
            .created(new URI("/api/competances/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /competances/:id} : Updates an existing competances.
     *
     * @param id the id of the competances to save.
     * @param competances the competances to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated competances,
     * or with status {@code 400 (Bad Request)} if the competances is not valid,
     * or with status {@code 500 (Internal Server Error)} if the competances couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/competances/{id}")
    public ResponseEntity<Competances> updateCompetances(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody Competances competances
    ) throws URISyntaxException {
        log.debug("REST request to update Competances : {}, {}", id, competances);
        if (competances.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, competances.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!competancesRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Competances result = competancesRepository.save(competances);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, competances.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /competances/:id} : Partial updates given fields of an existing competances, field will ignore if it is null
     *
     * @param id the id of the competances to save.
     * @param competances the competances to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated competances,
     * or with status {@code 400 (Bad Request)} if the competances is not valid,
     * or with status {@code 404 (Not Found)} if the competances is not found,
     * or with status {@code 500 (Internal Server Error)} if the competances couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/competances/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Competances> partialUpdateCompetances(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody Competances competances
    ) throws URISyntaxException {
        log.debug("REST request to partial update Competances partially : {}, {}", id, competances);
        if (competances.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, competances.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!competancesRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Competances> result = competancesRepository
            .findById(competances.getId())
            .map(existingCompetances -> {
                if (competances.getNom() != null) {
                    existingCompetances.setNom(competances.getNom());
                }
                if (competances.getDescription() != null) {
                    existingCompetances.setDescription(competances.getDescription());
                }

                return existingCompetances;
            })
            .map(competancesRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, competances.getId().toString())
        );
    }

    /**
     * {@code GET  /competances} : get all the competances.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of competances in body.
     */
    @GetMapping("/competances")
    public List<Competances> getAllCompetances(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all Competances");
        return competancesRepository.findAllWithEagerRelationships();
    }

    /**
     * {@code GET  /competances/:id} : get the "id" competances.
     *
     * @param id the id of the competances to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the competances, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/competances/{id}")
    public ResponseEntity<Competances> getCompetances(@PathVariable Long id) {
        log.debug("REST request to get Competances : {}", id);
        Optional<Competances> competances = competancesRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(competances);
    }

    /**
     * {@code DELETE  /competances/:id} : delete the "id" competances.
     *
     * @param id the id of the competances to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/competances/{id}")
    public ResponseEntity<Void> deleteCompetances(@PathVariable Long id) {
        log.debug("REST request to delete Competances : {}", id);
        competancesRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
