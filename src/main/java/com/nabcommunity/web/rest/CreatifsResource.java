package com.nabcommunity.web.rest;

import com.nabcommunity.domain.Creatifs;
import com.nabcommunity.repository.CreatifsRepository;
import com.nabcommunity.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.nabcommunity.domain.Creatifs}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class CreatifsResource {

    private final Logger log = LoggerFactory.getLogger(CreatifsResource.class);

    private static final String ENTITY_NAME = "creatifs";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CreatifsRepository creatifsRepository;

    public CreatifsResource(CreatifsRepository creatifsRepository) {
        this.creatifsRepository = creatifsRepository;
    }

    /**
     * {@code POST  /creatifs} : Create a new creatifs.
     *
     * @param creatifs the creatifs to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new creatifs, or with status {@code 400 (Bad Request)} if the creatifs has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/creatifs")
    public ResponseEntity<Creatifs> createCreatifs(@Valid @RequestBody Creatifs creatifs) throws URISyntaxException {
        log.debug("REST request to save Creatifs : {}", creatifs);
        if (creatifs.getId() != null) {
            throw new BadRequestAlertException("A new creatifs cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Creatifs result = creatifsRepository.save(creatifs);
        return ResponseEntity
            .created(new URI("/api/creatifs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /creatifs/:id} : Updates an existing creatifs.
     *
     * @param id the id of the creatifs to save.
     * @param creatifs the creatifs to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated creatifs,
     * or with status {@code 400 (Bad Request)} if the creatifs is not valid,
     * or with status {@code 500 (Internal Server Error)} if the creatifs couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/creatifs/{id}")
    public ResponseEntity<Creatifs> updateCreatifs(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody Creatifs creatifs
    ) throws URISyntaxException {
        log.debug("REST request to update Creatifs : {}, {}", id, creatifs);
        if (creatifs.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, creatifs.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!creatifsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Creatifs result = creatifsRepository.save(creatifs);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, creatifs.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /creatifs/:id} : Partial updates given fields of an existing creatifs, field will ignore if it is null
     *
     * @param id the id of the creatifs to save.
     * @param creatifs the creatifs to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated creatifs,
     * or with status {@code 400 (Bad Request)} if the creatifs is not valid,
     * or with status {@code 404 (Not Found)} if the creatifs is not found,
     * or with status {@code 500 (Internal Server Error)} if the creatifs couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/creatifs/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Creatifs> partialUpdateCreatifs(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody Creatifs creatifs
    ) throws URISyntaxException {
        log.debug("REST request to partial update Creatifs partially : {}, {}", id, creatifs);
        if (creatifs.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, creatifs.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!creatifsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Creatifs> result = creatifsRepository
            .findById(creatifs.getId())
            .map(existingCreatifs -> {
                if (creatifs.getNom() != null) {
                    existingCreatifs.setNom(creatifs.getNom());
                }
                if (creatifs.getMail() != null) {
                    existingCreatifs.setMail(creatifs.getMail());
                }
                if (creatifs.getAdresseSiegeSocial() != null) {
                    existingCreatifs.setAdresseSiegeSocial(creatifs.getAdresseSiegeSocial());
                }
                if (creatifs.getTelephonne() != null) {
                    existingCreatifs.setTelephonne(creatifs.getTelephonne());
                }
                if (creatifs.getInstagram() != null) {
                    existingCreatifs.setInstagram(creatifs.getInstagram());
                }
                if (creatifs.getPrenom() != null) {
                    existingCreatifs.setPrenom(creatifs.getPrenom());
                }
                if (creatifs.getMicroEntreprise() != null) {
                    existingCreatifs.setMicroEntreprise(creatifs.getMicroEntreprise());
                }
                if (creatifs.getDateRecrutement() != null) {
                    existingCreatifs.setDateRecrutement(creatifs.getDateRecrutement());
                }
                if (creatifs.getSiret() != null) {
                    existingCreatifs.setSiret(creatifs.getSiret());
                }
                if (creatifs.getSiren() != null) {
                    existingCreatifs.setSiren(creatifs.getSiren());
                }
                if (creatifs.getVille() != null) {
                    existingCreatifs.setVille(creatifs.getVille());
                }

                return existingCreatifs;
            })
            .map(creatifsRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, creatifs.getId().toString())
        );
    }

    /**
     * {@code GET  /creatifs} : get all the creatifs.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of creatifs in body.
     */
    @GetMapping("/creatifs")
    public List<Creatifs> getAllCreatifs() {
        log.debug("REST request to get all Creatifs");
        return creatifsRepository.findAll();
    }

    /**
     * {@code GET  /creatifs/:id} : get the "id" creatifs.
     *
     * @param id the id of the creatifs to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the creatifs, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/creatifs/{id}")
    public ResponseEntity<Creatifs> getCreatifs(@PathVariable Long id) {
        log.debug("REST request to get Creatifs : {}", id);
        Optional<Creatifs> creatifs = creatifsRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(creatifs);
    }

    /**
     * {@code DELETE  /creatifs/:id} : delete the "id" creatifs.
     *
     * @param id the id of the creatifs to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/creatifs/{id}")
    public ResponseEntity<Void> deleteCreatifs(@PathVariable Long id) {
        log.debug("REST request to delete Creatifs : {}", id);
        creatifsRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
