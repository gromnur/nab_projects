package com.nabcommunity.web.rest;

import com.nabcommunity.domain.StatusCreatifsHistorique;
import com.nabcommunity.repository.StatusCreatifsHistoriqueRepository;
import com.nabcommunity.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.nabcommunity.domain.StatusCreatifsHistorique}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class StatusCreatifsHistoriqueResource {

    private final Logger log = LoggerFactory.getLogger(StatusCreatifsHistoriqueResource.class);

    private static final String ENTITY_NAME = "statusCreatifsHistorique";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final StatusCreatifsHistoriqueRepository statusCreatifsHistoriqueRepository;

    public StatusCreatifsHistoriqueResource(StatusCreatifsHistoriqueRepository statusCreatifsHistoriqueRepository) {
        this.statusCreatifsHistoriqueRepository = statusCreatifsHistoriqueRepository;
    }

    /**
     * {@code POST  /status-creatifs-historiques} : Create a new statusCreatifsHistorique.
     *
     * @param statusCreatifsHistorique the statusCreatifsHistorique to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new statusCreatifsHistorique, or with status {@code 400 (Bad Request)} if the statusCreatifsHistorique has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/status-creatifs-historiques")
    public ResponseEntity<StatusCreatifsHistorique> createStatusCreatifsHistorique(
        @RequestBody StatusCreatifsHistorique statusCreatifsHistorique
    ) throws URISyntaxException {
        log.debug("REST request to save StatusCreatifsHistorique : {}", statusCreatifsHistorique);
        if (statusCreatifsHistorique.getId() != null) {
            throw new BadRequestAlertException("A new statusCreatifsHistorique cannot already have an ID", ENTITY_NAME, "idexists");
        }
        StatusCreatifsHistorique result = statusCreatifsHistoriqueRepository.save(statusCreatifsHistorique);
        return ResponseEntity
            .created(new URI("/api/status-creatifs-historiques/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /status-creatifs-historiques/:id} : Updates an existing statusCreatifsHistorique.
     *
     * @param id the id of the statusCreatifsHistorique to save.
     * @param statusCreatifsHistorique the statusCreatifsHistorique to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated statusCreatifsHistorique,
     * or with status {@code 400 (Bad Request)} if the statusCreatifsHistorique is not valid,
     * or with status {@code 500 (Internal Server Error)} if the statusCreatifsHistorique couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/status-creatifs-historiques/{id}")
    public ResponseEntity<StatusCreatifsHistorique> updateStatusCreatifsHistorique(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody StatusCreatifsHistorique statusCreatifsHistorique
    ) throws URISyntaxException {
        log.debug("REST request to update StatusCreatifsHistorique : {}, {}", id, statusCreatifsHistorique);
        if (statusCreatifsHistorique.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, statusCreatifsHistorique.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!statusCreatifsHistoriqueRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        StatusCreatifsHistorique result = statusCreatifsHistoriqueRepository.save(statusCreatifsHistorique);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, statusCreatifsHistorique.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /status-creatifs-historiques/:id} : Partial updates given fields of an existing statusCreatifsHistorique, field will ignore if it is null
     *
     * @param id the id of the statusCreatifsHistorique to save.
     * @param statusCreatifsHistorique the statusCreatifsHistorique to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated statusCreatifsHistorique,
     * or with status {@code 400 (Bad Request)} if the statusCreatifsHistorique is not valid,
     * or with status {@code 404 (Not Found)} if the statusCreatifsHistorique is not found,
     * or with status {@code 500 (Internal Server Error)} if the statusCreatifsHistorique couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/status-creatifs-historiques/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<StatusCreatifsHistorique> partialUpdateStatusCreatifsHistorique(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody StatusCreatifsHistorique statusCreatifsHistorique
    ) throws URISyntaxException {
        log.debug("REST request to partial update StatusCreatifsHistorique partially : {}, {}", id, statusCreatifsHistorique);
        if (statusCreatifsHistorique.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, statusCreatifsHistorique.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!statusCreatifsHistoriqueRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<StatusCreatifsHistorique> result = statusCreatifsHistoriqueRepository
            .findById(statusCreatifsHistorique.getId())
            .map(existingStatusCreatifsHistorique -> {
                if (statusCreatifsHistorique.getDateModification() != null) {
                    existingStatusCreatifsHistorique.setDateModification(statusCreatifsHistorique.getDateModification());
                }
                if (statusCreatifsHistorique.getStatus() != null) {
                    existingStatusCreatifsHistorique.setStatus(statusCreatifsHistorique.getStatus());
                }

                return existingStatusCreatifsHistorique;
            })
            .map(statusCreatifsHistoriqueRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, statusCreatifsHistorique.getId().toString())
        );
    }

    /**
     * {@code GET  /status-creatifs-historiques} : get all the statusCreatifsHistoriques.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of statusCreatifsHistoriques in body.
     */
    @GetMapping("/status-creatifs-historiques")
    public List<StatusCreatifsHistorique> getAllStatusCreatifsHistoriques() {
        log.debug("REST request to get all StatusCreatifsHistoriques");
        return statusCreatifsHistoriqueRepository.findAll();
    }

    /**
     * {@code GET  /status-creatifs-historiques/:id} : get the "id" statusCreatifsHistorique.
     *
     * @param id the id of the statusCreatifsHistorique to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the statusCreatifsHistorique, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/status-creatifs-historiques/{id}")
    public ResponseEntity<StatusCreatifsHistorique> getStatusCreatifsHistorique(@PathVariable Long id) {
        log.debug("REST request to get StatusCreatifsHistorique : {}", id);
        Optional<StatusCreatifsHistorique> statusCreatifsHistorique = statusCreatifsHistoriqueRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(statusCreatifsHistorique);
    }

    /**
     * {@code DELETE  /status-creatifs-historiques/:id} : delete the "id" statusCreatifsHistorique.
     *
     * @param id the id of the statusCreatifsHistorique to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/status-creatifs-historiques/{id}")
    public ResponseEntity<Void> deleteStatusCreatifsHistorique(@PathVariable Long id) {
        log.debug("REST request to delete StatusCreatifsHistorique : {}", id);
        statusCreatifsHistoriqueRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
