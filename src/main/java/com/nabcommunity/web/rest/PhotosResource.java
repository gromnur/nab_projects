package com.nabcommunity.web.rest;

import com.nabcommunity.domain.Photos;
import com.nabcommunity.repository.PhotosRepository;
import com.nabcommunity.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.nabcommunity.domain.Photos}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class PhotosResource {

    private final Logger log = LoggerFactory.getLogger(PhotosResource.class);

    private static final String ENTITY_NAME = "photos";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PhotosRepository photosRepository;

    public PhotosResource(PhotosRepository photosRepository) {
        this.photosRepository = photosRepository;
    }

    /**
     * {@code POST  /photos} : Create a new photos.
     *
     * @param photos the photos to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new photos, or with status {@code 400 (Bad Request)} if the photos has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/photos")
    public ResponseEntity<Photos> createPhotos(@RequestBody Photos photos) throws URISyntaxException {
        log.debug("REST request to save Photos : {}", photos);
        if (photos.getId() != null) {
            throw new BadRequestAlertException("A new photos cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Photos result = photosRepository.save(photos);
        return ResponseEntity
            .created(new URI("/api/photos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /photos/:id} : Updates an existing photos.
     *
     * @param id the id of the photos to save.
     * @param photos the photos to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated photos,
     * or with status {@code 400 (Bad Request)} if the photos is not valid,
     * or with status {@code 500 (Internal Server Error)} if the photos couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/photos/{id}")
    public ResponseEntity<Photos> updatePhotos(@PathVariable(value = "id", required = false) final Long id, @RequestBody Photos photos)
        throws URISyntaxException {
        log.debug("REST request to update Photos : {}, {}", id, photos);
        if (photos.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, photos.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!photosRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Photos result = photosRepository.save(photos);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, photos.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /photos/:id} : Partial updates given fields of an existing photos, field will ignore if it is null
     *
     * @param id the id of the photos to save.
     * @param photos the photos to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated photos,
     * or with status {@code 400 (Bad Request)} if the photos is not valid,
     * or with status {@code 404 (Not Found)} if the photos is not found,
     * or with status {@code 500 (Internal Server Error)} if the photos couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/photos/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Photos> partialUpdatePhotos(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Photos photos
    ) throws URISyntaxException {
        log.debug("REST request to partial update Photos partially : {}, {}", id, photos);
        if (photos.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, photos.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!photosRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Photos> result = photosRepository
            .findById(photos.getId())
            .map(existingPhotos -> {
                if (photos.getLien() != null) {
                    existingPhotos.setLien(photos.getLien());
                }

                return existingPhotos;
            })
            .map(photosRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, photos.getId().toString())
        );
    }

    /**
     * {@code GET  /photos} : get all the photos.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of photos in body.
     */
    @GetMapping("/photos")
    public List<Photos> getAllPhotos() {
        log.debug("REST request to get all Photos");
        return photosRepository.findAll();
    }

    /**
     * {@code GET  /photos/:id} : get the "id" photos.
     *
     * @param id the id of the photos to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the photos, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/photos/{id}")
    public ResponseEntity<Photos> getPhotos(@PathVariable Long id) {
        log.debug("REST request to get Photos : {}", id);
        Optional<Photos> photos = photosRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(photos);
    }

    /**
     * {@code DELETE  /photos/:id} : delete the "id" photos.
     *
     * @param id the id of the photos to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/photos/{id}")
    public ResponseEntity<Void> deletePhotos(@PathVariable Long id) {
        log.debug("REST request to delete Photos : {}", id);
        photosRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
