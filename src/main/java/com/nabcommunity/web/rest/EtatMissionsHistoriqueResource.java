package com.nabcommunity.web.rest;

import com.nabcommunity.domain.EtatMissionsHistorique;
import com.nabcommunity.repository.EtatMissionsHistoriqueRepository;
import com.nabcommunity.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.nabcommunity.domain.EtatMissionsHistorique}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class EtatMissionsHistoriqueResource {

    private final Logger log = LoggerFactory.getLogger(EtatMissionsHistoriqueResource.class);

    private static final String ENTITY_NAME = "etatMissionsHistorique";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EtatMissionsHistoriqueRepository etatMissionsHistoriqueRepository;

    public EtatMissionsHistoriqueResource(EtatMissionsHistoriqueRepository etatMissionsHistoriqueRepository) {
        this.etatMissionsHistoriqueRepository = etatMissionsHistoriqueRepository;
    }

    /**
     * {@code POST  /etat-missions-historiques} : Create a new etatMissionsHistorique.
     *
     * @param etatMissionsHistorique the etatMissionsHistorique to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new etatMissionsHistorique, or with status {@code 400 (Bad Request)} if the etatMissionsHistorique has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/etat-missions-historiques")
    public ResponseEntity<EtatMissionsHistorique> createEtatMissionsHistorique(@RequestBody EtatMissionsHistorique etatMissionsHistorique)
        throws URISyntaxException {
        log.debug("REST request to save EtatMissionsHistorique : {}", etatMissionsHistorique);
        if (etatMissionsHistorique.getId() != null) {
            throw new BadRequestAlertException("A new etatMissionsHistorique cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EtatMissionsHistorique result = etatMissionsHistoriqueRepository.save(etatMissionsHistorique);
        return ResponseEntity
            .created(new URI("/api/etat-missions-historiques/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /etat-missions-historiques/:id} : Updates an existing etatMissionsHistorique.
     *
     * @param id the id of the etatMissionsHistorique to save.
     * @param etatMissionsHistorique the etatMissionsHistorique to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated etatMissionsHistorique,
     * or with status {@code 400 (Bad Request)} if the etatMissionsHistorique is not valid,
     * or with status {@code 500 (Internal Server Error)} if the etatMissionsHistorique couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/etat-missions-historiques/{id}")
    public ResponseEntity<EtatMissionsHistorique> updateEtatMissionsHistorique(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody EtatMissionsHistorique etatMissionsHistorique
    ) throws URISyntaxException {
        log.debug("REST request to update EtatMissionsHistorique : {}, {}", id, etatMissionsHistorique);
        if (etatMissionsHistorique.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, etatMissionsHistorique.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!etatMissionsHistoriqueRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        EtatMissionsHistorique result = etatMissionsHistoriqueRepository.save(etatMissionsHistorique);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, etatMissionsHistorique.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /etat-missions-historiques/:id} : Partial updates given fields of an existing etatMissionsHistorique, field will ignore if it is null
     *
     * @param id the id of the etatMissionsHistorique to save.
     * @param etatMissionsHistorique the etatMissionsHistorique to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated etatMissionsHistorique,
     * or with status {@code 400 (Bad Request)} if the etatMissionsHistorique is not valid,
     * or with status {@code 404 (Not Found)} if the etatMissionsHistorique is not found,
     * or with status {@code 500 (Internal Server Error)} if the etatMissionsHistorique couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/etat-missions-historiques/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<EtatMissionsHistorique> partialUpdateEtatMissionsHistorique(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody EtatMissionsHistorique etatMissionsHistorique
    ) throws URISyntaxException {
        log.debug("REST request to partial update EtatMissionsHistorique partially : {}, {}", id, etatMissionsHistorique);
        if (etatMissionsHistorique.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, etatMissionsHistorique.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!etatMissionsHistoriqueRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<EtatMissionsHistorique> result = etatMissionsHistoriqueRepository
            .findById(etatMissionsHistorique.getId())
            .map(existingEtatMissionsHistorique -> {
                if (etatMissionsHistorique.getDateModification() != null) {
                    existingEtatMissionsHistorique.setDateModification(etatMissionsHistorique.getDateModification());
                }
                if (etatMissionsHistorique.getEtat() != null) {
                    existingEtatMissionsHistorique.setEtat(etatMissionsHistorique.getEtat());
                }

                return existingEtatMissionsHistorique;
            })
            .map(etatMissionsHistoriqueRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, etatMissionsHistorique.getId().toString())
        );
    }

    /**
     * {@code GET  /etat-missions-historiques} : get all the etatMissionsHistoriques.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of etatMissionsHistoriques in body.
     */
    @GetMapping("/etat-missions-historiques")
    public List<EtatMissionsHistorique> getAllEtatMissionsHistoriques() {
        log.debug("REST request to get all EtatMissionsHistoriques");
        return etatMissionsHistoriqueRepository.findAll();
    }

    /**
     * {@code GET  /etat-missions-historiques/:id} : get the "id" etatMissionsHistorique.
     *
     * @param id the id of the etatMissionsHistorique to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the etatMissionsHistorique, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/etat-missions-historiques/{id}")
    public ResponseEntity<EtatMissionsHistorique> getEtatMissionsHistorique(@PathVariable Long id) {
        log.debug("REST request to get EtatMissionsHistorique : {}", id);
        Optional<EtatMissionsHistorique> etatMissionsHistorique = etatMissionsHistoriqueRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(etatMissionsHistorique);
    }

    /**
     * {@code DELETE  /etat-missions-historiques/:id} : delete the "id" etatMissionsHistorique.
     *
     * @param id the id of the etatMissionsHistorique to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/etat-missions-historiques/{id}")
    public ResponseEntity<Void> deleteEtatMissionsHistorique(@PathVariable Long id) {
        log.debug("REST request to delete EtatMissionsHistorique : {}", id);
        etatMissionsHistoriqueRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
