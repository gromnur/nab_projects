/**
 * View Models used by Spring MVC REST controllers.
 */
package com.nabcommunity.web.rest.vm;
