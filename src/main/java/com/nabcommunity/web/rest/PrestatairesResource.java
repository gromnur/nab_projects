package com.nabcommunity.web.rest;

import com.nabcommunity.domain.Prestataires;
import com.nabcommunity.repository.PrestatairesRepository;
import com.nabcommunity.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.nabcommunity.domain.Prestataires}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class PrestatairesResource {

    private final Logger log = LoggerFactory.getLogger(PrestatairesResource.class);

    private static final String ENTITY_NAME = "prestataires";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PrestatairesRepository prestatairesRepository;

    public PrestatairesResource(PrestatairesRepository prestatairesRepository) {
        this.prestatairesRepository = prestatairesRepository;
    }

    /**
     * {@code POST  /prestataires} : Create a new prestataires.
     *
     * @param prestataires the prestataires to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new prestataires, or with status {@code 400 (Bad Request)} if the prestataires has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/prestataires")
    public ResponseEntity<Prestataires> createPrestataires(@Valid @RequestBody Prestataires prestataires) throws URISyntaxException {
        log.debug("REST request to save Prestataires : {}", prestataires);
        if (prestataires.getId() != null) {
            throw new BadRequestAlertException("A new prestataires cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Prestataires result = prestatairesRepository.save(prestataires);
        return ResponseEntity
            .created(new URI("/api/prestataires/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /prestataires/:id} : Updates an existing prestataires.
     *
     * @param id the id of the prestataires to save.
     * @param prestataires the prestataires to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated prestataires,
     * or with status {@code 400 (Bad Request)} if the prestataires is not valid,
     * or with status {@code 500 (Internal Server Error)} if the prestataires couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/prestataires/{id}")
    public ResponseEntity<Prestataires> updatePrestataires(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody Prestataires prestataires
    ) throws URISyntaxException {
        log.debug("REST request to update Prestataires : {}, {}", id, prestataires);
        if (prestataires.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, prestataires.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!prestatairesRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Prestataires result = prestatairesRepository.save(prestataires);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, prestataires.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /prestataires/:id} : Partial updates given fields of an existing prestataires, field will ignore if it is null
     *
     * @param id the id of the prestataires to save.
     * @param prestataires the prestataires to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated prestataires,
     * or with status {@code 400 (Bad Request)} if the prestataires is not valid,
     * or with status {@code 404 (Not Found)} if the prestataires is not found,
     * or with status {@code 500 (Internal Server Error)} if the prestataires couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/prestataires/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Prestataires> partialUpdatePrestataires(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody Prestataires prestataires
    ) throws URISyntaxException {
        log.debug("REST request to partial update Prestataires partially : {}, {}", id, prestataires);
        if (prestataires.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, prestataires.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!prestatairesRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Prestataires> result = prestatairesRepository
            .findById(prestataires.getId())
            .map(existingPrestataires -> {
                if (prestataires.getNomSociete() != null) {
                    existingPrestataires.setNomSociete(prestataires.getNomSociete());
                }
                if (prestataires.getNom() != null) {
                    existingPrestataires.setNom(prestataires.getNom());
                }
                if (prestataires.getPrenom() != null) {
                    existingPrestataires.setPrenom(prestataires.getPrenom());
                }
                if (prestataires.getMail() != null) {
                    existingPrestataires.setMail(prestataires.getMail());
                }
                if (prestataires.getAdresse() != null) {
                    existingPrestataires.setAdresse(prestataires.getAdresse());
                }
                if (prestataires.getMicroEntreprise() != null) {
                    existingPrestataires.setMicroEntreprise(prestataires.getMicroEntreprise());
                }
                if (prestataires.getInstagram() != null) {
                    existingPrestataires.setInstagram(prestataires.getInstagram());
                }

                return existingPrestataires;
            })
            .map(prestatairesRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, prestataires.getId().toString())
        );
    }

    /**
     * {@code GET  /prestataires} : get all the prestataires.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of prestataires in body.
     */
    @GetMapping("/prestataires")
    public List<Prestataires> getAllPrestataires() {
        log.debug("REST request to get all Prestataires");
        return prestatairesRepository.findAll();
    }

    /**
     * {@code GET  /prestataires/:id} : get the "id" prestataires.
     *
     * @param id the id of the prestataires to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the prestataires, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/prestataires/{id}")
    public ResponseEntity<Prestataires> getPrestataires(@PathVariable Long id) {
        log.debug("REST request to get Prestataires : {}", id);
        Optional<Prestataires> prestataires = prestatairesRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(prestataires);
    }

    /**
     * {@code DELETE  /prestataires/:id} : delete the "id" prestataires.
     *
     * @param id the id of the prestataires to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/prestataires/{id}")
    public ResponseEntity<Void> deletePrestataires(@PathVariable Long id) {
        log.debug("REST request to delete Prestataires : {}", id);
        prestatairesRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
