package com.nabcommunity.repository;

import com.nabcommunity.domain.EtatMissionsHistorique;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the EtatMissionsHistorique entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EtatMissionsHistoriqueRepository extends JpaRepository<EtatMissionsHistorique, Long> {}
