package com.nabcommunity.repository;

import com.nabcommunity.domain.Competances;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Competances entity.
 */
@Repository
public interface CompetancesRepository extends JpaRepository<Competances, Long> {
    @Query(
        value = "select distinct competances from Competances competances left join fetch competances.missions left join fetch competances.creatifs",
        countQuery = "select count(distinct competances) from Competances competances"
    )
    Page<Competances> findAllWithEagerRelationships(Pageable pageable);

    @Query(
        "select distinct competances from Competances competances left join fetch competances.missions left join fetch competances.creatifs"
    )
    List<Competances> findAllWithEagerRelationships();

    @Query(
        "select competances from Competances competances left join fetch competances.missions left join fetch competances.creatifs where competances.id =:id"
    )
    Optional<Competances> findOneWithEagerRelationships(@Param("id") Long id);
}
