package com.nabcommunity.repository;

import com.nabcommunity.domain.StatusValidationsHistorique;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the StatusValidationsHistorique entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StatusValidationsHistoriqueRepository extends JpaRepository<StatusValidationsHistorique, Long> {}
