package com.nabcommunity.repository;

import com.nabcommunity.domain.StatusCreatifsHistorique;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the StatusCreatifsHistorique entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StatusCreatifsHistoriqueRepository extends JpaRepository<StatusCreatifsHistorique, Long> {}
