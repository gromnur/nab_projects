package com.nabcommunity.repository;

import com.nabcommunity.domain.Creatifs;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Creatifs entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CreatifsRepository extends JpaRepository<Creatifs, Long> {}
