package com.nabcommunity.repository;

import com.nabcommunity.domain.Missions;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Missions entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MissionsRepository extends JpaRepository<Missions, Long> {}
