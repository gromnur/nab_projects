package com.nabcommunity.repository;

import com.nabcommunity.domain.Prestataires;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Prestataires entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PrestatairesRepository extends JpaRepository<Prestataires, Long> {}
