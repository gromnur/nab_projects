package com.nabcommunity.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Competances.
 */
@Entity
@Table(name = "competances")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Competances implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Size(max = 50)
    @Column(name = "nom", length = 50)
    private String nom;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "parent")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "childs", "competancePrincipalCreatifs", "missions", "creatifs", "parent" }, allowSetters = true)
    private Set<Competances> childs = new HashSet<>();

    @OneToMany(mappedBy = "competancePrincipal")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(
        value = { "missions", "competancePrincipal", "statusCreatifsHistorique", "statusValidationsHistorique", "competances" },
        allowSetters = true
    )
    private Set<Creatifs> competancePrincipalCreatifs = new HashSet<>();

    @ManyToMany
    @JoinTable(
        name = "rel_competances__missions",
        joinColumns = @JoinColumn(name = "competances_id"),
        inverseJoinColumns = @JoinColumn(name = "missions_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(
        value = { "photos", "etatMissionsHistorique", "prestataire", "creatif", "statusValidationsHistorique", "competances" },
        allowSetters = true
    )
    private Set<Missions> missions = new HashSet<>();

    @ManyToMany
    @JoinTable(
        name = "rel_competances__creatifs",
        joinColumns = @JoinColumn(name = "competances_id"),
        inverseJoinColumns = @JoinColumn(name = "creatifs_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(
        value = { "missions", "competancePrincipal", "statusCreatifsHistorique", "statusValidationsHistorique", "competances" },
        allowSetters = true
    )
    private Set<Creatifs> creatifs = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "childs", "competancePrincipalCreatifs", "missions", "creatifs", "parent" }, allowSetters = true)
    private Competances parent;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Competances id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return this.nom;
    }

    public Competances nom(String nom) {
        this.setNom(nom);
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return this.description;
    }

    public Competances description(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Competances> getChilds() {
        return this.childs;
    }

    public void setChilds(Set<Competances> competances) {
        if (this.childs != null) {
            this.childs.forEach(i -> i.setParent(null));
        }
        if (competances != null) {
            competances.forEach(i -> i.setParent(this));
        }
        this.childs = competances;
    }

    public Competances childs(Set<Competances> competances) {
        this.setChilds(competances);
        return this;
    }

    public Competances addChilds(Competances competances) {
        this.childs.add(competances);
        competances.setParent(this);
        return this;
    }

    public Competances removeChilds(Competances competances) {
        this.childs.remove(competances);
        competances.setParent(null);
        return this;
    }

    public Set<Creatifs> getCompetancePrincipalCreatifs() {
        return this.competancePrincipalCreatifs;
    }

    public void setCompetancePrincipalCreatifs(Set<Creatifs> creatifs) {
        if (this.competancePrincipalCreatifs != null) {
            this.competancePrincipalCreatifs.forEach(i -> i.setCompetancePrincipal(null));
        }
        if (creatifs != null) {
            creatifs.forEach(i -> i.setCompetancePrincipal(this));
        }
        this.competancePrincipalCreatifs = creatifs;
    }

    public Competances competancePrincipalCreatifs(Set<Creatifs> creatifs) {
        this.setCompetancePrincipalCreatifs(creatifs);
        return this;
    }

    public Competances addCompetancePrincipalCreatif(Creatifs creatifs) {
        this.competancePrincipalCreatifs.add(creatifs);
        creatifs.setCompetancePrincipal(this);
        return this;
    }

    public Competances removeCompetancePrincipalCreatif(Creatifs creatifs) {
        this.competancePrincipalCreatifs.remove(creatifs);
        creatifs.setCompetancePrincipal(null);
        return this;
    }

    public Set<Missions> getMissions() {
        return this.missions;
    }

    public void setMissions(Set<Missions> missions) {
        this.missions = missions;
    }

    public Competances missions(Set<Missions> missions) {
        this.setMissions(missions);
        return this;
    }

    public Competances addMissions(Missions missions) {
        this.missions.add(missions);
        missions.getCompetances().add(this);
        return this;
    }

    public Competances removeMissions(Missions missions) {
        this.missions.remove(missions);
        missions.getCompetances().remove(this);
        return this;
    }

    public Set<Creatifs> getCreatifs() {
        return this.creatifs;
    }

    public void setCreatifs(Set<Creatifs> creatifs) {
        this.creatifs = creatifs;
    }

    public Competances creatifs(Set<Creatifs> creatifs) {
        this.setCreatifs(creatifs);
        return this;
    }

    public Competances addCreatifs(Creatifs creatifs) {
        this.creatifs.add(creatifs);
        creatifs.getCompetances().add(this);
        return this;
    }

    public Competances removeCreatifs(Creatifs creatifs) {
        this.creatifs.remove(creatifs);
        creatifs.getCompetances().remove(this);
        return this;
    }

    public Competances getParent() {
        return this.parent;
    }

    public void setParent(Competances competances) {
        this.parent = competances;
    }

    public Competances parent(Competances competances) {
        this.setParent(competances);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Competances)) {
            return false;
        }
        return id != null && id.equals(((Competances) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Competances{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
