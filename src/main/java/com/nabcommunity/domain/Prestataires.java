package com.nabcommunity.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Prestataires.
 */
@Entity
@Table(name = "prestataires")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Prestataires implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Size(max = 50)
    @Column(name = "nom_societe", length = 50)
    private String nomSociete;

    @Size(max = 50)
    @Column(name = "nom", length = 50)
    private String nom;

    @Size(max = 50)
    @Column(name = "prenom", length = 50)
    private String prenom;

    @Size(max = 50)
    @Column(name = "mail", length = 50)
    private String mail;

    @Size(max = 100)
    @Column(name = "adresse", length = 100)
    private String adresse;

    @Column(name = "micro_entreprise")
    private Boolean microEntreprise;

    @Size(max = 50)
    @Column(name = "instagram", length = 50)
    private String instagram;

    @OneToMany(mappedBy = "prestataire")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(
        value = { "photos", "etatMissionsHistorique", "prestataire", "creatif", "statusValidationsHistorique", "competances" },
        allowSetters = true
    )
    private Set<Missions> missions = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Prestataires id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomSociete() {
        return this.nomSociete;
    }

    public Prestataires nomSociete(String nomSociete) {
        this.setNomSociete(nomSociete);
        return this;
    }

    public void setNomSociete(String nomSociete) {
        this.nomSociete = nomSociete;
    }

    public String getNom() {
        return this.nom;
    }

    public Prestataires nom(String nom) {
        this.setNom(nom);
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public Prestataires prenom(String prenom) {
        this.setPrenom(prenom);
        return this;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getMail() {
        return this.mail;
    }

    public Prestataires mail(String mail) {
        this.setMail(mail);
        return this;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getAdresse() {
        return this.adresse;
    }

    public Prestataires adresse(String adresse) {
        this.setAdresse(adresse);
        return this;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public Boolean getMicroEntreprise() {
        return this.microEntreprise;
    }

    public Prestataires microEntreprise(Boolean microEntreprise) {
        this.setMicroEntreprise(microEntreprise);
        return this;
    }

    public void setMicroEntreprise(Boolean microEntreprise) {
        this.microEntreprise = microEntreprise;
    }

    public String getInstagram() {
        return this.instagram;
    }

    public Prestataires instagram(String instagram) {
        this.setInstagram(instagram);
        return this;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public Set<Missions> getMissions() {
        return this.missions;
    }

    public void setMissions(Set<Missions> missions) {
        if (this.missions != null) {
            this.missions.forEach(i -> i.setPrestataire(null));
        }
        if (missions != null) {
            missions.forEach(i -> i.setPrestataire(this));
        }
        this.missions = missions;
    }

    public Prestataires missions(Set<Missions> missions) {
        this.setMissions(missions);
        return this;
    }

    public Prestataires addMission(Missions missions) {
        this.missions.add(missions);
        missions.setPrestataire(this);
        return this;
    }

    public Prestataires removeMission(Missions missions) {
        this.missions.remove(missions);
        missions.setPrestataire(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Prestataires)) {
            return false;
        }
        return id != null && id.equals(((Prestataires) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Prestataires{" +
            "id=" + getId() +
            ", nomSociete='" + getNomSociete() + "'" +
            ", nom='" + getNom() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", mail='" + getMail() + "'" +
            ", adresse='" + getAdresse() + "'" +
            ", microEntreprise='" + getMicroEntreprise() + "'" +
            ", instagram='" + getInstagram() + "'" +
            "}";
    }
}
