package com.nabcommunity.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.nabcommunity.domain.enumeration.StatusCreatifs;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A StatusCreatifsHistorique.
 */
@Entity
@Table(name = "status_creatifs_historique")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class StatusCreatifsHistorique implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "date_modification")
    private LocalDate dateModification;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private StatusCreatifs status;

    @OneToMany(mappedBy = "statusCreatifsHistorique")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(
        value = { "missions", "competancePrincipal", "statusCreatifsHistorique", "statusValidationsHistorique", "competances" },
        allowSetters = true
    )
    private Set<Creatifs> creatifs = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public StatusCreatifsHistorique id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateModification() {
        return this.dateModification;
    }

    public StatusCreatifsHistorique dateModification(LocalDate dateModification) {
        this.setDateModification(dateModification);
        return this;
    }

    public void setDateModification(LocalDate dateModification) {
        this.dateModification = dateModification;
    }

    public StatusCreatifs getStatus() {
        return this.status;
    }

    public StatusCreatifsHistorique status(StatusCreatifs status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(StatusCreatifs status) {
        this.status = status;
    }

    public Set<Creatifs> getCreatifs() {
        return this.creatifs;
    }

    public void setCreatifs(Set<Creatifs> creatifs) {
        if (this.creatifs != null) {
            this.creatifs.forEach(i -> i.setStatusCreatifsHistorique(null));
        }
        if (creatifs != null) {
            creatifs.forEach(i -> i.setStatusCreatifsHistorique(this));
        }
        this.creatifs = creatifs;
    }

    public StatusCreatifsHistorique creatifs(Set<Creatifs> creatifs) {
        this.setCreatifs(creatifs);
        return this;
    }

    public StatusCreatifsHistorique addCreatif(Creatifs creatifs) {
        this.creatifs.add(creatifs);
        creatifs.setStatusCreatifsHistorique(this);
        return this;
    }

    public StatusCreatifsHistorique removeCreatif(Creatifs creatifs) {
        this.creatifs.remove(creatifs);
        creatifs.setStatusCreatifsHistorique(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof StatusCreatifsHistorique)) {
            return false;
        }
        return id != null && id.equals(((StatusCreatifsHistorique) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "StatusCreatifsHistorique{" +
            "id=" + getId() +
            ", dateModification='" + getDateModification() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
