package com.nabcommunity.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.nabcommunity.domain.enumeration.StatusValidations;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A StatusValidationsHistorique.
 */
@Entity
@Table(name = "status_validations_historique")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class StatusValidationsHistorique implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "date_modifiacation")
    private LocalDate dateModifiacation;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private StatusValidations status;

    @OneToMany(mappedBy = "statusValidationsHistorique")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(
        value = { "photos", "etatMissionsHistorique", "prestataire", "creatif", "statusValidationsHistorique", "competances" },
        allowSetters = true
    )
    private Set<Missions> missions = new HashSet<>();

    @OneToMany(mappedBy = "statusValidationsHistorique")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(
        value = { "missions", "competancePrincipal", "statusCreatifsHistorique", "statusValidationsHistorique", "competances" },
        allowSetters = true
    )
    private Set<Creatifs> creatifs = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public StatusValidationsHistorique id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateModifiacation() {
        return this.dateModifiacation;
    }

    public StatusValidationsHistorique dateModifiacation(LocalDate dateModifiacation) {
        this.setDateModifiacation(dateModifiacation);
        return this;
    }

    public void setDateModifiacation(LocalDate dateModifiacation) {
        this.dateModifiacation = dateModifiacation;
    }

    public StatusValidations getStatus() {
        return this.status;
    }

    public StatusValidationsHistorique status(StatusValidations status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(StatusValidations status) {
        this.status = status;
    }

    public Set<Missions> getMissions() {
        return this.missions;
    }

    public void setMissions(Set<Missions> missions) {
        if (this.missions != null) {
            this.missions.forEach(i -> i.setStatusValidationsHistorique(null));
        }
        if (missions != null) {
            missions.forEach(i -> i.setStatusValidationsHistorique(this));
        }
        this.missions = missions;
    }

    public StatusValidationsHistorique missions(Set<Missions> missions) {
        this.setMissions(missions);
        return this;
    }

    public StatusValidationsHistorique addMission(Missions missions) {
        this.missions.add(missions);
        missions.setStatusValidationsHistorique(this);
        return this;
    }

    public StatusValidationsHistorique removeMission(Missions missions) {
        this.missions.remove(missions);
        missions.setStatusValidationsHistorique(null);
        return this;
    }

    public Set<Creatifs> getCreatifs() {
        return this.creatifs;
    }

    public void setCreatifs(Set<Creatifs> creatifs) {
        if (this.creatifs != null) {
            this.creatifs.forEach(i -> i.setStatusValidationsHistorique(null));
        }
        if (creatifs != null) {
            creatifs.forEach(i -> i.setStatusValidationsHistorique(this));
        }
        this.creatifs = creatifs;
    }

    public StatusValidationsHistorique creatifs(Set<Creatifs> creatifs) {
        this.setCreatifs(creatifs);
        return this;
    }

    public StatusValidationsHistorique addCreatif(Creatifs creatifs) {
        this.creatifs.add(creatifs);
        creatifs.setStatusValidationsHistorique(this);
        return this;
    }

    public StatusValidationsHistorique removeCreatif(Creatifs creatifs) {
        this.creatifs.remove(creatifs);
        creatifs.setStatusValidationsHistorique(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof StatusValidationsHistorique)) {
            return false;
        }
        return id != null && id.equals(((StatusValidationsHistorique) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "StatusValidationsHistorique{" +
            "id=" + getId() +
            ", dateModifiacation='" + getDateModifiacation() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
