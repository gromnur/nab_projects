package com.nabcommunity.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.nabcommunity.domain.enumeration.EtatMissions;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A EtatMissionsHistorique.
 */
@Entity
@Table(name = "etat_missions_historique")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EtatMissionsHistorique implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "date_modification")
    private LocalDate dateModification;

    @Enumerated(EnumType.STRING)
    @Column(name = "etat")
    private EtatMissions etat;

    @OneToMany(mappedBy = "etatMissionsHistorique")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(
        value = { "photos", "etatMissionsHistorique", "prestataire", "creatif", "statusValidationsHistorique", "competances" },
        allowSetters = true
    )
    private Set<Missions> missions = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public EtatMissionsHistorique id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateModification() {
        return this.dateModification;
    }

    public EtatMissionsHistorique dateModification(LocalDate dateModification) {
        this.setDateModification(dateModification);
        return this;
    }

    public void setDateModification(LocalDate dateModification) {
        this.dateModification = dateModification;
    }

    public EtatMissions getEtat() {
        return this.etat;
    }

    public EtatMissionsHistorique etat(EtatMissions etat) {
        this.setEtat(etat);
        return this;
    }

    public void setEtat(EtatMissions etat) {
        this.etat = etat;
    }

    public Set<Missions> getMissions() {
        return this.missions;
    }

    public void setMissions(Set<Missions> missions) {
        if (this.missions != null) {
            this.missions.forEach(i -> i.setEtatMissionsHistorique(null));
        }
        if (missions != null) {
            missions.forEach(i -> i.setEtatMissionsHistorique(this));
        }
        this.missions = missions;
    }

    public EtatMissionsHistorique missions(Set<Missions> missions) {
        this.setMissions(missions);
        return this;
    }

    public EtatMissionsHistorique addMission(Missions missions) {
        this.missions.add(missions);
        missions.setEtatMissionsHistorique(this);
        return this;
    }

    public EtatMissionsHistorique removeMission(Missions missions) {
        this.missions.remove(missions);
        missions.setEtatMissionsHistorique(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EtatMissionsHistorique)) {
            return false;
        }
        return id != null && id.equals(((EtatMissionsHistorique) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EtatMissionsHistorique{" +
            "id=" + getId() +
            ", dateModification='" + getDateModification() + "'" +
            ", etat='" + getEtat() + "'" +
            "}";
    }
}
