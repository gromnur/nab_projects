package com.nabcommunity.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Creatifs.
 */
@Entity
@Table(name = "creatifs")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Creatifs implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Size(max = 50)
    @Column(name = "nom", length = 50)
    private String nom;

    @Size(max = 50)
    @Column(name = "mail", length = 50)
    private String mail;

    @Size(max = 50)
    @Column(name = "adresse_siege_social", length = 50)
    private String adresseSiegeSocial;

    @Size(max = 10)
    @Column(name = "telephonne", length = 10)
    private String telephonne;

    @Size(max = 50)
    @Column(name = "instagram", length = 50)
    private String instagram;

    @Size(max = 50)
    @Column(name = "prenom", length = 50)
    private String prenom;

    @Column(name = "micro_entreprise")
    private Boolean microEntreprise;

    @Size(max = 50)
    @Column(name = "date_recrutement", length = 50)
    private String dateRecrutement;

    @Size(max = 50)
    @Column(name = "siret", length = 50)
    private String siret;

    @Size(max = 50)
    @Column(name = "siren", length = 50)
    private String siren;

    @Size(max = 50)
    @Column(name = "ville", length = 50)
    private String ville;

    @OneToMany(mappedBy = "creatif")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(
        value = { "photos", "etatMissionsHistorique", "prestataire", "creatif", "statusValidationsHistorique", "competances" },
        allowSetters = true
    )
    private Set<Missions> missions = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "childs", "competancePrincipalCreatifs", "missions", "creatifs", "parent" }, allowSetters = true)
    private Competances competancePrincipal;

    @ManyToOne
    @JsonIgnoreProperties(value = { "creatifs" }, allowSetters = true)
    private StatusCreatifsHistorique statusCreatifsHistorique;

    @ManyToOne
    @JsonIgnoreProperties(value = { "missions", "creatifs" }, allowSetters = true)
    private StatusValidationsHistorique statusValidationsHistorique;

    @ManyToMany(mappedBy = "creatifs")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "childs", "competancePrincipalCreatifs", "missions", "creatifs", "parent" }, allowSetters = true)
    private Set<Competances> competances = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Creatifs id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return this.nom;
    }

    public Creatifs nom(String nom) {
        this.setNom(nom);
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getMail() {
        return this.mail;
    }

    public Creatifs mail(String mail) {
        this.setMail(mail);
        return this;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getAdresseSiegeSocial() {
        return this.adresseSiegeSocial;
    }

    public Creatifs adresseSiegeSocial(String adresseSiegeSocial) {
        this.setAdresseSiegeSocial(adresseSiegeSocial);
        return this;
    }

    public void setAdresseSiegeSocial(String adresseSiegeSocial) {
        this.adresseSiegeSocial = adresseSiegeSocial;
    }

    public String getTelephonne() {
        return this.telephonne;
    }

    public Creatifs telephonne(String telephonne) {
        this.setTelephonne(telephonne);
        return this;
    }

    public void setTelephonne(String telephonne) {
        this.telephonne = telephonne;
    }

    public String getInstagram() {
        return this.instagram;
    }

    public Creatifs instagram(String instagram) {
        this.setInstagram(instagram);
        return this;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public Creatifs prenom(String prenom) {
        this.setPrenom(prenom);
        return this;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Boolean getMicroEntreprise() {
        return this.microEntreprise;
    }

    public Creatifs microEntreprise(Boolean microEntreprise) {
        this.setMicroEntreprise(microEntreprise);
        return this;
    }

    public void setMicroEntreprise(Boolean microEntreprise) {
        this.microEntreprise = microEntreprise;
    }

    public String getDateRecrutement() {
        return this.dateRecrutement;
    }

    public Creatifs dateRecrutement(String dateRecrutement) {
        this.setDateRecrutement(dateRecrutement);
        return this;
    }

    public void setDateRecrutement(String dateRecrutement) {
        this.dateRecrutement = dateRecrutement;
    }

    public String getSiret() {
        return this.siret;
    }

    public Creatifs siret(String siret) {
        this.setSiret(siret);
        return this;
    }

    public void setSiret(String siret) {
        this.siret = siret;
    }

    public String getSiren() {
        return this.siren;
    }

    public Creatifs siren(String siren) {
        this.setSiren(siren);
        return this;
    }

    public void setSiren(String siren) {
        this.siren = siren;
    }

    public String getVille() {
        return this.ville;
    }

    public Creatifs ville(String ville) {
        this.setVille(ville);
        return this;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public Set<Missions> getMissions() {
        return this.missions;
    }

    public void setMissions(Set<Missions> missions) {
        if (this.missions != null) {
            this.missions.forEach(i -> i.setCreatif(null));
        }
        if (missions != null) {
            missions.forEach(i -> i.setCreatif(this));
        }
        this.missions = missions;
    }

    public Creatifs missions(Set<Missions> missions) {
        this.setMissions(missions);
        return this;
    }

    public Creatifs addMission(Missions missions) {
        this.missions.add(missions);
        missions.setCreatif(this);
        return this;
    }

    public Creatifs removeMission(Missions missions) {
        this.missions.remove(missions);
        missions.setCreatif(null);
        return this;
    }

    public Competances getCompetancePrincipal() {
        return this.competancePrincipal;
    }

    public void setCompetancePrincipal(Competances competances) {
        this.competancePrincipal = competances;
    }

    public Creatifs competancePrincipal(Competances competances) {
        this.setCompetancePrincipal(competances);
        return this;
    }

    public StatusCreatifsHistorique getStatusCreatifsHistorique() {
        return this.statusCreatifsHistorique;
    }

    public void setStatusCreatifsHistorique(StatusCreatifsHistorique statusCreatifsHistorique) {
        this.statusCreatifsHistorique = statusCreatifsHistorique;
    }

    public Creatifs statusCreatifsHistorique(StatusCreatifsHistorique statusCreatifsHistorique) {
        this.setStatusCreatifsHistorique(statusCreatifsHistorique);
        return this;
    }

    public StatusValidationsHistorique getStatusValidationsHistorique() {
        return this.statusValidationsHistorique;
    }

    public void setStatusValidationsHistorique(StatusValidationsHistorique statusValidationsHistorique) {
        this.statusValidationsHistorique = statusValidationsHistorique;
    }

    public Creatifs statusValidationsHistorique(StatusValidationsHistorique statusValidationsHistorique) {
        this.setStatusValidationsHistorique(statusValidationsHistorique);
        return this;
    }

    public Set<Competances> getCompetances() {
        return this.competances;
    }

    public void setCompetances(Set<Competances> competances) {
        if (this.competances != null) {
            this.competances.forEach(i -> i.removeCreatifs(this));
        }
        if (competances != null) {
            competances.forEach(i -> i.addCreatifs(this));
        }
        this.competances = competances;
    }

    public Creatifs competances(Set<Competances> competances) {
        this.setCompetances(competances);
        return this;
    }

    public Creatifs addCompetances(Competances competances) {
        this.competances.add(competances);
        competances.getCreatifs().add(this);
        return this;
    }

    public Creatifs removeCompetances(Competances competances) {
        this.competances.remove(competances);
        competances.getCreatifs().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Creatifs)) {
            return false;
        }
        return id != null && id.equals(((Creatifs) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Creatifs{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", mail='" + getMail() + "'" +
            ", adresseSiegeSocial='" + getAdresseSiegeSocial() + "'" +
            ", telephonne='" + getTelephonne() + "'" +
            ", instagram='" + getInstagram() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", microEntreprise='" + getMicroEntreprise() + "'" +
            ", dateRecrutement='" + getDateRecrutement() + "'" +
            ", siret='" + getSiret() + "'" +
            ", siren='" + getSiren() + "'" +
            ", ville='" + getVille() + "'" +
            "}";
    }
}
