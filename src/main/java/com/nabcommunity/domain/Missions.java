package com.nabcommunity.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Missions.
 */
@Entity
@Table(name = "missions")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Missions implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Size(max = 50)
    @Column(name = "nom", length = 50)
    private String nom;

    @Size(max = 50)
    @Column(name = "lieu", length = 50)
    private String lieu;

    @Column(name = "prix")
    private Float prix;

    @Column(name = "date_debut_mission")
    private LocalDate dateDebutMission;

    @Column(name = "date_fin_mission")
    private LocalDate dateFinMission;

    @Column(name = "date_creation_mission")
    private LocalDate dateCreationMission;

    @OneToMany(mappedBy = "mission")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "mission" }, allowSetters = true)
    private Set<Photos> photos = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "missions" }, allowSetters = true)
    private EtatMissionsHistorique etatMissionsHistorique;

    @ManyToOne
    @JsonIgnoreProperties(value = { "missions" }, allowSetters = true)
    private Prestataires prestataire;

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "missions", "competancePrincipal", "statusCreatifsHistorique", "statusValidationsHistorique", "competances" },
        allowSetters = true
    )
    private Creatifs creatif;

    @ManyToOne
    @JsonIgnoreProperties(value = { "missions", "creatifs" }, allowSetters = true)
    private StatusValidationsHistorique statusValidationsHistorique;

    @ManyToMany(mappedBy = "missions")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "childs", "competancePrincipalCreatifs", "missions", "creatifs", "parent" }, allowSetters = true)
    private Set<Competances> competances = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Missions id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return this.nom;
    }

    public Missions nom(String nom) {
        this.setNom(nom);
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getLieu() {
        return this.lieu;
    }

    public Missions lieu(String lieu) {
        this.setLieu(lieu);
        return this;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public Float getPrix() {
        return this.prix;
    }

    public Missions prix(Float prix) {
        this.setPrix(prix);
        return this;
    }

    public void setPrix(Float prix) {
        this.prix = prix;
    }

    public LocalDate getDateDebutMission() {
        return this.dateDebutMission;
    }

    public Missions dateDebutMission(LocalDate dateDebutMission) {
        this.setDateDebutMission(dateDebutMission);
        return this;
    }

    public void setDateDebutMission(LocalDate dateDebutMission) {
        this.dateDebutMission = dateDebutMission;
    }

    public LocalDate getDateFinMission() {
        return this.dateFinMission;
    }

    public Missions dateFinMission(LocalDate dateFinMission) {
        this.setDateFinMission(dateFinMission);
        return this;
    }

    public void setDateFinMission(LocalDate dateFinMission) {
        this.dateFinMission = dateFinMission;
    }

    public LocalDate getDateCreationMission() {
        return this.dateCreationMission;
    }

    public Missions dateCreationMission(LocalDate dateCreationMission) {
        this.setDateCreationMission(dateCreationMission);
        return this;
    }

    public void setDateCreationMission(LocalDate dateCreationMission) {
        this.dateCreationMission = dateCreationMission;
    }

    public Set<Photos> getPhotos() {
        return this.photos;
    }

    public void setPhotos(Set<Photos> photos) {
        if (this.photos != null) {
            this.photos.forEach(i -> i.setMission(null));
        }
        if (photos != null) {
            photos.forEach(i -> i.setMission(this));
        }
        this.photos = photos;
    }

    public Missions photos(Set<Photos> photos) {
        this.setPhotos(photos);
        return this;
    }

    public Missions addPhoto(Photos photos) {
        this.photos.add(photos);
        photos.setMission(this);
        return this;
    }

    public Missions removePhoto(Photos photos) {
        this.photos.remove(photos);
        photos.setMission(null);
        return this;
    }

    public EtatMissionsHistorique getEtatMissionsHistorique() {
        return this.etatMissionsHistorique;
    }

    public void setEtatMissionsHistorique(EtatMissionsHistorique etatMissionsHistorique) {
        this.etatMissionsHistorique = etatMissionsHistorique;
    }

    public Missions etatMissionsHistorique(EtatMissionsHistorique etatMissionsHistorique) {
        this.setEtatMissionsHistorique(etatMissionsHistorique);
        return this;
    }

    public Prestataires getPrestataire() {
        return this.prestataire;
    }

    public void setPrestataire(Prestataires prestataires) {
        this.prestataire = prestataires;
    }

    public Missions prestataire(Prestataires prestataires) {
        this.setPrestataire(prestataires);
        return this;
    }

    public Creatifs getCreatif() {
        return this.creatif;
    }

    public void setCreatif(Creatifs creatifs) {
        this.creatif = creatifs;
    }

    public Missions creatif(Creatifs creatifs) {
        this.setCreatif(creatifs);
        return this;
    }

    public StatusValidationsHistorique getStatusValidationsHistorique() {
        return this.statusValidationsHistorique;
    }

    public void setStatusValidationsHistorique(StatusValidationsHistorique statusValidationsHistorique) {
        this.statusValidationsHistorique = statusValidationsHistorique;
    }

    public Missions statusValidationsHistorique(StatusValidationsHistorique statusValidationsHistorique) {
        this.setStatusValidationsHistorique(statusValidationsHistorique);
        return this;
    }

    public Set<Competances> getCompetances() {
        return this.competances;
    }

    public void setCompetances(Set<Competances> competances) {
        if (this.competances != null) {
            this.competances.forEach(i -> i.removeMissions(this));
        }
        if (competances != null) {
            competances.forEach(i -> i.addMissions(this));
        }
        this.competances = competances;
    }

    public Missions competances(Set<Competances> competances) {
        this.setCompetances(competances);
        return this;
    }

    public Missions addCompetances(Competances competances) {
        this.competances.add(competances);
        competances.getMissions().add(this);
        return this;
    }

    public Missions removeCompetances(Competances competances) {
        this.competances.remove(competances);
        competances.getMissions().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Missions)) {
            return false;
        }
        return id != null && id.equals(((Missions) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Missions{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", lieu='" + getLieu() + "'" +
            ", prix=" + getPrix() +
            ", dateDebutMission='" + getDateDebutMission() + "'" +
            ", dateFinMission='" + getDateFinMission() + "'" +
            ", dateCreationMission='" + getDateCreationMission() + "'" +
            "}";
    }
}
