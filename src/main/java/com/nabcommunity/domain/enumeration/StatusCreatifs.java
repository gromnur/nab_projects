package com.nabcommunity.domain.enumeration;

/**
 * The StatusCreatifs enumeration.
 */
public enum StatusCreatifs {
    ACTIF,
    INACTIF,
}
