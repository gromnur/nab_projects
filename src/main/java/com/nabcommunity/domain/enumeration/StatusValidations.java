package com.nabcommunity.domain.enumeration;

/**
 * The StatusValidations enumeration.
 */
public enum StatusValidations {
    VALIDE,
    REFUSE,
    ATTENTE,
    TERMINE,
}
