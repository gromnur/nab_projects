package com.nabcommunity.domain.enumeration;

/**
 * The EtatMissions enumeration.
 */
public enum EtatMissions {
    CHOIX_CREATIF,
    NON_DETAILLEE,
    EN_COURS,
    FINI,
    ANNULEE,
}
